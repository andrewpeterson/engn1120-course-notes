\begingroup

\newcommand{\cc}[1]{\ensuremath{C_\mathrm{#1}}}

\chapter{Simplifying complicated reaction networks}



If you have a complicated reaction network, and you know all the rate constants, it's not too challenging to numerically integrate the concentrations and examine rate behavior.
However, we often don't have all the rate constants available, or can gain insights by simplifying the equations.
As we'll see, it often turns out that it can be difficult to measure the rate constanst independently.
Simplifying reaction networks can help us to understand this.


%\standardfigure{qea-pssa}{[width=5.0in]{../f/qea-pssa/drawing.pdf}}{Qualitative schematics of QEA and PSSA.}
\begin{figure}
\centering
\includegraphics[width=5.0in]{f/qea-pssa/drawing.pdf}
\caption{Qualitative examples of the QEA and PSSA reactions examples.
\label{fig:qea-pssa} }
\end{figure}

\section{The Quasi-Equilibrium Assumption (QEA)}
Imagine the simple reactions,

\[ \ce{A <->[1][2] B ->[3] C} \]

\noindent and we'll assume the reactions proceed as:

\[ \ce{A ->[1] B}\, , \, r_1 = k_1 \cc{A}  \text{ (fast)}\ \]

\[ \ce{B ->[2] A}\, , \, r_2 = k_2 \cc{B}  \text{ (fast)} \]

\[ \ce{B ->[3] C}\, , \, r_3 = k_3 \cc{C}  \text{ (slow)} \]


We can sketch what this should look like, see Figure~\ref{fig:qea-pssa} (left).
Here, we are assuming that A and B very quickly equilibrate, and in this case, A is a bit thermodynamically preferred over B (since it stays in higher concentration).
As \ce{B -> C}, a bit of a quickly converts to B to keep things in equilibrium.

As an example, Professor Dumesic at Wisconsin likes to make a precursor chemical, 5-HMF, from glucose.
(He then converts the 5-HMF to some fuels or chemicals in a different reactor.)
In actuality, it is thought that HMF does not come from glucose, but from fructose.
However, HMF is most likely actually formed from fructose, not glucose.
As we know, glucose and fructose are isomers of each other.
If glucose and fructose stay in rapid equilibrium at his reactor conditions, which are high temperature, and the rate of conversion of fructose to HMF is slow, then this would be an example.

Let's look at how we can analyze this with the QEA; note that in this case, the equilibrated reaction is at the start of the reaction network, but the analysis would be the same if it were elsewhere.
Start the analysis with our complete system of equations:

\[ \frac{d \cc{A}}{d t} = -k_1 \cc{A} + k_2 \cc{B} \]

\[ \frac{d \cc{B}}{d t} = +k_1 \cc{A} - k_2 \cc{B} - k_3 \cc{B} \]

\[ \frac{d C_C}{d t} = +k_2 C_B \]

This is already starting to look a little complicated to solve by hand.
We can of course solve by computer if we happen to know all the $k$'s.
But we can make a simplifying assumption, known as the QEA, that the first reaction is in equilibrium at all times, $r_1 \approx r_2$:
\[ k_1 C_A \approx k_2 C_B \]
Note that this of course is not the case in real life!
It is only an assumption, and an approximation.
We can easily see that the back reaction has to be a little bit slower, by about the amount that B is converting to C.
But if B is converting to C at a much slower rate that A and B are interconverting, then these two may be equal to well under 1\% error.
We will ultimately eliminate B from our differential equations, so we can rearrange for $\cc{B}$:

\[ \cc{B} \approx \frac{k_1}{k_2} \cc{A} \]

\noindent
We can re-write our differential equation for \cc{C}:

\[ \frac{d \cc{C}}{d t} \approx + k_3 \cc{B} = \frac{k_1 k_3}{k_2} \cc{A} = k_\mathrm{eff} \cc{A} \]

\noindent
As we saw in our homework and solutions, if we just plug our approximate form for $\cc{B}$ into our differential equation for A, we would get $d \cc{A}/dt = 0$!
(You should be able to reason out why this happens.)
Instead, it's useful to examine a \emph{pooled} reactant concentration of A and B, which we get by summing the differential equations for A and B:


\[ \frac{d[\cc{A} + \cc{B}]}{dt} =
\left(
-k_1 \cc{A} + k_2 \cc{B}
\right) +
\left(
+k_1 \cc{A} - k_2 \cc{B} - k_3 \cc{B}
\right) = -k_3 \cc{B}
\]

\noindent
Now we can plug in our approximation for \cc{B}:

\[ \frac{d[\cc{A} + \frac{k_1}{k_2} \cc{A}]}{dt} =
 -\frac{k_1 k_3}{k_2} \cc{A}
\]

\[ \frac{d\cc{A}}{dt} =
 -\frac{k_1 k_3}{k_1 + k_2} \cc{A}
\]

We've now reduced this to something first-order in A, which here we can solve by hand.
Note that if we experimentally measured the loss of A, we wouldn't have a way to separate out all these rate constants from one another, but would just be measuring an effective constant.


\section{The Pseudo Steady-State Approximation (PSSA)}

Often called just ``the steady-state approximation'', which we will pre-pend the word `pseudo' to for the sake of our class.
(Note that this was named by chemists, not chemical engineers.)
As engineers, we tend to think of steady states in terms of unit operationns that are under transient conditions or steady state conditions, which we'll use quite extensively in the second part of this class.
Hence, we pre-pend ``pseudo'' since that is not what that refers to in this case.

The PSSA applies to a species, not a reaction.
Use the same set of reaction as above:

\[ \ce{A ->[1] B}\, , \, r_1 = k_1 C_A \]
\[ \ce{B ->[2] A}\, , \, r_2 = k_2 C_B\]
\[ \ce{B ->[3] C}\, , \, r_3 = k_3 C_B \]

\noindent
Here, we make the approximation that the amount of B present barely changes with time, while the amounts of A is steadily decreasing and the amount of C is steadily increasing.
(A more rigorous discussion of this is given in the reading.)
Basically, as soon as B is produced it is consumed; see Figure~\ref{fig:qea-pssa} (right).
That is, B is a short-lived intermediate and is present in low concentrations; while we don't assume the concentration of B is zero, we instead assume that the derivative of this very small number is essentially zero:

\[ 0 = \frac{d \cc{B}}{d t} = +k_1 \cc{A} - k_2 \cc{B} - k_3 \cc{B}\]

\noindent
If we solve this for \cc{B}, we get a relation to \cc{A} (which is different than in the QEA):

\[ C_B = \frac{k_1}{k_2 + k_3} C_A \]

\noindent
We can then use this to simplify equations; we'll hold off on examples of this until the next section, when we use it extensively for chain reactions and enzymatic reactions.

\section{Summary}
So to summarize, the QEA is where we assume some reaction step is in equilibrium, while the PSSA is where we assume that some reactive intermediate is at ``steady state'' concentration.

\endgroup
