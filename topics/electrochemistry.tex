\chapter{Electrochemistry}
{

Electrochemistry is a very special type of reaction, which sees increasing importance as we need to be able to convert electrical and chemical energy, for example, in batteries, electrolyzers, and fuel cells.
Industrially, it also has a rich history in examples such as aluminum smelting, electroplating, and the chlor-alkali process.
It is also a key reaction in corrosion.

Additionally, many biochemical reactions are very similar to electrochemical reactions.
You can often immobilize an enzyme on an electrode and find that it is a great electrocatalyst; for example, this is used in sensors for glucose: glucose oxidase is fixed on an electrode, when glucose is broken down, it creates a current which is measured to detect glucose.
Microbial fuel cells are microbes that are fixed on an electrode, where the microbe shuttles electrons to and from the electrode.

\section{Working example}
{
\newcommand{\ep}{\mathcal{E}} % electrical potential
\newcommand{\Wrev}{W_\mathrm{rev}} % electrical potential


Let's first get a mental picture of what electrochemistry is.
We'll start off by looking at the reaction we all probably did in some elementary or high school experiment, in which we split water to make hydrogen and oxygen gas.
A rough sketch of this experiment is shown in Figure~\ref{fig:echem-cell}.

This electrochemical reaction literally is composed of two half reactions.
At one electrode (known as the anode), water is oxidized, losing electrons to the electrode.
The half reaction is

\begin{equation}\label{eq:water-oxidation}
\ce{2 H2O -> 2 H+ + 2 e- + O2}
\end{equation}

\noindent and at the other, the half reaction is

\begin{equation}\label{eq:hydrogen-evolution}
\ce{2 H+ + 2 e- -> 2 H2}
\end{equation}

\noindent and in net this makes

\[ \ce{2 H2O -> 2 H2 + O2} \]

\noindent
Examining Figure~\ref{fig:echem-cell}, the electrons traverse through the electronic components (\textit{i.e.}, a potentiostat) while the protons diffuse through the water.

Note that this reaction would never happen on its own (at standard conditions).
This is an extremely uphill reaction---it is the opposite of burning \ce{H2}.
Effectively, we can add in free energy by adjusting the voltage on the cell---this adds free energy to the electrons while they are away from the reacting species!
That is, the chemical potential of the electrons in formulae~\eqref{eq:water-oxidation} and \eqref{eq:hydrogen-evolution} differ.
In net, this allows us to convert electrical energy into chemical energy.

If we run such a process in the opposite direction, we can do just the opposite: take \ce{H2} and \ce{O2}, produce \ce{O2}, and produce electricity.
This is known as a fuel cell.
All of the concepts we discuss are indentical; for simplicity, we'll continue to focus on the well-known water-splitting reaction.

\begin{figure}
\centering
\includegraphics[width=4.0in]{f/echem-cell/drawing.pdf}
\caption{An electrochemical cell for splitting water..
\label{fig:echem-cell}
}
\end{figure}

}
{
\newcommand{\muw}{\mu[\ce{H2O}]}
\newcommand{\muh}{\mu[\ce{H2}]}
\newcommand{\mup}{\mu[\ce{H+}]}
\newcommand{\muo}{\mu[\ce{O2}]}
\newcommand{\mue}{\mu[\ce{e-}]}
\newcommand{\mus}{\mu[\ce{$*$}]}
\newcommand{\muhs}{\mu[\ce{H$*$}]}

\newcommand{\muow}{\mu^\circ[\ce{H2O}]}
\newcommand{\muoh}{\mu^\circ[\ce{H2}]}
\newcommand{\muoo}{\mu^\circ[\ce{O2}]}
\newcommand{\muoe}{\mu^\circ[\ce{e-}]}

\newcommand{\dgr}{\Delta G_\mathrm{rxn}}
\newcommand{\dgrhe}{\Delta G^\mathrm{RHE}}
\newcommand{\dgro}{\Delta G_\mathrm{rxn}^\circ}
\newcommand{\dgrm}{\Delta G_\mathrm{rxn}^\mathrm{mol}}
\newcommand{\dgri}[1]{\Delta G_{\mathrm{rxn},#1}}
\newcommand{\dga}{\Delta G_\mathrm{a}}
\newcommand{\dgc}{\Delta G_\mathrm{c}}
\newcommand{\ep}{\mathcal{E}} % electrical potential
\newcommand{\epa}{\mathcal{E}_\mathrm{a}} % anode potential
\newcommand{\epc}{\mathcal{E}_\mathrm{c}} % cathode potential
\newcommand{\eprhe}{\mathcal{E}^\mathrm{RHE}} % RHE potential
\newcommand{\epo}{\mathcal{E}^\circ} % equilibrium potential
\newcommand{\zrhe}{0^\mathrm{RHE}}
\newcommand{\V}{\ensuremath{\mathrm{V}}} % volts
\newcommand{\vrhe}{\ensuremath{\mathrm{V}_\mathrm{RHE}}} % volts RHE
\newcommand{\gbh}{G_\mathrm{B}[\ce{H}]}
\newcommand{\wrev}{W_\mathrm{rev}}

\newcommand{\Cred}{C_\mathrm{red}}
\newcommand{\Cox}{C_\mathrm{ox}}


\section{Defining potential and the reference electrode}

We noted that the electrons have different chemical potentials at the different electrodes, as a result of the different electrical potentials (voltages).
Let's first establish the concept of potential, and describe how to properly reference it.
From electrostatics, we might remember that the potential can be defined as the amount of work it takes to move a test charge to a certain point in a field\footnote{In class, we drew this out in more detail; here, we'll take it as a given from E\&M.}, and we can express the potential $\ep$ as

\[ \ep \equiv \frac{\wrev}{q} \]

\noindent
where $\wrev$ is the reversible (that is, minimum) amount of work it takes to move the test charge from an infinite distance to a specific point, $q$ is the charge, and $\ep$ is the potential\footnote{We will use the squiggly $\ep$ to denote electrical potential; but note this goes by many other symbols in various fields: common ones are $V$ (voltage), which we're using for volume, $U$, which we're using for internal energy, and $E$, which we use for energy, so we are stuck with $\ep$ in this course.} (assuming the potential is zero at the infinite distance the test charge started at).

From thermodynamics, we also know that $\Delta G = \wrev$ at constant temperature, pressure, and moles, so we have a link between free energy and potential:

\[ \Delta G = q \ep \]

\noindent
In SI units, the charge is $q = FN$, where $F$ is the Faraday constant and $N$ is the moles of charge.
For an electron of charge -1, its chemical potential is then a linear function of potential, which can be expressed equivalantly as:

\[ \boxed{\mue(\ep) = \muoe - e \ep} \hspace{2em} \text{(per electron)} \]

\[ \mu(\ep) = \muoe - F \ep \hspace{2em} \text{(per mole)} \]

\noindent
where $e$ is the charge on an electron and $\muoe$ is the chemical potential of the electron at 0~V.
In the per-electron expression, $e$ is the charge on an electron, a fundamental constant (and a positive number); we typically use units of eV/particle for the chemical potential, which makes life simple since we don't need to look up the value of $e$.
For example, if $\ep$ = 1 V, then $-e \ep = - e (1~\mathrm{V}) = -1$ eV.
In the second expression, $F$ is the Faraday constant, which is the total charge of a mole of electrons, which has a value of 96485.33 C/mol.
If $\ep$ = 1 V, then $-F \ep = (-96485)(1~\mathrm{V}) = 96485$ J/mol (or 96.5 kJ/mol).
We'll use the first expression here for its simplicity, but just recall if you want SI units that 1eV/particle is equivalent to 96.5~kJ/mol.

Like energies, voltages are considered a relative quantity, so we need to establish what we mean when we say 0~V.
This is the purpose of the third electrode in Figure~\ref{fig:echem-cell}, which is called the reference electrode.
There are many choices of reference electrode, but for simplicity we'll focus on just one: the ``reversible hydrogen electrode''.
In this electrode, we bubble 1 atm \ce{H2} gas on a Pt electrode (as shown in Figure~\ref{fig:she}; Pt is very good at catalyzing both the back and forth reactions of \ce{H2}:

\[ \ce{H2 -> H+ + e-} \]

\[ \ce{H+ + e- -> H2} \]

\begin{figure}
\centering
\includegraphics[width=0.3 \textwidth]{f/reference-hydrogen-electrode/she.pdf}
	\caption{An example setup of a hydrogen reference electrode. Hydrogen, at standard pressure, is bubbled across a Pt catalyst. The electrode is maintained at zero net current, which happens at a well-defined voltage. Image from wikimedia commons, CC BY-SA 3.0, \url{https://commons.wikimedia.org/w/index.php?curid=5881067}.
\label{fig:she}
}
\end{figure}

A reference electrode is defined as one with \emph{zero net current}.
Recall that all of the electrons that flow through an electrode cause a chemical reaction; thus, saying zero net current and zero net (electrochemical) reaction are equivalent statements; that is, the current and reaction rate are proportional.
So when we say zero net current, this means that the forward and reverse reactions occur at the same rate---the reaction is in equilibrium.
For a given value of $\muoh$ and $\mup$, there is one and only one potential that will give the electrons the correct chemical potential to put this reaction in equilibrium.
That is, if we operate the voltage that causes zero net current, we have fixed the voltage at a reproducible value; we can call this 0~\vrhe; that is, 0~V on the RHE scale.

Because the reaction is in equilibrium we can say

\[ \dgrhe = 0 = 2 \mup + 2 \mue(0~\vrhe) - \muoh \]

\[ \mue(0~\vrhe) \equiv \frac{1}{2} \muoh - \mup \]


This gives us the chemical potential of electrons at zero volts RHE, which is going to be very handy going forward.
Also, recall that from last time we said that we can find the change in chemical potential of an electron as a function of voltage with:

\[ \mue(\ep) = \muoe - e \ep \]

Thus,

\[ \boxed{\mue(\ep) = \frac{1}{2} \muoh - \mup - e \ep} \]

\noindent (on RHE scale).
As a subtle note, the RHE scale is defined with the \ce{H2} gas at 1 atm of pressure, but with the temperature and pH at the reaction conditions of interest.
This is why we have applied the $^\circ$ symbol to $\muoh$ but not $\mup$.
Many other reference scales are possible, including the ``standard hydrogen electrode'' (SHE) which defines the above only at pH 0 (fixing $\mup$).
RHE lets the pH float.
Experimentally, people often use electrodes such as silver--silver chloride, which can be related to the RHE scale.

\section{Zooming in}
In the subsequent sections, we'll take our example reaction of splitting water, and zoom in from the macroscopic to elementary scales.


\subsection{The reaction}

Let's consider the reaction

\[ \ce{2 H2O -> 2 H2 + O2} \]

\noindent
which we'd like to run.
First consider just a beaker of water; this reaction is not going to take place; from thermodynamics, we intuitively know this is uphill in free energy.
We can look up the free energy change of this reaction and we get, at standard conditions: $\muw$ = -237.18 kG = -2.458 eV/molecule.
We get:

\[ \dgrm = 2 \muh + \muo - 2 \muw = 0 + 0 - 2 (-2.458) = +4.916~\mathrm{eV} \]

That is, this reaction is strongly uphill in free energy and won't happen on its own.

\subsection{Add an electrode}

Now, let's do a thought experiment.
Let's take two electrodes attached to a potentiostat, which keeps their potentials fixed at values we set; that is, it keeps $\Delta \ep$ constant, and also reports how much current it takes to do so.
Initially, consider that the electrodes are not immersed in water, but are just in vacuum.
\hl{Figure to come...}
The chemical potential of the electrons in either side will be different, and we'll call the ones at a higher voltage $\epa$ (anode) and at the lower voltage $\epc$ (cathode).

Now, let's immerse these electrodes in our aqueous solution.\footnote{Typically there will be some buffer that controls the pH, but for our purposes, we can just think of it as water.}
When we do this, we have the two half-reactions we suggested earlier, but now we can add free energy expressions for each half reaction.
On the left (anode), we have

\[ \ce{2 H2O -> O2 + 4 H+ + 4 e_{\mathrm{a}}- } \]

\[ \dga = \muo + 4 \mup + 4 \mue(\epa) - 2 \muw
\]

\noindent
On the right (cathode), we have

\[ \ce{4 H+ + 4 e_{\mathrm{c}}- -> 2 H2} \]

\[ \dgc = 2 \muh - 4 \mup - 4 \mue(\epc) \]

\noindent
Here, we've added subscripts to the electrons to distinguish them; this is to remind us they have different chemical potentials in the two half reactions.
If we add the two half reactions---keeping the distinguishing marks--- we get:

\[ \ce{ 2 H2O + 4 e_{\mathrm{c}}- -> O2 + 2 H2 + 4 e_{\mathrm{a}}- } \]

\noindent
That is, our original chemical equation where we have just noted that we have differing electron potentials.
Adding the half-cell free energies results in:

\[ \dgr = \dga + \dgc \]

\[ \dgr = \underbrace{\left(\muo + 2 \muh - 2 \muw\right)}_{\dgrm} + 4 \underbrace{\left( \mue(\epa) - \mue(\epc) \right)}_{e (\epc - \epa)}
\]

\[ \dgr = \dgrm + 4 e (\epc - \epa) \]

\noindent
The first term is just the free energy change of our desired reaction (ignoring electrochemistry), while the second term is the energy contribution from the potentiostat.
We can then ask, what is the minimum voltage we must supply in order to make the reaction possible?
Recall it was uphill by 4.916 eV (at standard states), we just need to get the net $\dgr \le 0$.

\[ 0 \le \dgrm + 4 e (\epc - \epa) \]

\[ \frac{- \dgrm}{4e} \le (\epc - \epa) \]

\[ \epa - \epc \ge \frac{\dgrm}{4e} = \frac{4.916~\mathrm{eV}}{4 e} = 1.23~\mathrm{eV} \]

\noindent
That is, in principle we need to supply at least 1.23~V to make this reaction work when our molecules are in standard states.
This gives us the overall voltage window, but doesn't tell us specifically what the voltage of each electrode must be at.

\subsection{Half reaction}

Now we zoom in one level, and examine the half reactions.
First, note that each half reaction doesn't really know about the other one---for example, on the \ce{H2} evolution side, as long as protons and electrons are flowing to the electrode, the reaction doesn't care where those protons and electrons are coming from.
We could have been splitting \ce{H2} on the other side (although this would have been a thermodynamic loser).
Let's examine one of the half reactions independently:

\[ \ce{4 H+ + 4 e- -> 2 H2} \]

\[ \dgc = 2 \muh - 4 \mup - 4 \mue(\epc) \]

\[ \dgc = 2 \muh - 4 \mup - 4 \left( \frac{1}{2} \muoh - \mup - e \epc \right) \]


\[ \dgc = 2 \left(\muh - \muoh \right) + 4 e \epc \]

\noindent
In the above, note that we have two similar terms: $\muoh$ and $\muh$.
The first, $\muoh$, indicates the chemical potential of \ce{H2} gas at 1 atm, which is the standard state; this comes from our reference electrode which is always run at the standard state---this is a constraint of the choice of reference.
However, we can choose to introduce \ce{H2} gas to our \emph{working} electrode\footnote{By convention, the electrode where the reaction we care about is taking place is called the ``working electrode''. The electrode that is running at equal and opposite current is called the ``counter electrode''. The electrode that is held at 0 current (reaction) is called the ``reference electrode. Of course, which is the working and which is the counter is just a matter of perspective.}

\[ \dgc = 2 \left(\muh - \muoh \right) + 4 e \epc \le 0 \]

\[ \epc \le \frac{\muoh - \muh}{2 e} \]

\noindent
If our \ce{H2} gas is fed at standard state,

\[ \epc \le 0~\vrhe \]

\noindent
That is, for this specific reaction, the potential must be less than 0 V on the RHE scale in order for the reaction to proceed in the positive direction, in net.
If we performed an identical analysis on the \ce{2 H2O -> O2 + 4 H+ + 4 e-} half reaction, we would find $\epa \ge 1.23~\vrhe$; that is, the potential must be greater than 1.23 V on the RHE scale for the reaction to proceed in the positive direction.
This matches our earlier finding that $\epa - \epc = 1.23~\V$.
Note that when we give a specific voltage: that is, $\epa$ or $\epc$, we need to provide the reference, and here we use units of \vrhe.
However, when we give a change in voltage ($\epa - \epc$) no reference is necessary, and we just use units of \V.

\paragraph{Definitions: Equilibrium potential, overpotential.}
The potential we just found above---that which makes the half reaction's $\Delta G = 0$---is referred to as the \emph{equilibrium potential} and given the symbol $\epo$.
At the equilibrium potential, the half reaction is in thermodynamic equilibrium; that is, the \emph{net} reaction rate is zero.
This does not mean there is no reaction---just that the forward and reverse reactions are occuring at identical rates, and in net cancel one another out: $r_\mathrm{f}(\epo) = r_\mathrm{r}(\epo)$.
In fact, this (forward or reverse) reaction rate at $\epo$ has a name: the \emph{exchange current}, when expressed in units of amps (or more commonly, the exchange current density when normalized per electrode surface area).
Typically, a good electrocatalyst (such as Pt for hydrogen evolution) will have a high exchange current---that is, it is already catalyzing the reaction even when there is no driving force, so when a driving force is applied the net reaction takes off quickly.

Therefore, the driving force for a reaction is the difference between the electrode's potential\footnote{Sometimes called the working potential.} and the $\epo$; this is given a special name, the \emph{overpotential} which is defined as

\[ \eta \equiv \ep - \epo \]

\noindent
Note that the overpotential has a sign: for an anodic reation (one in which electrons flow from chemical species into the electrode), a positive overpotential means a driving force for the forward reaction.
For a cathodic reaction (one in which electrons flow from the electrode to chemical species), a negative overpotential means a driving force for the forward reaction.\footnote{There are competing definitions here: some will speak of absolute overpotential and toss out the negative sign on the cathodic overpotentials. And the term is often used synonymously with how much overpotential is necessary to get the reaction to proceed at an appreciable rate on a given material. E.g., you might say that ``Platinum has a low overpotential for the hydrogen evolution reaction while gold has a high overpotential.'', which we would translate to mean the overpotential necessary to achieve a certain current. Here, we're using the definition from the Castellan textbook~\cite{Castellan1983}.}


\paragraph{Cell performance, efficiency}

We can now consider how a real electrolyzer runs, and make some statements about the efficiency of the process.
See Figure~\ref{fig:overpotentials-with-kinetics}.
The left plot shows the case for the electrolyzer that splits water into \ce{H2} and \ce{O2}, the case we have been examining.
The equilibrium potentials ($\epo$) of 0 and 1.23 \vrhe\ have been marked on the plot.
In the case of the cathodic reaction (\ce{H2} evolution), the potential must be lower than 0 \vrhe\ for the reaction to begin; this is shown when the current ($|I|$, expressed here as an absolute value) only takes off when the driving force, or overpotential, is made negative.
The anodic reaction needs a potential more positive than its $\epo$ to take off, which is also depicted.
For simplicity, we mark the overpotentials and currents as absolute values in Figure~\ref{fig:overpotentials-with-kinetics}.
Note that in this sketch the kinetics of the two reactions are quite different; this is actually the case as the best catalysts for the oxygen reduction half reaction are not as good as the best for hydrogen evolution.

\begin{figure}
\centering
\includegraphics[width=0.49 \textwidth]{f/overpotentials/electrolyzer.pdf}
\includegraphics[width=0.49 \textwidth]{f/overpotentials/fuelcell.pdf}
	\caption{Schematic of the performance of an electrolyzer (left) and a fuel cell (right), both running the net reaction \ce{H2O <-> H2 + 1/2 O2}.
\label{fig:overpotentials-with-kinetics}
}
\end{figure}

If we instead have a fuel cell, we instead analyze the reactions \ce{2 H2 -> 4 H+ + 4 e-} and \ce{O2 + 4 H+ + 4 e- -> 2 H2O}.
Since these are the reverse of the reactions we analyzed earlier, the equilibrium potentials stay the same, but the direction to have a net current for the reaction reverses.\footnote{You may want to work through the same analysis we did earlier to convince yourself.}
Additionally, note now that the hydrogen electrode is acting as the anode and the oxygen electrode as the cathode.
In this case, electrical power is being extracted from the reaction rather than added to it; however, we see that at some kinetic limitation the amount of power we can extract becomes very small.

\begin{figure}
\centering
\includegraphics[width=0.49 \textwidth]{f/overpotentials/drawing.pdf}
	\caption{A simple analysis of the efficiency of an electrolyzer.
\label{fig:overpotentials-efficiency}
}
\end{figure}

We can analyze the efficiency of these systems very simply, on the basis of the plots we have drawn above.
Efficiency is tends to be expressed as

\[ \mathrm{efficiency} \equiv \frac{\dot{W}_\mathrm{useful}}{\dot{W}_\mathrm{applied}} \]

\noindent
From your circuits class, you should remember that power is expressed as $\dot{W} = I \Delta \ep$.
Let's choose a specific current $|I|$ on Figure~\ref{fig:overpotentials-with-kinetics}, then we can just express the efficiency as a ratio of voltages.
The applied voltage is $\Delta \ep^\mathrm{applied} = \Delta \ep^\mathrm{equil} + |\eta_\mathrm{cathode}| + |\eta_\mathrm{anode}|$.
The maximum useful work out is $\Delta \ep^\mathrm{equil}$; that is, this is the free energy stored in the \ce{H2} and \ce{O2} produced, relative to \ce{H2O}.
Therefore, we can express the efficiency of the electrolyzer as 

\[ \mathrm{efficiency} = \frac{\Delta \ep^\mathrm{equil}}{\Delta \ep^\mathrm{equil} + |\eta_\mathrm{cathode}| + |\eta_\mathrm{anode}|} \]

\noindent
This is summarized in Figure~\ref{fig:overpotentials-efficiency}.
We can apply the same sort of analysis to the fuel cell, where the useful work out is $\Delta \ep^\mathrm{extracted}$ and the applied work is the free energy in the original chemicals, and we find for the fuel cell that

\[ \mathrm{efficiency} = \frac{\Delta \ep^\mathrm{eq} - |\eta_\mathrm{cathode}| - |\eta_\mathrm{anode}|}{\Delta \ep^\mathrm{eq}} \]



\subsection{Elementary steps}

\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/HER-mechanism/drawing.pdf}
\caption{A hypothesized mechanism for the hydrogen evolution reaction.
\label{fig:HER-mechanism}
}
\end{figure}

Now we zoom in again to the level of elementary steps.
To do so, we'll have to hypothesize a reaction mechanism.
Let's hypothesize it to be as in Figure~\ref{fig:HER-mechanism}; in words we can write this as below, where $*$ represents a surface site.

\[ \ce{H+ + e- + $*$ ->[1] H$*$} \]

\[ \ce{H$*$ + H+ + e- ->[2] H2 + $*$ } \]

\noindent
Similar as before, we can write a $\Delta G$ for each reaction; this allows us to start to put together free energy diagrams and to ask questions---such as what potential does it take to make an individual step be downhill in free energy.

\[ \dgri{1} = \muhs - \mus - \mup - \mue(\ep) \]

\[ \dgri{2} = \muh + \mus - \muhs - \mup - \mue(\ep) \]

Here, the potential ($\ep$) will be the same for each step, as they are both occurring at the same electrode.\footnote{Unless we can vary our potential down at the femtosecond level, which seems hard.}
We can plug in our expression for the chemical potential of electrons as a function of the voltage, and we get

\[ \dgri{1} = \muhs - \mus - \frac{1}{2} \muoh + e \eprhe \]

The non-electrochemical portion of this (the first three terms) are often referred to as the binding (free) energy of H.
This is a material property: that is, platinum will bind hydrogen with a different strength than will copper.
This helps us to understand why different catalysts perform differently in this reaction.

In this example, we can define:

\[ \gbh \equiv \muhs - \left(\mus + \frac{1}{2}\muoh\right) \]

\noindent
and re-express as

\[ \dgri{1}(\ep) = \underbrace{\gbh}_{\shortstack{property of\\material}} + \underbrace{e \eprhe}_{\shortstack{function of\\potential}} \]

\noindent
Similarly, we find

\[ \dgri{2}(\ep) = \left(\muh - \muoh\right) - \gbh + e \eprhe \]

As in the previous section, the $\muh - \muoh$ term just allows for us to feed in \ce{H2} gas at any pressure we desire.
Note that if we add together the free energy changes of these two elementary steps, we exactly recover the half-reaction free energy---although we chose to express it on a 2-electron basis here and a 4-electron basis earlier, so it differs by a factor of 2.


\paragraph{Example.}
Let's take an example and try to build a free energy diagram for our hydrogen-evolution reaction.
In our example, we will conduct our experiment with an \ce{H2} pressure of 1 atm, so we are left with

\begin{align*}
	\dgri{1}(\ep) &= \gbh + e \eprhe \\
	\dgri{2}(\ep) &= - \gbh + e \eprhe \\
	\cline{1-2}
	\dgc(\ep) &= 2 e \eprhe \\
\end{align*}

\noindent
Let's assume we have two possible electrocatalyst materials: Material A binds hydrogen with +0.25~eV, and Material B binds hydrogen with -0.40~eV, both in terms of $\gbh$.
In the below script, we make potential-dependent free-energy diagrams for these materials, shown in Figure~\ref{fig:HER-FED}.

\begin{figure}
\centering
\includegraphics[width=0.99 \textwidth]{f/HER-FED/out.pdf}
\caption{Free energy diagram for hydrogen evolution for the example shown in the text, as a function of potential and material.
\label{fig:HER-FED}
}
\end{figure}

\subsection{Reaction barriers\label{sec:echem-barriers}}

Of course, the ultimate kinetics are controlled by the reaction barriers between steps.
These barriers themselves are functions of potential; however, the potential-dependence cannot be treated in as straightforward of a manner.
Computationally, methods are just emerging for the explicit calculation of potential-dependent barriers.
One of these methods has been developed in my group~\cite{Kastlunger2018}, and we'll show some results here to highlight the behavior.



\begin{figure}
\centering
 \includegraphics[width=0.42\textwidth]{f/Kastlunger2018/interpolation_Au_round_barriers_and_line.pdf}
 \includegraphics[width=0.45\textwidth]{f/Kastlunger2018/interpolation_Pt_round_barriers_and_line.pdf}
	\caption{Potential-dependent barriers as calculated by Kastlunger \textit{et al.}~\cite{Kastlunger2018} for the reaction \ce{$*$ + H+ + e- -> H$*$} on gold (left) and platinum (right). $\Delta \Omega$ can be taken as the potential-energy, which has been corrected to account for the variable number of electrons as one moves across the barrier.
\label{fig:Kastlunger2018}
}
\end{figure}

Figure~\ref{fig:Kastlunger2018} shows potential-dependent barriers calculated for the first step of hydrogen evolution:

\[ \ce{$*$ + H+ + e- -> H$*$} \]

\noindent
These barriers need to be calculated with special electronic structure methods that allow for a non-constant number of electrons in the cell; the symbol $\Delta \Omega$ is used to denote this.
For our purposes, we can just think of the $y$ axis as potential energy.

Overall, we see the elementary step becomes more downhill as the potential is made more negative; this is as we expect from our discussion earlier.\footnote{Note, however, that it is not one-to-one: that is, we would expect that a 1 V change would result in a 1 eV change in free energy, as this is a one-electron process. In the figures, we see something closer to 0.6 eV. The latest research~\cite{Chen2018} suggests there is a step not shown in this figure in which the proton first approaches the surface, reducing its free energy somewhat.}
The barrier is also reduced as the step is made downhill.
As might qualitatively be expected, the barrier does not decrease as quickly as the reaction energetics; this is to be expect due to limiting behaviors: for example, the barrier cannot go lower than zero, while the reaction energetics do.

\begin{figure}
\centering
\includegraphics[width=0.40\textwidth]{f/Kastlunger2018/marcus.pdf}
\includegraphics[width=0.59\textwidth]{f/Kastlunger2018/Au-and-Pt.pdf}
\caption{Left: A model of overlapping parabolas is a reasonable model to the observed reaction barriers.~\cite{Lindgren2018-submitted} Right: The Au and Pt data of Figure~\ref{fig:Kastlunger2018} fitted to the overlapping parabolas model, described by just the parameter $b$.
\label{fig:parabolas}
}
\end{figure}

We can think of the limiting behavior of these reaction barriers: as a reaction becomes very downhill in free energy, we eventually hit the point where $E^\ddag \to 0$.
Similarly, as a reaction becomes very uphill in free energy, we hit the point where there is no additional behavior, other than the reaction energy itself, that is, $E^\ddag \to \Delta E$.
In my research group, we use a simple model to capture this trend: overlapping parabolas that change in height,\footnote{This is qualitatively similar to ``Marcus theory'', a well-known concept in electron transfer, but here we're really just using this as a functional form, not to claim anything about the physics.} as shown in Figure~\ref{fig:parabolas}(left).
The equation that captures this is given by:

\begin{equation}\label{eq:marcus-type}
E^\ddag = \left\{
\begin{array}{lll}
0 &, & \Delta E < -4 b \\
\frac{(\Delta E + 4 b)^2}{16b} &, & -4b \le \Delta E \le 4b \\
\Delta E &, & \Delta E > 4b
\end{array}
\right.
\end{equation}

\noindent
If we use non-linear regression to fit the parameter $b$ (which is the barrier height when the reaction is thermoneutral), we see we get a very nice fit as in the right side of Figure~\ref{fig:parabolas}.

However, most electrochemical literature assumes something much simpler, and effectively assumes that the barrier change is proportional to the energy change.
This will be the basis of the Tafel and Butler--Volmer analyses shown next.


\section{Common kinetic expressions: Butler--Volmer, Tafel equations}

Many electrochemists express reactions in Butler--Volmer (for reversible reactions) or Tafel (for irreversible reactions) formalisms.
These are just simplified kinetic forms, much the same as Michaelis--Menten is a simlified kinetic form that is useful for enzyme kinetics.
Here, we'll give the background on where they come from.


\paragraph{Butler--Volmer.}
We'll start by examining the easiest electrochemical reaction we can think of: a metal dissolving.
This is the driving reaction in batteries---e.g., when a \ce{Li+} ion travels back and forth between two electrodes, and also important in processes like corrosion.
See Figure~\ref{fig:butler-volmer} (top).



\begin{figure}
\centering
\includegraphics[width=3.0in]{f/butler-volmer/drawing.pdf}
	\caption{(Top) A simple electrochemical reaction of a metal dissolving into its ions. (Bottom) The free energy at zero overpotential ($\eta \equiv \ep - \epo = 0$) and at some applied applied overpotential $\eta$, along with the changes in transition-state energies
\label{fig:butler-volmer}
}
\end{figure}

The forward reaction is

\[ \ce{M -> M$^{z+}$ + $z$ e-} \]

\noindent and the reverse reaction is

\[ \ce{M$^{z+}$ + $z$ e- -> M} \]

\noindent
Here, $z$ is the charge on the ion.
An example of this would be a silver electrode producing \ce{Ag$^{+}$} ions which dissolve out into solution, or can be added back onto the electrode; in this case, $z=1$.
Let's sketch a free energy diagram for this elementary reaction at the equilibrium potential $\epo$; see Figure~\ref{fig:butler-volmer}, middle.

Next, we apply an overpotential to this free energy diagram; that is, we set the potential to $\eta = \ep - \epo$.
For this one-electron process, a change of $\eta$ will result in a free energy change of the elementary step of $z F \eta$ (using a per-mole basis; if using a per-particle basis this will be $z e \eta$).
In the diagram, we apply this to the initial state, but this choice is arbitrary.
We know from the previous sections that the barrier will change by something less than $z F \eta$; in the Butler--Volmer formalism it is assumed to change by a constant fraction (even though we know from Section~\ref{sec:echem-barriers} that this cannot be true); that is, the barrier is assumed to change by

\[ (1 - \alpha) z F \eta \]

\noindent
$\alpha$ is called the `transfer coefficient', and it typically varies between 0 and 1, depending on the reaction.

We define the current when the electrode behaves as an anode and a cathode as $I_+$ and $I_-$, respectively, but because these are surface-area dependent reactions (catalytic) we normalize the currents by area and speak in terms of ``current densities'', $i_-$ and $i_+$.
The current densities are nothing other than reaction rates, times the number of electrons, in other words

\[ i_+ = z F k_f \Cred \]

\[ i_- = z F k_r \Cox \]

\noindent where we'll keep it generic in terms of $\Cred$ and $\Cox$, rather than put in the metal and ion concentrations.
Note that even though we included \ce{e-} in the chemical equations, it does \emph{not} appear in the rate equation ($r = k C$).
How can we find these reaction rate constants?
Let's assume they follow transition-state theory.

\[ k_f = \left( \frac{k_B T}{h} \right) e^{-\Delta G^\ddag_+/RT} \]

\[ k_r = \left( \frac{k_B T}{h} \right) e^{-\Delta G^\ddag_{-}/RT} \]

\noindent
In general, we can write equations for the transistion state free energies:

\[ \Delta G^\ddag_+ = \Delta G^\ddag_f + (1 - \alpha) z F \eta - z F \eta = \Delta G^\ddag_f - \alpha z F \eta \]

\[ \Delta G^\ddag_- = \Delta G^\ddag_r + (1 - \alpha) z F \eta \]

\noindent
So,

\[ i_+ = z F \Cred \frac{k_B T}{h} \exp \left\{ \frac{- (\Delta G^\ddag_f - \alpha z F \eta)}{RT}\right\} \]

\[ i_- = - z F \Cox \frac{k_B T}{h} \exp \left\{ \frac{- (\Delta G^\ddag_r + (1 - \alpha) z F \eta)}{RT}\right\} \]

\noindent
Or in shorthand,

\[ i_+ = k_+ \Cred \exp \left\{ \frac{  \alpha z F \eta}{RT}\right\} \]

\[ i_- = - k_- \Cox \exp \left\{ \frac{- (1 - \alpha) z F \eta)}{RT}\right\} \]

\noindent
where the $k_+$ and $k_-$ terms do not depend on potential.
The net current density (net rate of reaction) is

\[ i = i_+ + i_- \]

\[ i = k_+ \Cred \exp \left\{ \frac{  \alpha z F \eta}{RT}\right\} -
 k_- \Cox \exp \left\{ \frac{- (1 - \alpha) z F \eta)}{RT}\right\} \]


To simplify further, let's look at the case when there is no net current, $i = 0$,

\[ (i_+)_0 = (i_-)_0 \equiv i_0 \]

\noindent
$i_0$ is called the `exchange current density', in other words, its the amount of reaction that's actually happening when there is no current (when it looks like there's no reaction!).
This is equivalent to

\[ i_0 = k_+ (\Cred)_0 \exp \left\{ \frac{  \alpha z F 0}{RT}\right\} 
 =  k_- (\Cox)_0 \exp \left\{ \frac{- (1 - \alpha) z F 0)}{RT}\right\} \]

\noindent
We can divide our net current expression by $i_0$, and we get,

\[ i/i_0 = \frac{\Cred}{(\Cred)_0} \exp \left\{ \frac{  \alpha z F \eta}{RT}\right\} -
\frac{\Cox}{(\Cox)_0} \exp \left\{ \frac{- (1 - \alpha) z F \eta}{RT}\right\} \]

Note that if its well-stirred, $\Cox = (\Cox)_0$, (no mass-transfer limitations) so that term will go away.
$\Cred$ is just the metal, so that's constant.
We simplify to

\[ i = i_0 \left[  \exp \left\{ \frac{  \alpha z F \eta}{RT}\right\} -
\exp \left\{ \frac{- (1 - \alpha) z F \eta }{RT}\right\} \right] \]

This is called the `Butler--Volmer' equation, and it is one of the standard equations of electrochemistry; but you can see we can derive it from basic kinetics principles (albeit, with one or two crude assumptions)!


\begin{figure}
\centering
\includegraphics[width=3.0in]{f/butler-volmer/drawing2.pdf}
\caption{A sketch of the Butler--Volmer form.
\label{fig:butler-volmer2}
}
\end{figure}

We sketch the form of this in Figure~\ref{fig:butler-volmer2}.
Although this was derived for a very simple reaction, it turns out to hold for many cases of more complex chemistry as well.

\paragraph{Tafel equation.}
A simplification of this is the Tafel equation, obtained when we are far on one side or the other.
The Castellan reading defines this as $\eta$ greater than 75 mV, but the definition is subjective.
Looking at just the anodic side for now, we can ignore one of the exponents and we are left with

\[ i \approx i_a = i_0 \exp \left\{ \frac{\alpha z F \eta }{RT} \right\} \]

\noindent
Often, we this is linearized by taking the log of both sides.

\[ \ln i = \ln i_0 + \frac{\alpha z F}{RT} \eta \]

\noindent
A Tafel plot of $\eta$ versus $\ln i$ is used to get the needed parameters.
I've always found these plots to be confusing to new electrochemists, as the driving force is shown on the $y$ axis and the response is on the $x$ axis.
However, ``Tafel slopes'' are commonly used to describe how well the current responds to changes in potential, and a small Tafel slope is desirable for a responsive reaction.

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/tafel/tafel-from-wiki.png}
\caption{Tafel plot.
\label{tafel}
}
\end{figure}

}





\section{Appendix: code}

Code for creating the free energy diagrams.
\pcode{f/HER-FED/run.py}
}
