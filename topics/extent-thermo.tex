\begingroup
\newcommand{\pd}[3]{\left(\frac{\partial #1}{\partial #2}\right)_{#3}}
\newcommand{\n}[1]{n_\text{#1}}
\newcommand{\nuv}[1]{\nu_\text{#1}}
\newcommand{\na}{n_\mathrm{A}}
\newcommand{\nb}{n_\mathrm{B}}
\newcommand{\nao}{n_\mathrm{A}^0}
\newcommand{\nbo}{n_\mathrm{B}^0}
\newcommand{\nua}{\nu_\mathrm{A}}
\newcommand{\nub}{\nu_\mathrm{B}}
\newcommand{\mua}{\mu_\mathrm{A}}
\newcommand{\mub}{\mu_\mathrm{B}}
\newcommand{\grxn}{\ensuremath{\Delta G_\mathrm{rxn}}}

\chapter{Basics of reaction thermodynamics}

All reactions are rooted in thermodynamics: the driving force for reactions is most properly described in terms of the chemical potential, and derived quantities such as activity and fugacity.
Ultimately, both reaction equilibria and reaction rates are rooted in thermodynamics and are stated most generically in those terms, and are properly functions of activity (and not concentration, as is commonly assumed).


\section{Stoichiometry and extent of reaction ($\xi$)}

Before diving into thermodynamics, we'll first define systematic metrics to describe the extent of reaction and stoichiometry.
Let's use the following reaction as an example:

\[ \ce{{Glucose} ->[1] {Fructose}} \]

\noindent
These are isomers, both with the chemical formula \ce{C6H12O6}.
When glucose is dissolved in water, it will slowly react and the concentrations of glucose and fructose will adjust until they hit their equilibrium point.
(Of course, this is not the only reaction that is possible if you dissolve glucose in water: you might degrade the glucose or grow some bacteria!
However, for today's example we'll assume that this is the only possible reaction.)
Industrially, enzymes are added to catalyze this reaction to produce high-fructose corn syrup.
The enzymes make the reaction occur quickly, but to the same equilibrium point.

Let's state that we initially have 2 mol of glucose and 0.5 mol of fructose, then observe the system as a reaction occurs, as in Table~\ref{table:glufru}.
At time $t$, we observe that we now have 1.5 mol of glucose.
Under the assumption that this is the only allowed reaction, how much fructose do we have?
It should be pretty obvious that if 0.5 mol of glucose is destroyed, then 0.5 mol of fructose must be created.

\begin{table}
\centering
\caption{Moles of glucose and fructose observed at various times during an experiment. \label{table:glufru}}
\begin{tabular}{lrr}
\hline \hline
Time & $n_\mathrm{G}$, mol & $n_\mathrm{F}$, mol\\
\hline
	0 & 2 & 0.5 \\
	$t$ & 1.5 & ? \\
	$\infty$ & ? & ? \\
\hline
\end{tabular}
\end{table}

However, if this was not a single reaction, but if we had multiple, interconnected reactions---as in Table~\ref{table:water-rxns} with all the possible \ce{H2 + O2} reactions---this could be more difficult to assess.
Thus, we need a systematic way to approach it; let's define this based on our simple reaction.

First, note that we specified one thing about the system at time $t$ (the moles of glucose) and \emph{both} components changed; clearly, they are related.
Specifically, we can relate them by defining an ``extent of reaction'', $\xi$ (``xi'').
Let's define it by using it:

\[ \n{G}(t) = \n{G}(t=0) + \nuv{G,1} \xi_1(t) \]

\[ \n{F}(t) = \n{F}(t=0) + \nuv{F,1} \xi_1(t) \]

\noindent
We used the subscript ``1'' here to correspond to the reaction number; in principle we can write a $\xi_i$ for every (independent) reaction in the system.
Note that we need to add a stoichiometric coefficient, $\nu$, in each of those reactions.
Here the coefficients in our reaction were -1 and +1 for glucose and fructose, respectively: that is, for every one mole of glucose consumed, one mole of fructose is created.
However, if we were examining a different reaction (\textit{e.g.}, \ce{maltose -> 2 glucose}), then these coefficients would of course change.

Let's add just that reaction, and re-write our equations:

\[ \ce{Maltose ->[2] 2 Glucose} \]

\[ \n{G}(t) = \n{G}(t=0) + \nuv{G,1} \xi_1(t) + \nuv{G,2} \xi_2(t) \]

\[ \n{F}(t) = \n{F}(t=0) + \nuv{F,1} \xi_1(t) + \nuv{F,2} \xi_2(t) \]

\[ \n{M}(t) = \n{M}(t=0) + \nuv{M,1} \xi_1(t) + \nuv{M,2} \xi_2(t) \]

\noindent Of course, $\nuv{M,1} = 0$ and $\nuv{F,2} = 0$, but we leave them in for generality.
Therefore, we can see that the general equation is

\[ \boxed{ n_j(t) =  n_j(t=0) + \sum_i^\mathrm{reactions} \nuv{j,i} \xi_i(t)} \text{ for each species $j$} \]

\noindent
In the next section, we'll discuss how to find the last entry in Table~\ref{table:glufru}: the equilibrium amounts.


\section{Describing reactions with thermodynamics}\label{sxn:equilibria}

Here, we start to review (or introduce!) the description of reactions on a rigorous thermodynamic basis.

\paragraph{How to find equilibrium for a single reaction?} Consider a single reversible reaction,

\[ \ce{A <-> B} \]

\noindent at a fixed temperature and pressure, $T, P$.
This reaction does not need to be elementary.
How do we determine what the equilibrium concentrations---or equilibrium extent of reaction---will be?
From thermodynamics, the equilibrium point at a fixed temperature and pressure is that which minimizes the Gibbs free energy $G$, since $T$ and $P$ are the canonical variables for $G$.
In our case, we would like to minimize $G$ as a function of the extent of reaction $\xi$, while holding $T$ and $P$ constant:

\begin{align*}
\min_{\xi} \, & G(\xi) \\
\text{subject to } \, & dT = 0, dP=0
\end{align*}


\noindent
We'll start by expressing $G$ as a function of the change in moles of both species.
Assume we have some initial concentration: $\nao, \nbo$.

\[
G(\na, \nb) = G(\nao, \nbo) 
+ \int_{\nao}^{\na} \left(\frac{\partial G}{\partial \na}\right)_{T, P, \nb} \mathrm{d}\na
+ \int_{\nbo}^{\nb} \left(\frac{\partial G}{\partial \nb}\right)_{T, P, \na} \mathrm{d}\nb
\]

\noindent
(Equivalently, we could have integrated the fundamental equation, $dG = -S dT + V dP + \sum_i \mu_i dn_i $, at constant $T$ and $P$.)
By stoichiometry, we can clear out the moles and put in the extent of reaction ($\xi$),

\[ \na = \nao + \nua \xi \Rightarrow  d \na = \nua \, d \xi \]

\noindent
Algebraically changing the integration variable:

\[
\int_{\na=\nao}^{\na=\na} \left(\frac{\partial G}{\partial \na}\right)_{T, P, \nb} d\na
=
\int_{\xi=0}^{\xi=\xi} \left(\frac{\partial G}{\partial \na}\right)_{T, P, \nb} \nua d \xi
\]

\noindent
If we did this for B, the only difference was in the original $\xi$ expression $\nu$ would be +1 instead of -1; however, we'll leave them both in terms of $\nu$.
Therefore, we can re-write the whole equation for $G$, and substitute in the chemical potential\footnote{Chemical potential is formally defined in \secref{sxn:chemical-potential}, if you don't recall it from thermodynamics.}

\[
G(\xi) = G(\xi{=}0)
+ \int_{0}^{\xi} \nua \mua d \xi
+ \int_{0}^{\xi} \nub \mub d \xi
\]

\noindent
or

\[
G(\xi) = G(\xi{=}0)
+ \int_{0}^{\xi} \left[ \nua \mua + \nub \mub \right] d \xi
\]

Our plan was to to minimize this as a function of $\xi$, which we will do by taking its derivative and setting it to 0.

\[
\frac{d G}{d \xi} = 0 =\nu_A \mu_A + \nu_B \mu_B
\]

Nothing was particular to having a single reactant and a single product, so in general to minimize the free energy of a reaction at a particular temperature and pressure we require

\[
	\boxed{0 = \sum_{i=1}^{N_\mathrm{components}} \nu_i \mu_i}
\]

\noindent
The above is the thermodynamic criterion for chemical equilibrium for a single reaction at fixed temperature and pressure.
We will use this to define an equilibrium constant in \secref{sxn:equilibrium-constant}, but first we should make sure we understand $\mu$.


\subsection{The chemical potential \label{sxn:chemical-potential}}

The above equation states that the chemical potential is crucial in determining reaction equilibria.
Before going further, we'll review the definition of chemical potential from thermodynamics.
The chemical potential is \emph{the} driving force for chemistry.
Its name can give us a hint as to what it means---like the electrical potential or gravitational potential, it dictates the direction and size of the driving force.\footnote{In fact, there is a direct correspondence. Take a system like a copper wire: you might recall that the electrical potential is equivalent the Fermi energy. One definition of the Fermi energy is as the chemical potential of \emph{electrons}---that is, the incremental energy to add or remove an electron---from the system. Thus, chemical potential and electrical potential are often equivalent concepts.}
It is formally defined as

\[ \mu_i \equiv \left( \frac{dG}{dn_i} \right)_{T,P,n_{j \neq i}} \]

\noindent
This is also known as the ``partial molar'' Gibbs free energy (that is, the Gibbs free energy per incremental mole).
Graphically, it is just the slope of the plot of the Gibbs free energy versus the number of moles of the component of interest.
See Figure \ref{fig:mu}.

\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/mu/mu.pdf}
\caption{Definition by illustration of $\mu$\, the chemical potential.
\label{fig:mu}
}
\end{figure}

When I teach thermodynamics, I typically introduce $\mu$ by talking about homemade vodka.
If you mix a liter of pure water and a liter of pure ethanol, do you get 2 L of ``vodka''?
No.
As you might recall, you get something smaller, like 1.9 L, because volumes are not additive.
Physically, this means that the \ce{H2O} and \ce{CH3CH2OH} molecules can pack together a little more tightly when mixed as compared to when they are pure.
Now, envision some different scenarios:
\begin{itemize}
\item Say you have a liter of pure ethanol, and you add another tiny drop (like 1 $\mu$L) of ethanol to it: how much does the volume change?
Intuitively, it should increase by 1 $\mu$L, the amount added.
\item At the other extreme, if you have a liter of pure water and you add a $\mu$L of pure ethanol to it, does it change by the same amount?
Based on our previous insight, it should increase by something less than 1 $\mu$L.
\end{itemize}

\noindent
From this analysis, we can say that when adding a drop of ethanol to our system, the total (system) volume change is dependent upon composition.
Formally, we call the amount that the total system volume changes with the incremental addition of component $i$ the ``partial molar volume'' and define it as:

\[ \bar{V}_i \equiv \pd{V}{n_i}{T,P,n_{j\ne i}} \]

\noindent
(where the incremental $i$ is expressed in moles, not liters).

Similarly, free energies are not additive.
In a nearly perfect analogy, we can define the amount that the free energy of a system changes when we add an incremental amount of component $i$:

\[ \mu_i \equiv \bar{G}_i \equiv \pd{G}{n_i}{T,P,n_{j\ne i}} \]

\noindent
Therefore, to find $G$ given a change in $n$ (at constant $T$, $P$), we can just integrate $\mu_i$:

\[
G(n_i) = G(n_i^0) + \int_{n_i^0}^{n_i} \left( \frac{\partial G}{\partial n_i}\right)_{T, P, n_{j \ne i}}  \mathrm{d}n_i
 = G(n_i^0) + \int_{n_i^0}^{n_i} \mu_i  \mathrm{d}n_i
\]

\noindent
In summary, the chemical potential describes how much the free energy of a \emph{system} changes when we add or subtract a bit of one of its components.
But how do we relate the chemical potential to something real (that is, physical)?


\subsection{Linking the chemical potential to physical quantities}

\paragraph{How the chemical potential varies with composition.}
If---and only if---you have an ideal gas, you can derive that the chemical potential follows the relation:

\begin{equation} \label{eqn:ideal-gas-mu}
\mu_i = \mu_i (T, P^\circ) + R T \ln \frac{P_i}{P^\circ} \hspace{3em}  \text{(ideal gases only!)}
\end{equation}

\noindent where $P_i$ is the partial pressure of component $i$.
In words: if you know the chemical potential at one pressure ($P^\circ$), you can find the chemical potential at any other pressure ($P$), if temperature is held constant.

\paragraph{Fugacity.}
The previous relationship has proved so useful that thermodynamicists have defined a general version---applicable to both non-ideal and ideal systems---as

\begin{equation}\label{eqn:fugacity}
\mu_i = \mu_i (T, P^\circ) + R T \ln \frac{f_i}{P^\circ}
\end{equation}

\noindent where $f_i$ is the fugacity of component $i$.

What is fugacity?
Equation~\eqref{eqn:fugacity} is actually the \emph{definition} of fugacity.
Here's an attempt at a definition in words:

\begin{quote}
\emph{Fugacity is something that has units of pressure. It is equal to the partial pressure when you have an ideal gas, and is something else when you don't.}
\end{quote}

\noindent
This sounds facetious. But, this is the actual point of fugacity: to describe deviations from ideal-gas behavior.
That is, it's a convenient stand-in for pressure, and is often something that can be measured.

\paragraph{Activity.}
Another common approach is to use the activity:

\begin{equation}\label{eqn:activity}
\mu_i = \mu_i^\circ + R T \ln a_i
\end{equation}

\noindent where the reference state now is defined to have an activity of 1.
This is often used for liquids and related to a concentration or mole fraction with an activity coefficient, like $a_i = \gamma_i x_i$ or $a_i = \gamma_i C_i$.
When $\gamma_i$ is a constant (with respect to concentration), then the solution is considered ideal.

The reference state is defined by the one who writes the equation and it is often ``close to'' the concentration of interest.
For example, if you are examining glucose in water you might choose a reference state of pure water for \ce{H2O} and 1 M glucose for \ce{C6H12O6}.

\paragraph{Summary:}

\begin{itemize}
\item When you hear fugacity, think ``thermodynamically-rigorous partial pressure''.
\item When you hear activity, think ``thermodynamically-rigorous concentration''.
\end{itemize}

\subsection{Defining the equilibrium constant} \label{sxn:equilibrium-constant}

Now, we can come back to defining the criterion for equilibrium for a reaction at a specified temperature and pressure.
From earlier, we stated the criterion for equilibrium was related to the chemical potentials:

\[ 0 = \sum_{i=1}^{N_\mathrm{components}} \nu_i \mu_i \]

\noindent
Let's tie this to activity, which we'll recall is our proxy for concentration.
Plugging in equation~\eqref{eqn:activity}:


\[ 0 = \sum_i \nu_i \mu_i^\circ + RT \sum_i \nu_i \ln a_i \]

\[
- \frac{\sum_i \nu_i \mu_i^\circ}{RT}
= \sum_i  \ln \left(a_i\right)^{\nu_i}
= \ln \left(\Pi_i \left(a_i\right)^{\nu_i} \right)
\]

\noindent
since $\ln a + \ln b = \ln (ab)$.
Conventionally, we take the exponent of the above to define the equilibrium constant $K$ for this reaction:

\begin{equation} \label{eqn:equilibrium-activity}
K \equiv
\Pi_i \left(a_i\right)^{\nu_i} = 
\exp \left(- \frac{\sum_i \nu_i \mu_i^\circ}{RT} \right)
\end{equation}

For our example reaction \ce{A <-> B} we have $K = a_\mathrm{B} / a_\mathrm{A}$; that is, equilibrium is dictated by the ratio of their activities.
We can tie  this to concentration with the activity coefficient: $K = (\gamma_\mathrm{B} C_\mathrm{B}) / (\gamma_\mathrm{A} C_\mathrm{A})$.
In the case where the activity coefficients are constant, we get the familiar form we probably learned in first-year chemistry courses: $K' \equiv \frac{K \gamma_\mathrm{A}}{\gamma_\mathrm{B}} = \frac{C_\mathrm{B}}{C_\mathrm{A}}$; that is, the ratio of concentrations is constant.


Equivalently, we could have done our derivation based on the fugacity definition \eqref{eqn:fugacity}, in which case we would have arrived at

\begin{equation} \label{eqn:equilibrium-fugacity}
K \equiv
\Pi_i \left(\frac{f_i}{P^\circ}\right)^{\nu_i} = 
\exp \left(- \frac{\sum_i \nu_i \mu_i^\circ}{RT} \right)
\end{equation}

\noindent
Note that equations~\eqref{eqn:equilibrium-activity} and \eqref{eqn:equilibrium-fugacity} are identically the same equation; that is, they are equivalent and interchangeable.
Note when we have ideal gases we end up with the familiar ratio of pressures---\textit{e.g.}, $K = P_\mathrm{B}/P_\mathrm{A}$---instead of fugacities.

Often, we are lucky and we operate in regimes where such simplifications are possible.
For example, dilute solutions over reasonably narrow concentration ranges will have activity coefficients that are roughly constant.
Low-pressure gases typically display ideal-gas behavior.

\subsection{Free energy change of reaction}

Let's recall the criterion for equilibrium at a specified temperature and pressure.

\[  0 = \sum_i^{N_\mathrm{components}} \nu_i \mu_i \]

Before going further, recall where this came from: we are trying to minimize $G$.
Consider the reaction of glucose (A) \ce{<->} fructose (B), which we track with the variable $\xi$ (the extent of reaction, in moles.)
In this case, equilibrium is established when the chemical potentials are equal, $\mua = \mub$.
For the sake of argument, we'll examine the case where they are positive numbers, and we'll recall that $\mu_i$ is defined as the slope of the total free energy $G$ when the moles of species $i$ is increased.
Now, if we run our reaction a bit further, $\Delta \xi$, the change in free energy due to A is negative ($ \nu_i \mu_i \Delta \xi$, where $\nu_i = -1$ for A).
However, the change in free energy due to B is positive ($\nu_i = +1$ for B).
That is, $\mu_A$ is how much we are going downhill by reacting some A, but we are simultaneously going uphill by the same amount because we are creating $\mu_B$.
This is sketched in Figure~\ref{glu-fru-equil}.

\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/glu-fru-equil/glu-fru-equil.pdf}
\caption{(Hypothetical) schematic of free energy versus extent of reaction, starting with a solution with no fructose present.
\label{glu-fru-equil}
}
\end{figure}

The minimum (equilibrium) point on this plot can be found from the slope.
What is $dG/d\xi$?

\[ \frac{dG}{d\xi} = - \mu_A + \mu_B \equiv \Delta G_\mathrm{rxn}(\xi) \]

\noindent
The above is the definition of \grxn\ for this system: it is how much the system free energy changes per incremental amount of reaction; note that this is a composition-dependent quantity.
Thus, we can conclude that \grxn\ is the slope of Figure~\ref{glu-fru-equil}.
On the left-hand side of the minimum $\mu_A > \mu_B$, and $\grxn < 0$ (the reaction has a forward driving force); and of course the opposite on the right-hand side.
Again, this should match our intuition that the reaction wants to ``flow'' from high chemical potential to low chemical potential.

This tells us which way the reaction will proceed---it will always proceed towards equilibrium.
As a side question: does this mean we can only react a fixed amount of the glucose?
In practice, as the reaction proceeds we can remove fructose (the product) to keep a positive driving force.
This is why separations are so important in chemical engineering, and are often tied with reactions.

This leads to our definition of 

\[ \boxed{ \Delta G_\mathrm{rxn} = \sum_i^{N_\mathrm{components}} \nu_i \mu_i} \]

\subsection{The equilibrium ``constant'', $K$}

We'll put together the above to arrive at our final form for the equilibrium constant.
By putting the definition of the free energy change of reaction into equation~\eqref{eqn:equilibrium-activity} we get:


\begin{equation}\label{eqn:equilibrium-constant}
\boxed{ K \equiv \prod_i a_i^{\nu_i} = \exp \left\{ \frac{-\Delta G^\circ_\mathrm{rxn}}{RT} \right\} }
\end{equation}

\noindent
It's important to note the circle ($^\circ$) on $\grxn^\circ$; this means that this is a ``standard state'' free energy change, $\grxn^\circ \equiv \sum_i \nu_i \mu_i^\circ$.
As we have defined it here, this is taken at  an activity of 1 for each component.
(\textit{E.g.}, if for every species you define your activity of 1 to be at a concentration of 1 M, then $\grxn^\circ$ is the free energy change at 1 M, but equation~\eqref{eqn:equilibrium-constant} is valid at any concentration.)

This is the equilibrium ``constant'', in a thermodynamically rigorous form.
We could equivalently have done this for fugacity, from \secref{sxn:equilibrium-constant}.
If you know that you have an ideal gas or a dilute solution, you can simplify this to be in terms of concentrations or pressures; otherwise, you should use the fully rigorous form (with activity coefficients to link to concentrations, for example).

In your homework, you will be constructing this for the glucose--fructose equilibrium from tabulated values.

So, $K$ is just a shortcut to minimizing $G$. It is also something we can directly measure when $G$ or (chemical potentials) are not accessible.





\section{Intuition on activity and fugacity}

The concepts of activity and fugacity are always tricky ones, but they are all roughly the same thing: convenient ways to express the chemical potential.
Chemical potential is not typically an easy quantity to measure or directly relate to measurable quantities,\footnote{One situation where chemical potential \emph{is} easy to measure is for the Fermi level, which can be measured with a potentiostat. But this is an excption.} so the concepts of activity and fugacity exist to fill in that gap.
We'll provide more details on them individually below.

\subsection{Activity}


Activity is a concept that relates to concentration.
Before we give its thermodynamic expression, let's note a couple of limiting cases in the common definition of activity:

\begin{itemize}
\item The activity $a_i$ of a substance without any $i$ present is 0.
\item In physical chemistry classes, you will often learn that the activity $a_i$ of a pure substance $i$ is defined as  1.
(However, this is not the only reasonable choice, as we'll see below. We'll often define the $a_i=1$ state to be a 1 M solution, for example.)
\end{itemize}

\noindent
Thus, it looks a lot like mole fraction, and we often even relate it to mole fraction with

\[ a_i = \gamma_i x_i \]

\noindent where $\gamma_i$ is the ``activity coefficient'', which typically has a value of order 1, but is itself a function of the concentration.
Alternatively, we could relate it to the concentration (in mol/L) by

\[ a_i = \gamma_i C_i \]

\noindent
but note that $\gamma_i$ is different in the above two equations, as you would notice if you were given values: in the first, $\gamma_i$ is unitless, in the second it would have units of L/mol.
I like to think of $\gamma_i$ as the ``thermodynamic fudge factor''.

The activity can be related to the chemical potential by

\[ \mu_i = \mu_i^\circ(T) + RT \ln a_i \]

\noindent
and we should recall that this is the \emph{real} definition of activity.

We can now look at how the chemical potential changes with activity.
See Figure~\ref{fig:activity}.
At an activity of 1, $\mu_i = \mu_i^\circ$.
At an activity of 0, $\mu_i = -\infty$.\footnote{Perhaps more properly it's undefined; in practice it approaches $-\infty$ as the activity or concentration approaches infinite dilution. In any case, it comes out in the calculus: since the chemical potential is integrated to get the free energy, it quickly rises from $-\infty$ as the concentration rises.}
The figure should now match our intuition; a more concentrated solution has a higher chemical potential than a more dilute solution: the chemical will flow from high chemical potential to low chemical potential, analogous to gravitational or electrical potential.


\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/activity/out.pdf}
\caption{Chemical potential as a function of activity.
\label{fig:activity}
}
\end{figure}


\paragraph{Example: Water activity.}
One of the best-known examples of activity in practice is ``water activity''; which is very important in biological systems, and particularly in food and microbiology, such as fermentation and safety.
We can relate this to all kinds of fun phenomena, like assessing when cookies get stale.
We looked at an obscure reference in class:~\cite{Palou1997}.
Interestingly, water activity and relative humidity are exactly the same!
That is, if you leave a food in a room with a 50\%\ relative humidity, it will equilibrate to a wateractivity of 0.5.
Therefore, some people refer to water activity as ``equilibrium relative humidity''.

\subsection{Fugacity}

Fugacity is just like activity, but with units of pressure instead of dimensionless.
Mathematically, we can't take the logarithm of a quantity that has units, so we need to divide by some reference pressure; hence, the definition is

\[ \mu_i = \mu_i(T, P^\circ) + RT \ln \frac{f_i}{P^\circ} \]

\noindent
By inspection, the limiting cases are well-defined:

\begin{itemize}
\item When the fugacity is approaches 0, the chemical potential approaches $-\infty$.
\item When the fugacity equals $P^\circ$, the chemical potential is $\mu_i^\circ$.
\end{itemize}

In an ideal gas mixture,\footnote{As a rule of thumb, gases are ideal at atmospheric pressure or lower. If you get orders-of-magnitude higher you will deviate from ideality.} the fugacity is equal to the partial pressure.
Gases at higher pressures will deviate from this.
Just like with activity coefficients, non-ideal cases are linked to partial pressure with a \emph{fugacity coefficient}, which is defined by $f_i = \phi_i P_i$.

\paragraph{Example: Water fugacity.}
We tend to think of fugacity as being a concept for ``non-ideal gases'', but liquids have fugacities too.
In fact, fugacity is a quick way to get to get the chemical potential of liquid water.
Liquid water is always in equilibrium with water vapor, and the pressure of this vapor at equilibrium is known as the vapor pressure.
The vapor pressure of water is well below atmospheric pressure, and is within the ideal-gas regime.
Therefore, the fugacity of water vapor is equal to the vapor pressure, and, since they are in equilibrium, this \emph{fugacity of liquid water is also equal to the vapor pressure}.
This is true regardless of whether the water is pure or not: if you take sea water and measure the vapor pressure, you have the fugacity (and by extension the chemical potential) of liquid water in the ocean (at the temperature and salinity of your measurement).

\endgroup
