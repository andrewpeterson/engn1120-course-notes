\begingroup

\newcommand{\genericbalance}{\[ (\mathrm{acc}) = (\mathrm{in}) - (\mathrm{out}) + (\mathrm{gen}) - (\mathrm{cons}) \] }
\newcommand{\q}{\ensuremath{\dot{Q}}}
\newcommand{\w}{\ensuremath{\dot{W}}}
\newcommand{\f}[1]{\ensuremath{F_\mathrm{#1}}}
\newcommand{\h}[1]{\ensuremath{h_\mathrm{#1}}}
\newcommand{\n}[1]{\ensuremath{n_\mathrm{#1}}}
\newcommand{\dhr}{\ensuremath{\Delta H_\mathrm{rxn}}}
\newcommand{\xid}{\ensuremath{\dot{\xi}}}
\newcommand{\cc}[1]{C_\mathrm{#1}}
\newcommand{\dgro}{\Delta G_\mathrm{rxn}^\circ}
\newcommand{\dhro}{\Delta H_\mathrm{rxn}^\circ}
\newcommand{\dsro}{\Delta S_\mathrm{rxn}^\circ}
\newcommand{\ea}[1]{E_\mathrm{A,#1}}
\newcommand{\ceq}[1]{C^\mathrm{eq}_\mathrm{#1}}
\newcommand{\htn}[2]{H \left( T_{#1}, \{ n \}_{#2} \right) }

\chapter{Thermal effects}


\begin{figure}
\centering
\includegraphics[width=3.0in]{f/open-system/drawing4.pdf}
\caption{A generic open system. Note the walls are not necessarily rigid.
\label{fig:open-system}
}
\end{figure}


Thermal effects are incredibly important in reactions for two main reasons: (1) reaction rates and reaction equilibria are strong (exponential!) functions of temperature, so small changes in temperature can cause drastic changes in how a reactor operates, and (2) all reactions either produce or consume thermal energy (the ``heat of reaction''), and so even if we are operating isothermally we still need to figure out how much heat we'll need to add to or remove from our system.

\section{Energy balances and ``enthalpy balances''}

To address thermal effects, we need to add an energy balance to the mole balances we have been using so far.
Let's start as we would in a thermodynamics course, and write an energy balance.
See Figure~\ref{fig:open-system}; although we don't explicitly specify it, there can be a reaction taking place inside of our control volume.
Let's write a generic energy balance; this is the pattern to follow in any derivation.

\genericbalance

\[ \frac{\partial}{\partial t} \left( U + \frac{1}{2} m v^2 + mgh \right) = 
\q + \w
+ \sum_{\mathrm{in}} \dot{m}_\mathrm{in} \left(\bar{h}_\mathrm{in} + g z_\mathrm{in} + \frac{1}{2} v_\mathrm{in}^2 \right)
- \sum_{\mathrm{out}} \dot{m}_\mathrm{out} \left(\bar{h}_\mathrm{out} + g z_\mathrm{out} + \frac{1}{2} v_\mathrm{out}^2 \right)
\]

\noindent
Here, we are using $\bar{h}_\mathrm{i}$ to refer to specific enthalpy on a mass basis.
Recall from thermodynamics that this is enthalpy, instead of internal energy, because it accounts for the ``flow work'' of the fluids entering and exiting the system; see Fogler's derivation or your thermodynamic notes if this is not clear in your mind.

Let's make a number of simplifications to this.
Note that these simplifications are not always justified, so when examining a new system it's safest to start from scratch and only simplify when appropriate.

\begin{itemize}

\item Neglect all kinetic and potential energy terms.

\item Express the enthalpy flow terms on a per-mole rather than per-mass basis. That is, $\dot{m}_i \bar{h}_i = F_i h_i$, where $F_i$ is the flow rate of species $i$ in mol/time, and $h_i$ is the enthalpy per mole of species $i$.

\item Split the work term into ``shaft work'' and system volume work:
	\[ \w = \w_s - p \, \partial V/ \partial t \]

Note we can justify the minus sign: if the volume expands, then the system is doing work on the surroundings.
That is, energy is leaving the system, so the sign should be negative.

\end{itemize}

We are left with

\[ \frac{\partial U}{\partial t} = 
\q + \w_\mathrm{s} - p \, \frac{\partial V}{\partial t}
+ \sum_{\mathrm{in}} F_\mathrm{in} h_\mathrm{in}
+ \sum_{\mathrm{out}} F_\mathrm{out} h_\mathrm{out}
\]

\noindent
If we further assume that the pressure is constant:

\[ \frac{\partial (U + pV)}{\partial t} = 
\q + \w_\mathrm{s}
+ \sum_{\mathrm{in}} F_\mathrm{in} h_\mathrm{in}
+ \sum_{\mathrm{out}} F_\mathrm{out} h_\mathrm{out}
\]

\begin{equation} \label{eq:enthalpy-balance}
\boxed{ \frac{d H}{d t} = 
\q + \w_\mathrm{s}
+ \sum_{\mathrm{in}} F_\mathrm{in} h_\mathrm{in}
+ \sum_{\mathrm{out}} F_\mathrm{out} h_\mathrm{out}
}
\end{equation}

\noindent
The above is sometimes called an ``enthalpy balance'', and it's common to just write these equations in terms of enthalpy directly, as above.
(And we'll even further split up the enthalpy contributions later.)
However, this can be confusing when you are not used to it, so I typically recommend starting with a true energy balance and simplifying to enthalpy when appropriate, so that we know exactly what we're doing.


\subsection{Batch reactor, isothermal, isobaric}

Let's start by examining an isothermal, isobaric batch reactor, like you are examining in laboratory.
We'll assume a single reaction, \ce{A ->[1] B}, and an unreactive solvent S.
Our mole balances are the same as we have done previously.
Our assumptions in the above energy balance are met (no kinetic, potential energy, constant pressure) so we can start write from the enthalpy balance equation and get:

\[ \frac{dH}{dt} = \q \]

\noindent
Where we assumed no significant shaft work, and there are no flows in or out.
Let's integrate this.

\[ H_t - H_{t=0} = Q \]

\noindent where $Q \equiv \int_0^t \q dt$.
The H terms can be written as a sum of the individual species:

\[ H = \n{A} \h{A} + \n{B} \h{B} + \n{C} \h{C} \]

\noindent
then our difference in $H$ can be written as:

\[ \Delta H = \n{A,t} \h{A,t} - \n{A,0} \h{A,0} + \n{B,t} \h{B,t} - \n{B,0} \h{B,0} + \n{S,t} \h{S,t} - \n{S,0} \n{S,t} \]

\noindent
Recall that we can relate our moles via the extent of reaction variable:

\[ \n{A,t} = \n{A,0} - \xi \]
\[ \n{B,t} = \n{B,0} + \xi \]
\[ \n{S,t} = \n{S,0} = \n{S} \]

Let's assume that the specific enthalpies are \emph{not} functions of composition\footnote{Note this isn't always true; when this assumption is violated we say there is a $\Delta H_\mathrm{mixing}$.
For example, if you mix water and acetonitrile you will feel the temperature of the bottle drop. If you have a system where this occurs, you can just separate this portion out.}, but are only functions of temperature.
With this assumption, we only have one $h$ for each species and our $\Delta H$ becomes


\[ \begin{array}{ll}
	\Delta H &= (\n{A,t} - \n{A,0}) \h{A} + (\n{B,t} - \n{B,0}) \h{B} + (\n{S} - \n{S}) \h{S} \\
	&= - \xi \h{A} + \xi \h{B} \\
	&= \xi \cdot (\h{B} - \h{A}) \\
	&= \xi \Delta H_\mathrm{rxn}
\end{array} \]

We call the last term the \dhr, which is defined very similarly to $\Delta G_\mathrm{rxn}$,

\[ \boxed{ \dhr \equiv \sum_i \nu_i h_i} \] 

\noindent
Plugging back into the above gives us our solution for the total amount of heat liberated or consumed at time $t$ to keep this reactor isothermal:

\[ Q(t) =  \xi(t) \dhr  \]

\noindent
It's worth checking the signs.
We know that $\dhr < 0$ indicates an ``exothermic reaction''.
If we put in a negative number on the LHS, then $Q$ is also negative and heat must be removed from the reactor to keep it isothermal.

We can also take a time derivative of this if we prefer to have a differential form; that is, to be able to know how much heat is given off or taken in instantaneously.

\[ \q = \dhr \frac{d \xi}{dt} = \dhr r V \]

\noindent
\textit{E.g.}, if we have a first-order irreversible reaction $ r = k C_\mathrm{A}$, we'd get

\[ \q = \dhr V k C_\mathrm{A} \]


\subsection{Flow reactor, isothermal, isobaric, steady-state}

The procedure is the same on a reactor with flows.
Here, we'll only look at this briefly and table the rest of the discussion until we have actual reactor models to examine.

Let's just consider a ``black box'' flow reactor, again with the same reaction \ce{A -> B} and solvent S.
We'll assume that the velocity of the fluid going in and coming out are essentially the same, as are the heights of the ports, so that we can neglect kinetic and potential energies.
We can then start from our enthalpy balance equation \eqref{eq:enthalpy-balance} and write:

\[ \cancelto{0}{\frac{dH}{dt}} = \q + \cancelto{0}{\w_\mathrm{s}}
+ \sum_{\mathrm{in}} F_\mathrm{in} h_\mathrm{in}
+ \sum_{\mathrm{out}} F_\mathrm{out} h_\mathrm{out}
\]

\noindent where we assume steady-state operation (canceling the left-hand side) and no shaft work.

Let's examine the flow terms:


\[\begin{array}{ll}
\mathrm{In:} & \f{A,in} \h{A,in} + \f{B,in} \h{B,in} + \f{S,in} \h{S,in} \\
- \mathrm{Out:} & \f{A,out} \h{A,out} + \f{B,out} \h{B,out} + \f{S,out} \h{S,out} \\
\end{array}
	\]

\noindent
Let's again assume that the enthalpies of individual components are not functions of composition, $\Delta H_\mathrm{mixing} = 0$.

\[ \mathrm{Net:} \h{A}(\f{A,in}-\f{A,out}) + \h{B} (\f{B,in} - \f{B,out}) \]

\noindent
Let's define a continuous version of the extent of reaction variable, which we'll call $\dot{\xi}$ [mol/t], versus $\xi$ [mol] and define based off:

\[ \f{A,out} = \f{A,in} - \dot{\xi} \]

\[ \f{B,out} = \f{B,in} + \dot{\xi} \]

\noindent
So the flow terms become

\[ \h{A} \xid - \h{B} \xid = \xid (\h{A} - \h{B}) = - \dhr \xid \]

\noindent and we can plug this back into our orginal enthalpy balance to obtain:

\[ \q = \dhr \xid \]

\noindent which looks a lot like our batch case!
For now, we won't relate $\xid$ to the reaction rate, as this form will vary with the reactor geometry.

\subsection{Adiabatic batch case}

To derive this, you can use the path-independence of enthalpy.
This is assigned as a homework problem.
(Refer to the solutions for the ultimate form and derivation.)

Interestingly, you should see that an exothermic reaction can \emph{speed itself up}.
That is, we typically expect a reaction to go slower with extent of reaction, as the driving force becomes less.
But an exothermal reaction raises the temperature, and the exponential effect of this temperature rise on the rate constant overwhelms the first-order effect of the weakening concentration.

This general effect can make exothermic reactions difficult to control, and can lead to dangerous \emph{runaway reactions}.


\subsection{Shortcut enthalpy balances and path independence}

It's fairly common practice to write enthalpy balances down while skipping ahead even more steps.
If you are not comfortable with that, just write full energy balance and work your way through (and you should do that whenever encountering a new type of system).

Recall that above, we wrote true energy balances, and after some massaging these turned into expressions that were entirely in terms of enthalpy.
(You should also see this in your homework.
Specifically, in your homework you split out the temperature dependence from the composition dependence.)

\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/path-independence/path-independence.pdf}
\caption{Path independence of enthalpy.
\label{fig:path-independence}
}
\end{figure}

We rely on the ``path-independence'' property of thermodynamic quantities, as shown in Figure~\ref{fig:path-independence}.
That is, it does not matter what path we take from from point 1 to point 2, the enthalpy change is a state quantity that will remain the same.
Thus, we can take a constant-temperature change in composition (which will be the $\Delta H_\mathrm{rxn}$ and/or $\Delta H_\mathrm{rxn}$ mixing terms) and a consant-composition change in temperature (which will be heat-capacity, etc.). 

\[ \htn{2}{2} - \htn{1}{1} = \underbrace{\htn{2}{2} - \htn{2}{1}}_{\text{composition at constant temperature}} + \underbrace{\htn{2}{1} - \htn{1}{1}}_{\text{temperature at constant composition}}
\]

You will ultimately write something that has the temperature dependence on the LHS and the composition dependence on the RHS of the reaction.


\paragraph{Enthalpy balances.}
For the reason of the examples given here and in your homework, it's common to just skip the energy balance and write an enthalpy balance directly.
Here, the accumulation term refers only to accumulation of ``sensible'' and ``latent'' heat---which includes temperature rise and phase change---and a generation term is present.
That is,

\[ \underbrace{(\mathrm{acc})}_{\substack{\text{sensible/latent heat} \\ \text{in the system}}} = \underbrace{(\mathrm{in}) - (\mathrm{out})}_{\text{heat, work, and mass flows}} + \underbrace{(\mathrm{gen}) - (\mathrm{cons})}_{\text{heat of reaction}} \]

\noindent
Consider a system with no mass flows; we will leave the  treatment of flowing systems until we study actual flowing reactors.

\[ \bar{c}_P \frac{dT}{dt} = \dot{Q} + \dot{W}_\mathrm{s} + (-\dhr) \frac{d \xi}{dt}
\]

\noindent
Note here that $\bar{c}_P$ refers to the total heat capacity of the system.
We can often take this as a sum of the components, if $\Delta H_\mathrm{mix}=0$.
Note that it is a function of composition and temperature.

\section{Equilibrium constants}

So far, we have addressed how to deal with the heat produced or consumed by the $\dhr$.
However, as we noted the reaction equilibria are also strong functions of temperature, which we'll examine here.
Recall the most generic form of the equilibrium constant, written with activities:

\[ K_a = \prod_i \left(a_i\right)^{\nu_i} = 
\exp \left\{ \frac{-\dgro}{RT} \right\}
\]

\noindent
where $\dgro$ is given by (note the $^\circ$ symbol is important!):

\[ \dgro \equiv -\sum_i \nu_i \mu_i^0 \]

\noindent
Since $G = H - TS$,

\[ \boxed{ \ln K_a = - \frac{\dhro}{RT} + \frac{\dsro}{R}} \]

In principle, the above is the most generic form for how the equilibrium constant varies with temperature.
A common way to back out estimates for $\dhro$ and $\dsro$ is to assume that they are not functions of temperature; that is, the dominant temperature dependence is from the direct term $T$ in the above.
Therefore, to find how quickly $K_a$ changes with temperature we take its derivative with respect to temperature.
However, we can see that might be a little messy; and it would be a lot easier to take a derivative with respect to inverse temperature.

\[ \frac{d \ln K_a}{d \frac{1}{T}} \approx - \frac{\dhro}{R} \]

\noindent
The above is called the \emph{van't Hoff equation}.
The integrated form of it is

\[ \ln \frac{K_{a2}}{K_{a1}} = - \frac{\dhro}{R} \left(\frac{1}{T_2} - \frac{1}{T_1}\right) \]

Note we can make a van't Hoff plot, by plotting $\ln K_a$ versus $1/T$ to extract the apparent enthalpy change of reaction.
This is very similar to how we use an Arrhenius plot to extract the activation energy from rate constants.
(In fact, Arrhenius was inspired by van't Hoff.)


\section{Intuition on thermal effects}

Let's try to get a little intution for what we should expect as we raise or lower the temperature.
For this example, let's assume a single reversible reaction

\[ \ce{A <->[1][2] B} \]

\[ r_1 = k_1 \cc{A}, \,\, r_2 = k_2 \cc{B}, \,\, k_1 = A_1 e^{-\ea{1}/RT}, \,\, k_2 = A_2 e^{-\ea{2}/RT} \]

\noindent
We'll examine this reaction piece-by-piece.


\subsection{Reaction rates.}

Recall that the rate constants, $k_i$, are named strangely---they are only constant at a given temperature.
``Rate coefficients'' would probably be a better term for them, but we'll stick with calling them rate constants.
In this reaction, we are assuming Arrhenius forms for the reaction rate constants as a function of temperature.
The activation energy is always a positive number, so which can we say?

\emph{As temperature increases, reaction rates always increase.}

\emph{As temperature increases, reaction rate \textbf{constants} always increase.}


\paragraph{Exceptions.}
The exceptions to the first statement are rare, and are usually caused by some third effect (and thus not really an exception).
For example, if a reaction takes place in liquid water and we increase the temperature, at some point the water boils and we have steam; thus, the reaction may ``stop'' but it's because the environment changed.

A popular reaction to study is ``artificial photosynthesis'', in which we try to mimic natural photosynthesis in a reaction like

\[ \ce{CO2 + 8 H+ + 8 e- -> CH4 + 2 H2O } \]

\noindent
This ``half reaction'' takes place in an aqueous solution, at an electrode.
The reaction rate can decrease in liquid water as you raise the temperature---in this case, it's because \ce{CO2} becomes insoluble in water as the temperature is raised; that is, it is the $C_i$ term that is decreasing in $k C_i$.

We can think of many examples when reactions become slower as the temperature raises (another would be life---if we raise the temperature around you too much the reactions known as your life would cease), but they are typically caused by a change in phase, concentration, or competing reaction.
That is, the fundamental elementary reaction rates still continue to increase with temperature.

\subsection{Equilibrium}

As the reaction above proceeds, it continues until it reaches an equilibrium, which we know from thermodynamics we can write as

\[ \exp \left\{ \frac{-\dgro}{RT} \right\} = K = \sum_i a_i^{\nu_i} \approx \frac{[\mathrm{products}]}{[\mathrm{reactants}]} \]

\noindent
Let's first examine what we should expect for the behavior of this.
Of course, note that $T$ is in K (absolute temperature), so it's positive.
As a rule of thumb, any time you see $RT$ be sure to use Kelvin.
$R$ is of course also positive, as it's a constant.
Thus, we can look at the behavior of this with $\dgro$:
\begin{itemize}
	\item If $\dgro > 0$, $\Rightarrow$ $e^{-\#}$, $\Rightarrow$ mostly reactants
	\item If $\dgro < 0$, $\Rightarrow$ $e^{+\#}$, $\Rightarrow$ mostly products
\end{itemize}

In words,
\begin{itemize}
	\item If a reaction is downhill in free energy (``exergonic''), equilibrium favors the products.
	\item If a reaction is uphill in free energy (``endergonic''), equilibrium favors the reactants.
\end{itemize}

\paragraph{Temperature dependence.}
We examined the temperature dependence last time with the van't Hoff equation; this time let's try to be more intuitive.
It's a bit more common if we first split $G$ out with $\dgro = \dhro - T \dsro$:


\[
\exp \left\{ \frac{-\dhro}{RT} \right\} 
\cancelto{\not\approx f(T) }{\exp \left\{ \frac{-\dsro}{R} \right\} }
\approx \frac{[\mathrm{products}]}{[\mathrm{reactants}]} \]

\noindent
Now, examine the temperature dependence as a function of $\dhro$:

\begin{itemize}
	\item If $\dhro > 0$ and $T \uparrow$, (ex: $e^{-8} \rightarrow e^{-7}$), $\Rightarrow$ shift towards products
	\item If $\dhro > 0$ and $T \downarrow$, (ex: $e^{-8} \rightarrow e^{-9}$), $\Rightarrow$ shift towards reactants
\end{itemize}

\noindent
This is often understood intuitively with Le Chatelier's rule, by writing the reaction as

\[ \ce{A + heat -> B} \]

\begin{itemize}
	\item If we add ``heat'' (temperature), we shift to right.
	\item If we remove ``heat'' (temperature), we shift to left.
\end{itemize}

\noindent
For an exothermic reaction, it is just the opposite:

\[ \ce{A -> B + heat} \]

\begin{itemize}
	\item If we add ``heat'' (temperature), we shift to left.
	\item If we remove ``heat'' (temperature), we shift to right.
\end{itemize}

\paragraph{van't Hoff.}
The van't Hoff relation, which we showed earlier, formalizes the statements above.
The logic was identical, but leads to the more abstract-looking relation:

\[ \frac{d \ln K_a}{d \frac{1}{T}} \approx - \frac{\dhro}{R} \]

\subsection{Linking kinetics and thermo}
We've discussed already that kinetics and thermo are linked.
Recall that, at equilibrium,\footnote{More rigorously, we should express $K$ in terms of activity ratios, not concentration ratios. As we'll see soon when we discuss transition-state theory, \secref{sxn:tst}, rates are also best expressed in terms of activities, so this equation works out without assuming anything about ideal solutions.}

\[ k_1 \ceq{A} = k_2 \ceq{B} \]

\[ \frac{k_1}{k_2} = \frac{\ceq{B}}{\ceq{A}} \equiv K \]

\noindent
Plugging in the exponential forms for $k_1$, $k_2$, and $K$,

\[ \frac{A_1}{A_2} e^{-(\ea{1} - \ea{2})/RT}
= e^{-\dsro/R} e^{-\dhro/RT} \]

\noindent
By comparison, we can infer that in this case $\dhro = \ea{1} - \ea{2}$.
We can reason out what this might look like by drawing Figure~\ref{fig:dhro-ea-link}.
We'll find out soon how to properly link these at the microscopic scale.

\begin{figure}
\centering
\includegraphics[width=0.6 \textwidth]{f/first-PES/drawing3.pdf}
\caption{A possible link between activation energies and $\dhro$ for the example reaction?
\label{fig:dhro-ea-link}
}
\end{figure}

\endgroup
