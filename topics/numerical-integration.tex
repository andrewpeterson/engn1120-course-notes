\begingroup
\newcommand{\cc}[1]{C_\mathrm{#1}}
\newcommand{\dt}{\Delta t}

\chapter{Numerical integration} \label{sxn:numeric-integration}


\section{Differential equations}
Many problems in engineering and physics can only be expressed as differential equations.
We've seen this in reaction kinetics already, that even for the simplest of systems we end up with challenging calculus.
From last time,

\[ \frac{d}{d t} \left[ \begin{array}{c}
	\cc{A} \\
	\cc{B} \\
\end{array} \right] =
\left[ \begin{array}{c}
	-k_1 \cc{A} + k_2 \cc{B} \\
	+k_1 \cc{A} - k_2 \cc{B} \\
\end{array} \right]
\]

\noindent
Although we could find an explicit solution to this, it was a decent amount of effort (and prone to error), and it's not promising that we'd be able to replicate that for more complicated systems or geometries.
Differential equations are the norm in physics and engineering, for example:
\begin{itemize}
\item Transport: Fick's law, Fourier's law
	\[ j = -D \frac{dC}{dt}, q = -k \frac{dT}{dt} \]
\item Circuits: inductor equation
	\[ V = L \frac{dI}{dt} \]
\item Dynamics (Newton's second law)
	\[ F = m a = m \frac{d^2x}{dt^2} \]
\end{itemize}



For complicated systems the analytical solution of these can be tedious, and often impossible.
These notes are to give you a basic idea of how numerical methods are used to solve differential equations by presenting three of the most basic algorithms for this task.
Most pre-packaged solutions will use sophisticated variants of these---that are capable of dealing with large systems of equations, that use adaptive time steps, and that can automatically decide whether to use stiff or nonstiff solvers depending upon the numerics.
(This is the case with 'scipy.integrate.odeint' that we use in this class; with matlab you may need to manually specify a stiff solver in some cases.)

\section{Algorithms}

Here we'll go over three basic algorithms and how they work.

\subsection{Explicit Euler}

Consider an example initial-value problem:


\[ \frac{dy}{dt} = ty + 1\]

\[ y(t=0) = 1 \]

\noindent
The explicit Euler method is probably the most intuitive method---it just extrapolates from the current value to predict the next value, and does this over consecutive time steps $\dt$. It starts from the definition of a derivative:

\[ \left.\frac{dy}{dt}\right|_{t} \equiv \lim_{\Delta t \rightarrow 0}
		  \frac{y(t + \Delta t) - y(t)}{\Delta t} \]

\noindent then makes the approximation of using small values of $\dt$ and dropping the limit. This gives:

\[ y(t + \dt) = y(t) + \left.\frac{dy}{dt}\right|_t \dt \]

\noindent Using our example and plugging in numbers with a time step of $\Delta t = 0.1$ and a slope $\frac{dy}{dt} = t y + 1 = 1$ we get:

\begin{align*}
y(0.1) &= y(0) + \left.\frac{dy}{dt}\right|_{t=0} \dt \\
					  &= 1 + (1)(0.1) = 1.1
\end{align*}

\noindent This gives us our next point (0.1, 1.1). Noting our new slope is $ty + 1 = (0.1)(1.1) + 1 = 1.11$ we can calculate the next step as:

\[ y(0.2) = 1.1 + (1.11)(0.1) = 1.211 \]

For very small time steps this can work reasonably well, but we note that small errors in the slope estimate can propagate over time, and methods like this aren't great for stiff systems.

\subsection{Implicit Euler}
In the Explicit Euler method, we used the slope estimate from the beginning of the interval, as shown in Figure~\ref{fig:implicit-euler}.
A different option, that ends up being more robust for some systems such as stiff systems, is to use the slope at the end of the timestep interval, also shown in Figure~\ref{fig:implicit-euler}.
Mathematically,

\[ \left.\frac{dy}{dt}\right|_{t + \Delta t} \approx 
		  \frac{y(t + \Delta t) - y(t)}{\Delta t} \]

\[ y(t + \dt) = y(t) + \left.\frac{dy}{dt}\right|_{t + \Delta t} \dt \]

In our example,

\begin{align*}
		  y(0.1) &= y(0) + \left[(t + \dt) \cdot y(0.1) + 1\right] \dt \\
       &= 1 + \left[0.1 \cdot y(0.1) + 1\right] (0.1) \\
\end{align*}

This equation does not give the value of $y(0.1)$ \textit{explicitly}; instead, we have to do some algebra to solve for $y(0.1)$ in the above equation. If my algebra is correct, we get 1.111 in this case, close to, but not identical to, the value of 1.110 in the explicit method.

For a general case, the algebra will not be this easy. Thus, the algorithm uses a numerical method to solve for the value of $y(0.1)$ in the above case. So at each time step of our numerical integration, the algorithm also needs to numerically solve algebraic equations. This results in a costlier algorithm. However, some implicit methods allow larger time steps to be taken robustly, so although the time per step can increase, the number of steps may decrease to offset this.

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/numerical-integration/implicit-euler.pdf}
\caption{Slope estimates for explicit and implicit Euler methods.
\label{fig:implicit-euler}
}
\end{figure}


\subsection{Runge-Kutta}

A traditional workhorse for solving differential equations is the so-called Runge-Kutta algorithm.
It gives a good blend of accuracy and speed.
This is also a simple algorithm that can be easily coded by hand when a numerical solver is not available---for example, you could implement this in the columns of a spreadsheet.
There are many varieties of this, but the classic method is known as ``Fourth-order Runge Kutta'' (RK4) and blends four estimates of the slope in order to make its time step.
Note that this is still an explicit method.
The four estimates of the slope are listed below, and build on each other sequentially.
These are also illustrated in Figure~\ref{fig:runge-kutta}.

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/numerical-integration/runge-kutta.pdf}
\caption{The Runge-Kutta slope estimates.
\label{fig:runge-kutta}}
\end{figure}


\[ m_1 = \left. \frac{dy}{dt} \right|_{t, y} \]

\[ m_2 = \left. \frac{dy}{dt} \right|_{t + \frac{1}{2} \dt, y + \frac{\dt}{2} m_1} \]

\[ m_3 = \left. \frac{dy}{dt} \right|_{t + \frac{1}{2} \dt, y + \frac{\dt}{2} m_2} \]

\[ m_4 = \left. \frac{dy}{dt} \right|_{t + \dt, y + \dt m_3} \]

\noindent The pooled estimate of the slope is a weighted average of the four slope estimates, with more weights given to the middle values:

\[ m = \frac{m_1 + 2 m_2 + 2 m_3 + m_4}{6} \]

\noindent Finally, the time step is taken with the weighted average slope estimate,

\[ y(t + \dt) = y(t) + m \dt  \]

\subsection{Smart/adaptive methods}

The prepackaged methods use techniques similar to these, but with layers of sophistication added on.
For instance, `scipy.integrate.odeint' uses a variation known as the implicit Adams method for non-stiff steps and a backwards differentiation formula approach for stiff steps.
It also automatically figures out an optimal time step to use.
(Note that the vector of times you give to it contains the time steps it will report to you, not the ones it uses internally.)

\subsection{Example}

Imagine that in the course of solving a problem you create the following pair of ODEs:

\begin{equation} \label{eq:numerical-integrate}
	\frac{d}{dt}
\left[ \begin{array}{c}
	y_1 \\ y_2
\end{array} \right] =
\left[ \begin{array}{c}
	y_2 - 10 e^{-t} \\
	y_1 + 10 \sin 10 t \\
\end{array} \right]
\end{equation}

\noindent
We can numerically integrate this.
A numerical integrator named \mintinline{python}{odeint} exists in scipy, and we show an example of integrating from an initial value of $y_1 = 5$, $y_2 = 2$ for 2 seconds below.
The key task is to code up the function \verb'get_yprime', which returns the value of the derivatives in equation~\eqref{eq:numerical-integrate}.
The resulting plot is shown in Figure~\ref{fig:numerical-integrate}.

\noindent
File attached here.  \attachfile{f/numerical-integrate/run.py}
\pythoncode{f/numerical-integrate/run.py}


\begin{figure}
\centering
\includegraphics[width=0.7 \textwidth]{f/numerical-integrate/out.pdf}
\caption{Numerical integration example.
\label{fig:numerical-integrate}
}
\end{figure}

\noindent
Notes:

\begin{itemize}
\item \emph{Lines 6--10:} This defines the derivative; that is, it returns a list (or array) of numbers equal to the function on the right-hand side of eq.~\eqref{eq:numerical-integrate}.
Note that it has to receive as arguments \verb'y' and \verb't', where \verb'y' is an array as in eq.~\eqref{eq:numerical-integrate} and \verb't' is a scalar.
In line 7, the \verb'y' arrray is ``unpacked'' to give \verb'y1' and \verb'y2'.
In line 10, it is re-packed to give an output array of $y'$ values.
\item \emph{Line 13:} This array of times are the points at which the values of $y$ will be computed.
\item \emph{Line 14:} This line is the integration routine, which uses \verb'odeint'.
It takes in the function \verb'get_yprime', the times at which to evaluate the answer, and the values of $y$ at time zero.
\item \emph{Lines 15--16:} \verb'odeint' returns an array of size (number of time points) $\times$ (number of variables), so we can extract the two time series as shown.
\end{itemize}





\endgroup
