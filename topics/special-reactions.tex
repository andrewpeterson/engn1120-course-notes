{
\chapter{Some special reaction types}

Here, we'll look at three classes of special reactions: autocatalytic reactions, chain reactions, and enzyme kinetics.

\section{Autocatalytic reactions \label{sxn:autocatalytic}}


\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/autocat/out.pdf}
\caption{Autocatalytic reactions behave opposite to normal reactions.
\label{fig:autocat}
}
\end{figure}
 
An autocatalytic reaction is one that goes faster the greater the extent of reaction (at constant temperature).
A normal reaction goes slower the greater the extent of reaction; in the case of a first-order reaction, we get exponential decay in the reactant concentration over time.
In an autocatalytic reaction, we often get just the opposite:
Exponential growth.
See Figure~\ref{fig:autocat}.


Autocatalytic reactions are catalyzed by a product.
The most prominent example?
Life.
Let's try to right a reaction to produce humans.
Perhaps we could write:

\[ \ce{Food -> Humans} \]

But this won't work; we need some humans to start with.
So a better example is:

\[ \ce{Humans + Food -> More Humans} \]

\noindent
Because humans show up on the left-hand side of this reaction, we can guess the rate form is

\[ r = k [\text{humans}] [\text{food}] \]

\paragraph{General definition.}
With the example above, we can suppose an autocatalytic reaction is one that has a rate-form something like:

\[ \ce{A -> B} \]

\[ r = k C_A C_B \]

\noindent
Or more generically,

\[ r = k f_1(C_A) f_2(C_B^\alpha) \]

\noindent
where $\alpha > 0$.
Note that this assumes there is a little bit of B present at the beginning.
The rate increases as more B is produced, but then decreases as too much A gets consumed (and becomes limiting).

In the case of life, you need to have some product present (that is some humans present) in order for the reaction (more humans being born) to proceed.
This isn't a feature of all autocatalytic reactions, however.
Here, we give a couple of examples where that is not the case.

\paragraph{Example.}
Another example would be in the conversion of cellulose, in water at high temperatures and pressures.
It will hydrolyze to give glucose and water.
Then the glucose can break down, resulting in many things including organic acids.
This leads to acid hydrolysis of the cellulose, which speeds things up.
This may not be a good thing if we're trying to make glucose for a fermentor!

\begin{figure}
\centering
\includegraphics[width=4.2in]{f/Hill-Ill9p5.png}
\caption{Hill illustration 9.5.
\label{fig:Hill-ill9p5}
}
\end{figure}


\paragraph{Hill example.}
The Hill textbook gives an example: 5-methyl-2-oxazolidinone to make N-(2-hydroxypropyl) imidazolidinone, shown inFigure~\ref{fig:Hill-ill9p5}.
Fortunately, we can just write this as

\[ \ce{2 A -> B + C} \]

\noindent
Hill suggests that this reaction network is really

\[ \ce{2 A ->[1] B + C} \]

\[ \ce{A + B ->[2] AB} \]

\[ \ce{2 AB ->[3] 3 B + C} \]

\noindent
(Fortunately in this class, we can just write AB and leave it to our friends in chemistry to draw up what AB looks like!)

\begin{figure}
\centering
	\includegraphics[width=1.0 \textwidth]{f/hill-9-5/out.pdf}
	\caption{Integration of an autocatalytic system with and without B initially present. $(k_1, k_2, k_3)$ = (1.02, 150, 172).
\label{fig:hill-9-5}
}
\end{figure}

We integrated this numerically in Figure~\ref{fig:hill-9-5}.
On the left, we start with only A.
On the right, we start with 1 M A and 0.3 M B, and see that the rate of A consumption is much faster.

\section{Chain reactions}

Consider the gas-phase combination of hydrogen and chlorine gases to make hydrochloric acid:

\[ \ce{H2 + Cl2 -> 2 HCl} \]

\noindent
This reation has an activation energy of around 200 kJ/mol (or about 2 eV).
We can estimate this rate with TST:

\[ \underbrace{k^\ddag}_{10^{-22} \text{s}^{-1}} = \underbrace{\frac{k_B T}{h}}_{\approx 10^{13} \text{ s}^{-1}} \underbrace{e^{S^\ddag/R}}_{\approx 1 \text{ (conservative)}} \underbrace{e^{H^\ddag/RT}}_{\approx 10^{-35}} \]

\noindent That's slow.

However, this reaction can be stimulated by light.
Whether or not you've taken QM, you should know that light is quantized.
That is, it comes in units that can be counted discretely, called photons.
So one or more photon is going to react with the chemicals to make it occur.
We might think of this as

\[ \ce{H2 + Cl2 + $x$ photons -> 2 HCl} \]

\noindent
So we might think about this in terms of how many quanta of light (photons) does it take to make one molecule of product?
It turns out that one photon of light produces about one million molecules of HCl!
How does that work?
It is thought to work as follows:

\[ \ce{Cl2 ->[light] 2 Cl*} \]

\[ \ce{Cl* + H2 -> HCl + H*} \]

\[ \ce{H* + Cl2 -> HCl + Cl*} \]

\[ \ce{2 Cl* -> Cl2} \]

This is known as a chain reaction.
(The \ce{*} is used to denote a free radical species, which is very reactive.)
We can divide the steps up into initiation, propagation, and termination.

\begin{itemize}
\item
Initiation:
		\[\ce{Cl2 ->[light] 2 Cl*} \]

\item
Propagation:
		\[\ce{Cl* + H2 -> HCl + H*} \]

		\[\ce{H* + Cl2 -> HCl + Cl*} \]

\item
Termination:
		\[\ce{2 Cl* -> Cl2} \]

\end{itemize}

So if the propagation reactions happen fast, then the reaction will perpetuate until the slow termination reaction takes place.
Note that the termination reaction rate \emph{constant} need not be slow for this to occur---in fact, we'd expect this particular termination reaction to have a very fast rate constant, as both \ce{Cl*} species are very reactive.
However, if you think of having making a Cl radical, they are present in very low concentrations but they are highly reactive.
We can use the mental immage that they will react with the first thing they collide with.
The stable \ce{H2} and \ce{Cl2} molecules are present in much higher concentrations, and thus are much more likely to be the species hit.
Eventually, a couple of Cl's come together which can stop the reaction.
Of course, there are other termination reactions possible (\ce{H* + H*} or \ce{H* + Cl*}), but we just listed one here for simplicity in the upcoming algebra.

\subsection{Analysis with the PSSA.}

We can use the PSSA to analyze this, since the radical species will be short-lived and in low concentration throughout.
I find the nomenclature easier if we re-write the reactions in ``lazy nomenclature'':

\[ \ce{A + B ->[overall] C } \]

\[  \ce{B ->[i] 2 D} \]

\[ \ce{D + A ->[2] C + E} \]

\[ \ce{E + B ->[3] C + D} \]

\[ \ce{2D ->[t] B} \]

\noindent
We'll define the reaction rate as the rate of HCl production in this case; this is species ``C''.
We'll keep our lives simple by using ``ultra-lazy nomenclature'' (like $A \equiv C_\mathrm{A}$, etc.):

\[ r \equiv \frac{dC}{dt} = k_2 AD + k_3 BE \]

\noindent
Typically, it's our goal to get this down to only be a function of A, B, and C; that is, we don't want any of the free radicals present in our equations because they're reactive intermediates and are extremely hard to measure.
We can use the PSSA on H and Cl.
Let's do it on hydrogen radicals first, which is E in lazy nomenclature

\[ \frac{dE}{dt} = k_2 AD - k_3 BE \approx 0 \]

\noindent
This immediately gives us a way to clear out E in our rate equation, so let's do that:

\[ r = 2 k_2 AD \]

\noindent
Now we do the PSSA on D:

\[ \frac{dD}{dt} = 2 k_i B - k_2 A D + k_3 B E - 2 k_t D^2 \approx 0 \]


\noindent
Algebraically, we add this and the previous PSSA equation together to get

\[ 0 = 2 k_i B - 2 k_t D^2 \]

\[ D = \left( \frac{k_i}{k_t} B \right)^\frac{1}{2} \]

\noindent
And so our final rate equation becomes

\[ r = 2 k_2 \left( \frac{k_i}{k_t} \right)^\frac{1}{2} A B^\frac{1}{2}
\]

Interestingly, we are half-order in B.
Experimentally, we could envision conducting many experiments, and if we observe that we are first-order in A and half-order in B, that means that we have a mechanism that is consistent with experimental measurements.
(It doesn't mean we have the right mechanism, though! Just that it's consistent.)

Also, note that these $k$'s are lumped: so it can be difficult to experimentally distinguish them, unless you can get out of the conditions at which the PSSA is valid (or find a way to measure the free radicals).

Perhaps surprisingly, we have a dependence on $k_2$ and not $k_3$.
Can we justify this?
First, there must be some balance between $r_2$ and $r_t$, the two reactions that consume Cl.
If termination is fast relative to $r_2$, then the reaction should shut off more quickly.
We do see this.
For $k_3$, if it's very slow, this means that H builds up because there is no other way to destroy H
However, if H builds up over time this means the PSSA does not apply, and we should not have used employed the PSSA (and would not have gotten this approximate form).
On the other hand, if we make it faster and faster at some point it doesn't matter, as the rate of reaction 2 limits.
(This part is a harder to reason out.)
Also, as noted earlier we only allowed one type of termination reaction.
Presumably others are possible.


\paragraph{More chain reactions.}
Other common chain reactions include combustion, polymer growth, and lipid oxidation.
We also hear about free radicals in the body and ``anti-oxidants'' to prevent them.
Whether or not they have any effect in the body\footnote{I have no expertise on this.}, they are a class of compounds known as \textbf{scavengers} that, as the name implies, scavenge the free radicals without creating more.
Thus, they offer an alternative termination reaction that may have a faster rate due to the higher abundance of scavengers than of free radicals.


\section{Enzyme kinetics \label{sxn:enzyme}}


An enzyme is a protein that catalyzes a reaction that is thermodynamically feasible.
They are often introduced by operating via a ``lock and key'' or a ``hand in glove'' mechanism, that the morphology of the enzyme and active center are a tight and specific fit for the particular molecule that is being transformed.
(However, this viewpoint might be a little bit dated, with a binding energy being a better descriptor, and many enzymes are considered ``promiscuous''.)

Generally, an enzyme is not consumed during a reaction, but can make a particular reaction occur indefinitely.
However, enzymes do degrade over time, and there is an average number of times a particular enzyme can perform a particular reaction before degrading.

Let's look at a generic case of enzyme kinetics.
Consider the generic reaction:

\[ \ce{E + S <->[1][2] $\underbrace{\ce{ES}}_{\mathrm{C}}$ ->[3] E + P} \]

\noindent where

\begin{itemize}
\item E: enzyme
\item P: product
\item S: substrate
\item ES: enzyme-substrate complex; but we'll call this C in ultra-lazy notation.
\end{itemize}

A good example here is to think of glucose isomerase, with the complexes representing glucose in the enzyme and fructose in the enzyme.
There are in reality probably quite a few elementary transformations occuring in that!
However, let's assume these reactions as written are all first order.
We will write a system of differential equations.
Let's put them in matrix form right away:


\[
\frac{d}{d t} \begin{bmatrix} E \\ S \\ C \\ P \\ \end{bmatrix} = 
\begin{bmatrix}
- k_1 E S + k_2 C + k_3 C \\
- k_1 E S + k_2 C \\
k_1 E S - k_2 C - k_3 C \\
k_3 C \\
\end{bmatrix}
\]

\noindent
We are interested in is the rate of product formation, that is

\[ r \equiv \frac{d P}{d t} = k_3 C \]

\noindent
It's pretty tough to try to imagine measuring the enzyme--substrate complex C; and if we could, how useful this knowledge would be.
Ideally, we would like this equation only in terms of the substrate concentration ($S$) which is something very accessible and controllable, and the total enzyme concentration, since we do not know a priori the ratio of all the different forms of enzyme in the system (that is, free or bound to substrate or product).
Let's define that quantity by writing a mole balance on the total amount of enzyme:

$$ E^\circ \equiv E + C $$

Now, we need to apply some approximations in order to proceed.
We have two major ones in our toolbox, the QEA and the PSSA.
Here, the ES complex is an excellent candidates for the PSSA: they will be present in very low and fairly steady concentrations.
If we write the PSSA, we get:

\[ 0 \approx \frac{d C}{dt} = k_1 E S - k_2 C - k_3 E' \]

\noindent
Now we have some nice algebraic equations.
Before we start blindly manipulating them, let's keep our eye on the prize.
We would like to have $r$ in terms of $S$ and $E^\circ$, and it is currently in terms of $C$.
Plugging in our E equation to the PSSA one to clear out E gives

\[ 0 = k_1 S (E^\circ - C) - (k_2 + k_3) C \] 

\[ 0 = k_1 S E^\circ - k_1 S C - (k_2 + k_3) C \] 

\[ 0 = k_1 S E^\circ - (k_1 S + k_2 + k_3) C \] 

\[ C = \frac{k_1 E^\circ S}{k_1 S + k_2 + k_3} \]

\[ r = \frac{k_1 k_3 E^\circ S}{k_1 S + k_2 + k_3}
= \frac{k_3 E^\circ S}{S + \frac{k_2 + k_3}{k_1}} \]


This is our final form.

\paragraph{Aside: larger reaction.}
Note that some textbooks derive by instead assuming the internal rate constants of the enzyme turning S into P:

\[ \ce{E + S <->[1][2] ES <->[3][4] EP ->[5] E + P} \]

\noindent
which leads to the rate form

\[
r = \frac{k_3 k_5 E^\circ S}
{(k_3 + k_4 + k_5) S + \frac{k_2 k_4 + k_2 k_5 + k_3 k_5}{k_1}}
\]

This is functionally the same form, just with different lumped parameters.
In either case, we could re-express this as:

\[ \boxed{ r = \frac{r_\mathrm{max} C_\mathrm{S}}{K_\mathrm{m} + C_\mathrm{S}} } \]

\noindent
This leads to a fairly universal treatment for enzymes, which is known as the \textbf{Michaelis-Menton form}.
In it, we define two constants.
Using the first form, these constants would be:
\[ K_\mathrm{m} \equiv \frac{k_2 + k_3}{k_1} \]

\noindent and

\[ r_\mathrm{max} \equiv k_3 C_\mathrm{E}^\circ \]

We can reason out the behavior of the Michaelis--Menton equation: when $C_\mathrm{S}$ is small, $r$ is directly proportional to $C_\mathrm{S}$.
When the substrate concentration is large, $r$ just become $r_\mathrm{max}$.
This makes intuitive sense: at low substrate concentrations, there is plenty of enzyme around (so the more substrate, the more reaction); whereas at high substrate concentration, every enzyme is working as hard as it can and we hit a maximum rate set by the amount of enzyme present.
See Figure~\ref{fig:michaelis}.



\begin{figure}
\centering
\includegraphics[width=3.0in]{f/michaelis/michaelis.pdf}
\caption{Michaelis--Menton enzyme kinetics.
\label{fig:michaelis}
}
\end{figure}


{
\renewcommand{\ce}{C_\mathrm{E}}
\newcommand{\cs}{C_\mathrm{S}}
\newcommand{\ces}{C_\mathrm{ES}}
\newcommand{\cep}{C_\mathrm{EP}}
\newcommand{\cp}{C_\mathrm{P}}
\newcommand{\ceo}{C_\mathrm{E}^0}

\paragraph{Lineweaver-Burk analysis (outdated).}
A common way to analyze enzyme data to see if it gets in M--M form is with the L--B analysis.
As a warning, there are much better methods today\footnote{You should just use numerical integration and nonlinear regression.}, but it follows the historical form of trying to make everything into a straight line.
First, it notes that 

\[ -\frac{d \cs}{d t} = \frac{r_\mathrm{max} \cs}{K_\mathrm{m} + \cs} \]

\noindent which we can linearize by inverting:

\[ -\left[ \frac{d \cs}{d t} \right]^{-1} 
= \frac{K_\mathrm{m} + \cs} {r_\mathrm{max} \cs} = 
\frac{K_\mathrm{m}}{r_\mathrm{max} \cs} + \frac{1}{r_\mathrm{max}}
\]

Thus, we can see that if we plot the negative derivative versus one over the substrate concentration we should get a straight line, with obvious slopes and intercepts.
This has some major flaws though, particularly that it blows up the error on the low-concentration measurements.
Davis and Davis Examples 4.2.4--4.2.5 show this nicely.


}
