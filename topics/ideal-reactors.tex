\begingroup
\newcommand{\genericbalance}{
\[
\left( \begin{array}{c}
		  \mathrm{Acc.}\\ 
\end{array}\right) = 
\left( \begin{array}{c}
		  \mathrm{In}\\
\end{array}\right) - 
\left( \begin{array}{c}
		  \mathrm{Out}\\
\end{array}\right) + 
\left( \begin{array}{c}
		  \mathrm{Gen.}\\ 
\end{array}\right) - 
\left( \begin{array}{c}
		  \mathrm{Con.}\\ 
\end{array}\right)
\]
}
\newcommand{\cc}[1]{\ensuremath{C_\mathrm{#1}}}
\newcommand{\ca}{\ensuremath{C_\mathrm{A}}}
\newcommand{\cao}{\ensuremath{C_\mathrm{A}^0}}
\newcommand{\n}[1]{\ensuremath{n_\mathrm{#1}}}
\newcommand{\axc}{\ensuremath{A_\mathrm{xc}}}
\newcommand{\vz}{\ensuremath{V_z}}
\newcommand{\rneti}{\ensuremath{r_{\mathrm{net},i}}}
\newcommand{\tauz}{\ensuremath{\tau_z}}  % w.r.t position
\newcommand{\taut}{\ensuremath{\tau}}


\chapter{Ideal reactors}

So far, we have really only looked at batch reactors.
In reality, most industrial reactors are continuous reactors---that is, they continuously have reactants flowing in and products flowing out.
There are a number of reasons for this: for example they can be controlled with PID controllers\footnote{``Proportional-integral-derivative'' controllers.} (if the outlet concentration is a little high, you might increase the flow rate or decrease the temperature until it comes back into specification).
However, the biggest factor is throughput: it tends to be a lot more efficient to run a continuous rather than a batch reactor.
That is, you can constantly have product coming out, rather than going up, waiting, dumping, cleaning, and repeating.

I did a project at BP where we worked on large-scale reactors.
Some reactors were literally about five stories high and as wide as a house.
In one project, we focused on trying to increase the length of time they could run a process before they had to take it down for maintenance.
If I recall correctly, they had to take it down every 4 years, but they wanted to increase this by a couple of years.
That is, their process could run steadily for as long as it takes to get a bachelors degree, and this was not good enough!
Because these are massive pieces of equipment, when they do take it down it could take weeks to do maintenance (which is mostly cleaning out gunk), and more time to bring the process back up and into line.
Because of the scale, when they bring the process down they are losing out on processing (probably) millions of dollars of chemicals, and are therefore losing a lot of money.
Incidentally, it wasn't the reactor that was bringing the process down, but another piece of equipment downstream of the reactor.
The reactor kept chugging along.

There can be great efficiencies to running continuously, but some industries still run in batch mode.
They tend to be pharma and bio-reactors.
There have been a lot of efforts to bring pharmaceuticals into continuous operation, but to my knowledge they still predominantly operate with batch reactions.
There are a couple of reasons for this: (1) their profits are often centered around big blockbuster drugs with huge margins, so their cost of manufacturing doesn't matter all that much (compared to spending money on marketing this drug or discovering the next one), and (2) batch reactors offer traceability: that is, you can read the code on a bottle of pills and say which particular batch it was made from, and then look at what exactly was done on that batch.
Traceability can be an important thing in medical manufacturing.

Bio-reactors are also often in batch mode, when they are run continuously they are called ``chemostats''.
(The word ``stat'' may sound funny for something that is continuously running, as opposed to a batch reactor. But it means the chemical concentrations inside the reactor are kept constant, rather than changing over time as they would in a batch reactor.)
Here when we refer to a bio-reactor we mean one that has active microbes in it doing a process: the most prominent and relatable one is beer making.
A big reason these types of reactors run in batch mode is contamination: if another microbe somehow sneaks into your reactor, it's population can grow and takeover from whatever microbe you had originally wanted to grow.
In a batch, you can seed with the microbe you want, grow it, then clean and sanitize and start over.
We'll discuss bio-reactors later in \yettocome.

In this section, we'll discuss the ideal reactors, which we can really think of as the limiting cases.

\section{Batch reactor}

\begin{figure}
\centering
\includegraphics[width=1.5in]{f/batch-reactor/drawing.pdf}
\caption{The batch reactor.
\label{fig:batchreactor}
}
\end{figure}

By now, we are all quite familiar with the equations that govern a perfectly mixed batch reactor, but we will review this here before proceeding.
Consider a batch reactor as shown in Figure~\ref{fig:batchreactor}.
We will assume the reactor operates at constant volume (and density), constant pressure, constant temperature, and has a single, first-order reaction given by

\[ \ce{A ->[1] B}, \hspace{2em} r_1 = k_1 \cc{A} \]

\noindent
We can derive our governing differential equations by writing conservation equations on the moles of each species; that is,

\genericbalance

\[ \frac{d n_i}{dt} = \sum_j \nu_{i,j} r_j V \]

\noindent
For species A we can then write:

\[ \frac{d \n{A}}{d t} = \frac{d (\cc{A} V)}{d t} = -r_1 V \]

\[ \frac{d \cc{A} }{d t} = -r_1 \]

\[ \frac{d \cc{A}}{dt} = -k_1 \cc{A} \]

\[ \frac{\cc{A}}{\cc{A,0}} =  e^{-k_1 t} \]

\noindent
where we integrated from an initial concentration of \cc{A,0} at time 0 in the last line.

We find the concentration of A decreases exponentially; we can get the equation for \cc{B} either by the same procedure or just by noting that $\cc{A,0} = \cc{A} + \cc{B}$.


\section{Continuous reactors}

So far in this course, we have generally dealt with reactions, and when we speak of reactors we have spoken of very simple batch reactors, which are typically in isothermal or adiabatic mode and at constant pressure and volume.

As noted above, many real reactors, whether it be a fluidized catalytic cracker at an oil refinery, or a cell in a leaf performing photosynthesis, or a liver in the human body, or the reaction of atmospheric pollutants to create smog, involve some sort of continuous or semicontinuous operation.
In this section, we will introduce the basics of how continuous reactors work, focusing on the two idealized, limiting cases.
These ideal reactors are very simple and elegant, and can be used as the basis to describe real reactors--that is, as deviations or modifications on these idealized, limiting cases.

However, unlike a massless rope or a frictionless pulley in idealized physics examples, these ideal reactors can actually be constructed in practice and are often used to find rates of chemical reactions.

\section{The PFR}

We'll start with the ``plug-flow reactor'' (sometimes called a ``plug-flow tubular reactor'', PFTR).
This is sketched in Figure~\ref{fig:pfr}.
The reactor is so-named because the liquid that flows through the tube does so in ``plug-flow'', which you might remember from fluid mechanics.
This means it is perfectly mixed in the radial direction, and does not mix at all in the axial direction.
How can this be achieved experimentally?
We'll discuss that shortly.

\begin{figure}
\centering
\includegraphics[width=5.0in]{f/pfr/pfr.pdf}
\caption{The plug-flow reactor, PFR.
\label{fig:pfr}}
\end{figure}

Just as we did for the batch reactor, we can use the balance approach to find the PFR's governing equations.
Here we will take the control volume to be the area inside the dashing in Figure~\ref{fig:pfr}; that is, the volume between $z$ and $z + \Delta z$.

\genericbalance

Today we will use a steady state analysis to make our lives a little easier; that is, we'll set the left-hand side of the balance equation to zero.
(It's actually a little tricky to imagine this reactor not operating in steady state!)
For now we will assume a single reaction with rate $r$; we'll extend this to many reactions when we're done.
If the flow rate of species $i$ along the reactor is $F_i$, we write:

\[ 0 = \left. F_i \right|_{z} - \left. F_i \right|_{z + \Delta z} + \nu_i r \axc \Delta z \]

\noindent
where $\nu_i$ is the stoichiometric coefficient of species $i$, $r$ is the rate of reaction, and \axc\ is the cross-sectional area.

You have probably used similar analyses in other classes such as thermodynamics or fluid mechanics, but since this is our first such analysis here we'll make a couple of notes.
First, we are looking at the flow into and out of our small control volume at $z$ and $z + \Delta z$, and $F_i$ is defined to be positive when flowing in the increasing-$z$ direction (left-to-right in our figure); these equations work fine if the flow rate is reversed, as $F_i$ just takes on a negative sign.
Second, the reaction term represents a generation or consumption term, depending on the sign of $\nu_i$.
Note that we have not said \emph{where} in our control volume this term is evaluated: presumably the rate will be different at $z$ and $z + \Delta z$ (since concentrations, by design, will differ along the length of the reactor).
However, in our next step we are going to collapse the width $\Delta z$ into a infinitesimal width; thus we will be able to identify a single position at which we evaluate this rate.

Let's do this.
We first divide by $\Delta z$ and then take the limit of both sides as $\Delta z \to 0$:

\[ \lim_{\Delta z \to 0} 0  = \lim_{\Delta z \to 0} \left[ \frac{ \left. F_i \right|_{z} - \left. F_i \right|_{z + \Delta z}}{\Delta z} + \nu_i r \axc \right] \]

\[ 0 = -\frac{d F_i}{d z} + \nu_i r A_C \]

If the cross-sectional area \axc\ is constant for the length of the reactor, we can redefine the length coordinate in terms of the volume of reactor traveled, $\vz \equiv \axc z$.

\[ 0 = -\frac{d F_i}{d \vz} + \nu_i r \]

If there are no density variations or changes in the volume caused by reaction, we can write $F_i = C_i v$, where $v$ is the volumetric flow rate.

\[ 0 = -\frac{d C_i v}{d \vz} + \nu_i r \]

\[ 0 = -\frac{d C_i}{d (\vz/v)} + \nu_i r \]

This last step may seem a little strange, but now that we've brought these two volumetric terms together let's examine them in terms of their dimensions.
We use $[=]$ to mean ``dimensionally equal to'' and $L$ and $t$ refer to length and time, respectively.

\[ \frac{\vz}{v} \, [=] \, \frac{L^3}{L^3 t^{-1}} = t \]

So this quantity has units of time---what does that mean?
If you put a drop in the front end of the tube this is how long it takes for that same drop to reach our point $z$.
This defined as the ``residence time''; that is, the amount of time that a droplet is in residence in the reactor from the entrance to point $z$:\footnote{We will use two subtly different variables for this. When we say $\tauz$, as we did here, we refer to how long it takes to traverse to a specified point $z$ in the reactor. Later we will also use $\taut$, which means the overall residence time of the reactor.}

\[ \tauz \equiv \frac{\vz}{v} \]

\noindent
With this, we get our final form for an idealized PFR (subject to the many simplifications above!):

\[ \boxed{ \frac{d C_i}{d \tauz} = \nu_i r } \]

\noindent
or for many reactions we would get:

\[ \boxed{
\frac{d C_i}{d \tauz} = \sum_j^{N_\mathrm{reactions}} \nu_{i,j} r_j
} \]


Note that this looks just like a batch reactor!
That is, the above equations are identical except the differential is with respect to residence time $\tauz$ rather than with respect to time $t$.
If you consider it, this is rather intuitive since there is no mixing---if you place a drop of reactant at the reactor entrance, it just travels down the reactor without mixing with any other droplets; almost as if you had put a little sealed vial in the reactor and pulled it out the other end.


\paragraph{A first-order reaction.}
Let's further say that our only reaction is \ce{A ->[1] B}, with first-order rate equation $r_1 = k \cc{A}$.

\[ \frac{d \cc{A}}{d\tauz} = - k_1 \cc{A} \]

\[ \int_{\cc{A,0}}^{\cc{A}} \frac{d\cc{A}}{\cc{A}} = -k_1 \int_0^{\tauz} d \tauz \]

\[ \cc{A}(\tauz) = \cc{A,0} e^{-k_1 \tauz} \]


At $\tauz=0$, we are at the reactor entrance and the concentration of A is just that of the feed, \cc{A,0}; we have an exponential decay with time through the reactor.
We see this looks just like the batch reactor case.
Therefore, a PFR is a continuous reactor that replicates the behavior of a batch reactor!

\paragraph{Aside: designing for plug flow.}
How do we design a reactor to approximate plug flow conditions?
A fluid would flow like a ``plug'' when there is absolutely no friction at the wall; that is, the fluid just slips right by it.
But this is counter to one of the basic assumptions in fluid mechanics: the no-slip boundary assumption commonly used at walls.
Certainly, in laminar flow a velocity profile will emerge, with the highest velocity in the center of the pipe and zero velocity at the walls.

It turns out that plug flow is a good assumption if you have turbulent flow, because then things are well-mixed.
This should occur when Reynolds numbers are high (roughly several thousand); the Reynolds number is a non-dimensional quantity:

\[ \mathrm{Re} \equiv \frac{\rho u D}{\mu}  \]

\noindent
which is the ratio of inertial forces to viscous forces.
(Note here that $\mu$ is viscosity, not chemical potential! $u$ indicates velocity, as we're be using $v$ and $V$ elsewhere for volumetric terms. $D$ is the diameter of the pipe, and $\rho$ is the density.)

How do we achieve this in practice?
First, let's note that we probably don't have much control over the $\rho$ and $\mu$ values of our fluid; that is, if we want to run our reaction in water, we're stuck with water's density and viscosity.
This leaves us the velocity and diameter, $u$ and $D$, to play with as design variables.
From the Reynolds number equation, at first glance it looks like we should increase the diameter $D$ to have turbulent flow, but let's check this.

For the moment, let's say we want a certain volumetric flow rate, $v$.
We can link this to the velocity (for the plug-flow case) with

\[ u \axc = v \]

\[ u = \frac{v}{\axc} = \frac{v}{\frac{\pi}{4} D^2} \]

\noindent
Putting this back into the Re equation gives

\[ Re = \frac{\rho v D}{(\pi/4) \mu D^2} = \frac{4}{\pi} \frac{\rho v}{\mu D} \]

So actually, we should \emph{decrease} the diameter (at a constant volumetric flow rate) in order to get turbulent flow; this looks opposite of the original Re equation, but it is due to the velocity increasing with the square of the diameter, and the square overwhelms the first power of $D$.


\section{The CSTR}

We have just discovered that the tubular PFR is a continuous reactor that acts like a batch reactor.
What if I took a batch reactor and operated it continuously, would this do the same thing?

This is the continuous stirred-tank reactor (CSTR), which is essentially a batch reactor with an inlet and outlet.
See Figure~\ref{fig:cstr}.
As we will see, it is the other idealized limit of chemical reactors.


\begin{figure}
\centering
\includegraphics[width=2.5in]{f/cstr/cstr.pdf}
\caption{A CSTR.
\label{fig:cstr}}
\end{figure}

Some of the unique aspects of this reactor are easier to see with a specific reaction.
Let's derive the equations for a CSTR first for a specific reaction, with \ce{A ->[1] B}, and $r_1=k_1 \cc{A}$.
(We'll come back and write the general form equations later.)

\genericbalance

\[ \frac{d \n{A}}{dt} = F_\mathrm{A}^\mathrm{in} - F_\mathrm{A}^\mathrm{out} - r_1 V \]

\noindent and likewise for B.
Let's make two assumptions: (1) the reactor is operating at steady state ($d\n{A}/dt=0$), (2) no density/volume change in the reactor, that is, $v$ in and out are the same.
Then we can re-write our previous equation as:

\[ 0 = v \cc{A0} - v \cc{A} - k_1 \cc{A} V \]

\noindent
Perhaps the most important thing to note is that the concentration of the outlet stream is identical to the composition inside; this was also shown in the figure.
That is, there is only one composition inside since it is perfectly mixed, and this will be the composition you get if something flows out.

Thus, no calculus is needed for the steady-state outlet concentration, we just solve for \cc{A}:

\[ \cc{A}
= \frac{v \cc{A,0}}{v + kV}
= \frac{\cc{A,0}}{1 + kV/v}
= \frac{\cc{A,0}}{1 + k \taut}
\]

\noindent
where we have substituted $\taut \equiv V / v$; note that $\taut$ here is defined based on the total volume of the CSTR---there is no equivalent of $\tauz$ in the CSTR case, as there is no spatial variation within the reactor.
(Although we will come back to exploring the characteristics of $\tau$'s in general in \yettocome.)
We can check that the equation makes sense by thinking about the limiting values of $\taut$.
If $\taut$ is zero, our reactor size is also zero and our equation correctly gives us that $\cc{A} = \cc{A,0}$.
Conversely, if $\taut$ is infinite, our reactor size is infinite and our equation correctly predicts that $\cc{A} = 0$, which is correct in this case where no reverse reaction is included.


\paragraph{PFR vs CSTR.}
Let's compare the equations for this simple first-order reaction in both idealized reactor types:


\[ \begin{array}{ll}
	\text{PFR:} & {\displaystyle \frac{\cc{A}}{\cc{A,0}} = e^{-k_1 \taut} } \\[2em]
\text{CSTR:} & {\displaystyle \frac{1}{1 + k_1 \taut} } \\
\end{array} \]


First, note that they both have identical limiting behavior at $\taut=0$ and $\taut=\infty$.
But at real values of $\taut$, which gives a better conversion?
The PFR does, as we show in Figure~\ref{fig:cstr-vs-pfr}.

Why is this?
(Because reaction rate is proportional to the concentration, and the CSTR is operating entirely at the exit concentration. See the right panel.)
We'll make more formal comparisons for reactions of different orders in \yettocome.

\begin{figure}
\centering
\includegraphics[width=0.9 \textwidth]{f/cstr-vs-pfr/out.pdf}
\caption{Conversion for a first-order reaction in a CSTR and PFR.
\label{fig:cstr-vs-pfr}
}
\end{figure}




\paragraph{General derivation for the CSTR.}
In general, we can write the balance on such a reactor as

\genericbalance

\[ \frac{\partial n_i}{\partial t} = F_i^\mathrm{in} - F_i + \sum_j^{N_\mathrm{rxns}} \nu_{i,j} r_j V \]


\[ \frac{d}{d t} \left(C_i V \right) = v^0 C_i^0 - v C_i + \sum_j^{N_\mathrm{rxns}} \nu_{i,j} \cdot r_j \left( \left\{C_k \right\}, T \right) \cdot V \]

The above is the complete time-dependent form; if we approach steady state we can set the left-hand side to zero.
Note that there is one such equation for each chemical species $i$, but in principle each equation includes the concentration of all other species, which we have noted by the inclusion of $r_j ( \{ C_k \}, T)$ where we are using the notation $\{ C_k \}$ to indicate the set of all other chemical species' concentrations.

Perhaps the key thing to note is that there are two types of concentrations here: the inlet and the bulk.
(The outlet concentration is equal to the bulk.)
The only place the inlet shows up is in the inlet flow, the rest are these combined outlet / bulk flows.

As we will see later (\yettocome), solving for the steady state solution is just algebra---but it can sometimes be harder to solve the system of (non-linear) algebraic equations and it is simpler to just integrate the full differential equations until steady state behavior is observed.

\section{Comparing the behavior}

Now, let's systematically compare the behavior of these two limiting ideal reactors, across reactions of different order.
We'll make this comparison for the constant-volume, constant-temperature, steady-state case.

\paragraph{CSTR.}
The CSTR represents the ``perfectly mixed'' extreme.
Analysis by standard balance equations in the steady-state, constant-volume case yields the \textit{algebraic} equation:

\[ C_i = C_i^0 + \taut \rneti \]

\noindent
There is one such expression per species.
I am defining \rneti\ as the net reaction rate including all reactions:

\[ \rneti \equiv  \sum_j^{N_\mathrm{rxns}} \nu_{i,j} \, r_j \]

\paragraph{PFR.}
The PFR represents the ``perfectly unmixed'' extreme.
Analysis by standard balance equations in the steady-state, constant-volume case yields the \textit{differential} equation:

\[ \frac{d C_i}{d \tauz} = r_\mathrm{net} \]

\noindent
Recall that \tauz\ refers to the distance down the reactor that has been converted to units of time; whereas \taut\ for the CSTR referred to the overall residence time of the reactor.
\taut\ only shows up for the PFR when we integrate: it is used as a bound on the integral.

\paragraph{Comparing the reactors for simple reactions.}
If the two reactors have identical \taut's---say 500 s---will they have identical behavior?
Let's look at this for common reaction orders.

\paragraph{0th order.}
\ce{A -> B}, $ r = k $.
\begin{itemize}
\item CSTR.
\[ \cc{A} = \cc{A,0} - k \taut \]
\item PFR.
\[ \frac{d \cc{A}}{d\tauz} = -k \]
\[ \int_{\cc{A,0}}^{\cc{A}} d \cc{A} = \int_0^{\taut} -k \, d\tauz \]
\[ \cc{A} - \cc{A,0} = k \, \taut \]
\end{itemize}


We get the same result in either case!
This may not be terribly surprising as a 0th-order reaction does not depend on concentrations.

\paragraph{1st order.}
\ce{A -> B}, $ r_\mathrm{net} = -k \cc{A} $.

\begin{itemize}
\item CSTR.
\[ \cc{A} = \cc{A,0} - k \,  \taut \, \cc{A} \]

\[ \cc{A} = \frac{ \cc{A,0}}{1 + k \taut} \]

\item PFR:

\[ \frac{d \cc{A}}{d\tauz} = -k \cc{A} \]

\[ \int_{\cc{A,0}}^{\cc{A}} \frac{d \cc{A}}{\cc{A}} = -k \int_0^{\taut}d \tauz \]

\[ \ln \frac{\cc{A}}{\cc{A,0}} = -k \taut \]

\[ \cc{A} = \cc{A,0} e^{-k \taut} \]

\end{itemize}

As we saw before, these are quite different, and the PFR outperforms the CSTR under these conditions---and the reason gives us intuition on the operation of these ideal reactors.
We should, however note that a comparison at constant $\taut$ helps us to understand the reactor behavior but would not be enough to choose one over the other---many other factors will enter the design choice.

\paragraph{2nd order.}
\ce{A -> B}, $r_\mathrm{net} = k \cc{A}^2$.

\begin{itemize}
\item CSTR.

\[ \cc{A} = \cc{A,0} - k \taut \cc{A}^2 \]

\[ 0 = k \taut k \cc{A}^2 + \cc{A}  - \cc{A,0}  \]

\[ \cc{A} = \frac{- 1 \pm \sqrt{1 + 4 k \taut \cc{A,0}}}{2 k \taut} \]

Which root do we take?
We can rationalize that the square-root term is always greater than 1; therefore, it must be the positive root.

\[ \cc{A} = \frac{- 1 + \sqrt{1 + 4 k \taut \cc{A,0}}}{2 k \taut} \]

What are the units on this?
They come from $k \taut$.
For a second-order reaction, $k$ has units of, (for example) L/mol/s, so $k \taut$ has units of L/mol, and inverting this we get the expected mol/L.

Let's check the limiting cases.
What happens if we plug in $\taut = 0$?
At first glance it looks like we get infinity!
But actually we get 0/0, which is undefined.
This suggests that we should use L'H{\^o}pital's rule to find an expression we can evaluate.
Recall that this rule states:

\[ \lim_{x \rightarrow 0} \frac{f(x)}{g(x)}
= \lim_{x \rightarrow 0} \frac{f'(x)}{g'(x)} \]

Evaluating this for our case gives

\[ \cc{A}(\taut\rightarrow0) = \frac{ \frac{1}{2} (1 + k \taut \cc{A,0})^{-1/2} \, 4 k \cc{A,0}}{2 k} = \cc{A,0} \]

This makes sense---if there is no reactor, there is no conversion.

For an infinitely large reactor, we can use the original form and find:

\[ \cc{A} (\tau \rightarrow \infty) = \frac{\infty^{1/2}}{\infty} = 0 \]


\item PFR.

\[ \frac{d \cc{A}}{dt} = -k \cc{A}^2 \]

\[ \int_{\cc{A0}}^{\cc{A}} \frac{d \cc{A}}{\cc{A}^2} = -k \int_0^\taut dt \]

\[ \left[- \frac{1}{\cc{A}} \right]_{\cc{A0}}^{\cc{A}} = - k \taut \]

\[ \left[- \frac{1}{\cc{A}} + \frac{1}{\cc{A0}} \right] = - k \taut \]

\[ \frac{1}{\cc{A}} = \frac{1}{\cc{A0}} +  k \taut \]

\[ \cc{A} = \frac{1}{\frac{1}{\cc{A0}} +  k \taut} \]

Plugging in $\taut=0$ we get \cc{A0}, plugging in $\taut=\infty$ we get 0. This is good.

\end{itemize}

\section{Transient analysis}

So far, we have analyzed our reactors under the assumption that they are in steady state.
Plant operators might say the system has ``lined out''---meaning if you plot the measurable quantities (like flow rates, concentrations, temperatures, etc.) you see nothing but a flat line on the screen.
See Figure~\ref{fig:steady-state}.

Next we're going to look at the behavior of these reactors under transient conditions, such as when they are first started up or the operating conditions change.

\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{f/steady-state/drawing.pdf}
\caption{What the measurable quantities of a reactor might look like as it is lines out. These might be output concentrations, flow rates, temperatures, etc. \label{fig:steady-state}}
\end{figure}

\subsection{PFR\label{sxn:pfr-transient}}

For the PFR, a transient analysis is of limited use, at least at the stage we are at today.
Because there is no axial mixing, we know that whatever we put in we will get out---in reacted form---$\taut$ seconds later.

Let's envision a startup procedure for a reaction that occurs in water.
We could initially line out our reactor with pure water, making sure we achieve the expected flow rate, temperature, pressure, etc.
At some time we switch the feed to our reactants; we'll designate this as $t=0$.
By intuition, we know that our concentrations at the exit should come out at their steady-state values ($C_i^\mathrm{SS}$) after $\tau$ seconds:

\[ C_i = \left\{ \begin{array}{ll}
0, & t < \taut \\
C_i^\mathrm{SS} & t \ge \taut
\end{array} \right. \]

\noindent
(For the example of the first-order reaction we examined earlier, $\ca^\mathrm{SS} = \cao e^{-k_1 \taut}$.)

There is not much point in doing a mathematical analysis; we won't gain any additional insight---at least in the ideal limit.
However, we'll come back to performing a transient analysis on the PFR when we incorporate dispersion effects, as discussed in~\secref{sxn:dispersion}.

\subsection{CSTR}

The CSTR is more interesting in transient analysis.
Let's assume a single, first-order reaction for now as shown in Figure~\ref{fig:cstr}.
We'll again use the mental picture of a reaction taking place in water, and we'll assume the reactor is initially filled with pure water---that is, no reactant or product.
We'll also assume there is no volume change within the reactor\footnote{That is, the density of the fluid is the same at the inlet and the outlet, and the reaction is not causing an increase in volume (as it might for combustion, for example).}, so that $v$ describes both the inlet and outlet volumetric flow rates.
We'll write a mole balance on A:

\genericbalance

\[ \frac{d }{dt} \big( \ca V \big) = v \cao - v \ca - k_1 \ca V \]

\noindent
Because the reactor was already full, and the reactor volume does not change, we can pull $V$ out of the derivative and convert to $\tau$.
(Note that if the reactor were initially empty you could not do this!)

\[ \tau \frac{d \ca }{dt}  =  \cao -  \ca - k_1 \tau \ca \]

\noindent
We will integrate from conditions where the reactor initially has no A:

\[ \int_0^{\ca} \frac{d \ca}{\ca + k \tau \ca - \cao} = - \frac{1}{\tau} \int_0^t dt \]

\[ \ln \frac{\ca + k \tau \ca - \cao}{-\cao} = - t/\tau \]

\[ \ca + k \tau \ca - \cao = - \cao e^{- t/\tau} \]

\[ \ca + k \tau \ca = \cao (1  - e^{- t/\tau}) \]

\[ \frac{\ca}{\cao} = \frac{1 - e^{-t / \tau}}{1 + k \tau} \]

This is a much more interesting case than that of the PFR.
Let's look at the limiting cases:
When we initially start up, we have no $\ca$ coming out before time zero.
Starting at time zero, it is 0, but starts exponentially rising.
At time infinity, we get $ 1 / (1 + k \tau)$, which is our steady state solution.

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{f/transient-cstr-pfr/out.pdf}
\caption{An example of the transient behavior of an ideal PFR and CSTR (with $k \taut$ = 1). \label{fig:transient-cstr-pfr}}
\end{figure}

The transient behavior of both the CSTR and PFR are shown in Figure~\ref{fig:transient-cstr-pfr}.
Obviously, it is somewhat arbitrary when we declare the CSTR to be ``lined out'' (in steady state).
If we want to base this on an analysis such as this, you typically would say that the CSTR reaches $x$ percent of its steady-state output concentrations after $y$ multiples of its residence time.
For a first-order reaction, you should be able to quickly work out that to reach 99\%\ of the steady state values, then we need to pass through about 4.6 times the volume of the reactor.
People often use rules-of-thumb that you need to wait 5--10 times the residence time before collecting steady-state measurements.

For cases of different reaction orders and reactions with thermal effects, this can be quite a bit larger.
There also can be slower-responding parts of the system that you might not have accounted for in your analysis: for example, say your reactor has thick walls and a high thermal mass, it might take a long time for the reactor to respond to the temperature of the reactants.
If it's important to know when you reach steady state---such as taking kinetics measurements or assessing changes to your process---it's a good idea to make measurements of your actual system.
We will discuss this in the next session.


\endgroup
