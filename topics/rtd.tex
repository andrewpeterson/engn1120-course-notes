\begingroup
\newcommand{\tauz}{\ensuremath{\tau_z}}  % w.r.t position
\newcommand{\taut}{\ensuremath{\tau}}
\newcommand{\ca}{\ensuremath{C_\mathrm{A}}}
\newcommand{\cao}{\ensuremath{C_\mathrm{A}^0}}
\newcommand{\genericbalance}{
\[
\left( \begin{array}{c}
		  \mathrm{Acc.}\\ 
\end{array}\right) = 
\left( \begin{array}{c}
		  \mathrm{In}\\
\end{array}\right) - 
\left( \begin{array}{c}
		  \mathrm{Out}\\
\end{array}\right) + 
\left( \begin{array}{c}
		  \mathrm{Gen.}\\ 
\end{array}\right) - 
\left( \begin{array}{c}
		  \mathrm{Con.}\\ 
\end{array}\right)
\]
}

\chapter{Residence time distributions and tracers}

The PFR and CSTR are classic acronyms that everyone with a chemical engineering bachelors degree knows.
However, if we could re-name them today, I think I'd vote for the ``perfectly mixed'' and the ``perfectly unmixed'' flow reactor, because these are the extremes that these two ideal reactors operate at.

A useful way to think about these, as well as real, reactors is how a typical drop of fluid flowing through them behaves.
The simplest way to think of this is with a ``tracer'': for now, consider sneaking a drop of food coloring into the fluid flowing into the reactor, and watching the exit to see when it comes out.

In a plug-flow reactor, if we put in a drop of color at time zero, the drop emerges from the other end of the reactor exactly at time $\taut$.
The PFR is the perfectly unmixed reactor.

The continuous stirred-tank reactor (CSTR) operates at the opposite end of the spectrum: the perfectly mixed extreme.
So when the drop of color enters the reactor, it is immediately mixed to a uniform, dilute color everywhere in the reactor.
Assuming the color is strong enough that we can see it even when it's diluted, we'll immediately start seeing it coming out of the exit at time zero.
Since we are continuously adding color-free fluid to the reactor entrance after time zero (and pulling color out the back end), the color's concentration will start to drop and will decay towards zero concentration.
A good guess would be that the color's intensity will exponentially decay with time.

A sketch of the expected behavior of these two reactor extremes is shown in Figure~\ref{fig:rtd}.

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{f/rtd/out.pdf}
\caption{The expected behavior of a drop of dye in a PFR and a CSTR.
\label{fig:rtd}}
\end{figure}

Studying such ``residence time distributions'' can give us a good bit of insight into how our reactor behaves, and measuring such residence time distributions in real reactors can help us understand the behavior inside of our reactor.
As an example: if we are trying to build a CSTR or PFR (for example to measure the rate of a reaction) we can do the exact experiment we describe above, and compare the real residence time to that we expect from our idealized reactor to analyze how closely we approach perfectly mixed or perfectly unmixed behavior.

In this section, we'll look at how to formally analyze residence time distirbutions in both idealized and real reactors; these concepts will be useful in future chapters when we study reactors (and reactor models) that operate between these two extremes.

\section{Probability distributions}

A residence-time distribution is a type of probability density function---which in turn is just a histogram in continuous space.
If you're comfortable with probability distributions, feel free to jump to the next section.
Here, we'll give some basic intuition on probability distributions by starting with our friend the histogram.

\subsection{Histograms}

I guess that every college student is familiar with histograms from looking at the histograms that their professors show regarding grade distributions.
An example histogram from a kinetics exam is shown on the left in Figure~\ref{fig:histogram}.
Let's ask some questions of this histogram:

\begin{figure}
\centering
\includegraphics[width=1.0\textwidth]{f/histogram/out.pdf}
\caption{A histogram of test scores on a kinetics exam. The right plot shows the same plot, but as a probability density. \label{fig:histogram}}
\end{figure}

\begin{itemize}
\item \textbf{What is the probability of scoring above 80\%?}\footnote{That is, if we pick a random student, what is the probability that their score is above 80\%? Your individual probability would be different than this, but that's a topic for Bayesian statistics!} 

Clearly, it is just 2/27, or about 7\%.

Let's try to write this as a formula.
Let's call the mean score of each bin $x_i$ and the number of events in that bin $E_i$; then we could write:

\[ \frac{\sum_i^{x_i > 80} E_i}{\sum_i E_i} \]

where the top sum indicates we're summing values over 80, and the bottom sum is over all bins.

\item \textbf{What is the probability of being between 30\%\ and 70\%?}

It is (3 + 7 + 2 + 3) / 27, or about 56\%.
We could write this formulaically as

\[ \frac{\sum_i^{x_i > 30} E_i - \sum_i^{x_i > 70} E_i}{\sum_i E_i} \]

\item \textbf{What is the mean score?}

We don't know precise values, so we'll have to assign each bar of the histogram the score at the midpoint; \textit{e.g.}, 15 for the 10--20 score range, 25 for the 20--30 score range, etc.
Then the mean should be

\begin{equation}\label{eq:histogram-mean} \langle x \rangle = \frac{2 \times 15 + 3 \times 25 + 3 \times 35 + \cdots}{27} = 51.3 \end{equation}

We can write this more compactly: let's call the bin's score $x_i$ and the number of events $E_i$:

\[ \langle x \rangle = \frac{ \sum_i x_i E_i}{\sum_i E_i} \]

\item \textbf{What is the variance?}

The variance tells us how far, on average, a point is from the mean, where the distance of a point from the mean is $(x_i - \langle x \rangle)^2$.
Therefore, we can calculate the variance as

\[ \mathrm{Var}(x) = \frac{\sum_i (x_i - \langle x \rangle)^2 E_i}{\sum_i E_i} \]

\[ \mathrm{Var}(x) =
\frac{ (15 - 51.3)^2 \times 2 + (25 - 51.3)^2 \times 3 + \cdots}{27} = 453 \]

You're probably more familiar with the standard deviation, which is the square root of the variance,\footnote{Some definitions of the standard deviation divide by $N - 1$ instead of by $N$, but that's a topic for a statistics class. For us, we'll just divide by $N$.} which gives 21.3.

\end{itemize}

\noindent
Note that in every one of the calculations above we divided by 27, which was our number of students ($N$).

\subsection{Probability density functions}

Probability density functions are essentially the continuous version of a histogram.
You're certainly familiar with the normal (Gaussian) distribution, but probability density functions can take on any shape.
Typically, these PDFs, as they are called, are ``normalized'' so that the total area under the curve integrates to 1; this is essentially the same as dividing our histogram by the total count in the example above.

As a continuous function, the height of the curve does not correspond to a probabiilty: it is only when you integrate over a specified width that you obtain a probability.
Because of this, the $y$ axis has dimensions inverse that of the $x$ axis.
For example, in our residence-time distribution plots, the units of the $x$ axis might be minutes, in which case the units of the $y$ axis will be min$^{-1}$.

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{f/pdf/drawing.pdf}
\caption{A rough sketch of a probability density function. \label{fig:pdf}}
\end{figure}

We can ask a similar set of questions of our PDF as we asked of our histogram.

\begin{itemize}
\item \textbf{What is the probability of being above a point $x_1$?}

We'll pick a point $x_1$ on the $x$ axis, and ask what is the probability of sampling a point above $x_1$.
The logic is really identical to our histogram example, except we integrate instead of sum; that is, the probability is

\[ P(x > x_1) = \frac{\int_{x_1}^{\infty} p(x) \, dx}{\int_{-\infty}^{\infty} p(x) \, dx} =  \int_{x_1}^{\infty} p(x) \, dx \]

That is, it is the area under the curve at points greater than $x_i$, divided by the total area under the curve.
Since the curve is typically normalized so that $\int_{-\infty}^{\infty} p(x) \, dx = 1$, we can remve the denominator; however, you should make sure that any PDF you use is actually normalized before leaving out the denominator.
In the rest of these examples, we'll assume the curve is normalized for simplicity.

Also, we can see how the units work out in this case.
$dx$ will have the same dimensions as $x$ (when integrated); in our RTD example this will typically be dimensions of time.
Therefore, $p(x)$ must have units of inverse time in order that the probability itself has no dimensions.

\item \textbf{What is the probability of being between points $x_1$ and $x_2$?}

For a normalized PDF, this will simply be a matter of changing the bounds of integration:

\[ P( x_1 {<} x {<} x_2 ) = \int_{x_1}^{x_2} p(x) \, dx \]

\item \textbf{What is the mean value of $x$?}

Here is where the insight from the histogram is useful to build our formula.
In the histogram case, we multiplied the height of the curve at each value of $x$ by that value of $x$; it turns out to be exactly that in the continuous case as well:

\[ \langle x \rangle = \int_{-\infty}^{\infty} x \, p(x) \, dx \]

Note the similarity to the histogram's mean equation, \eqref{eq:histogram-mean}; they would look even more similar if we included the normalization term in the current equation.

\item \textbf{What is the variance?}

It turns out that the mean and variance are known as ``moments'' of the distribution; we'll cover that generically in the next section.

\end{itemize}

\subsection{Moments of a distribution}

The mean and variance (which, in turn, is the square of the standard deviation), are referred to as ``moments'' of a distrubtion.
Mathematically, the moments of a distribution $p(x)$ are given as

\[ \mu_n = \int_{-\infty}^{+\infty} (x - c)^n \, p(x) \, dx \]

\noindent
where we are assuming that $p(x)$ is normalized.\footnote{As noted earlier, normalized means $\int_{-\infty}^{\infty} p(x) \, dx = 1$.}

To find the mean, we plug in $n=1$ and $c=0$; the mean is often referred to as the ``first moment'' (or the ``first raw moment'', where raw indicates $c=0$).
This gives

\[ \langle x \rangle \equiv \int_0^{\infty} x \, p(x) \, dx \]

The variance is known as the ``second moment about the mean''.
By second moment, we mean $n=2$, and by ``about the mean'' we mean $c=\langle x \rangle$.
Thus, the variance is

\[ \mathrm{Var}(x) \equiv \int_{-\infty}^{+\infty} (x - \langle x \rangle)^2 \, p(x) \, dx \]

\noindent
We can again see that this is very analagous for what we used for the variance in a histogram.

There are also well-known higher moments, such as \emph{skew} (a 3rd moment) and \emph{kurtosis} (a 4th moment).

\section{Tracers and residence time distributions}

At the beginning of this chapter, we reasoned on what the outlet flow would look like if we inject a ``tracer''; that is, some unreactive chemical that we can easily detect in the outlet flow.
Such tracers are used in practice, and their use is not confined to chemical reactors.
A couple of examples follow:

\begin{itemize}

\item Groundwater tracing. As an example, I grew up in an area of the country where sinkholes were common; that is, large holes in the ground that opened when water ate away limestone.
Unfortunately, at some point in history it seemed like a good place for local residents to drain their sewer pipes to, as well as discard old junk.
The University did tracer studies, where they poured dye into the sinkholes and monitored where and when it came back to the surface; they found the dye in a spring not far from the town.
This was used as evidence to suggestt the building of a municipal sewer system.
See Figure~\ref{fig:sinkhole-tracer} for an example of the tracer emerging at the spring.

\item Pharmacokinetics involves, in part, the study of how drugs traverse through the body; for example, they may use radiolabeled drugs and watch the output in urine and feces.
This gives an idea of how long they can expect a drug to be active for, as well as the large variations in concentration that they can expect after different means of administering the drug.
Figure~\ref{fig:pharmacokinetics-tracer} shows such an example.
\end{itemize}

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{f/sinkhole-tracer/Fig3.png}
	\caption{Concentration of a fluorescent tracer in a spring not far from where I grew up. The tracer was dumped into a sinkhole in the town. Figure from reference~\cite{Alexander1986} \label{fig:sinkhole-tracer}}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{f/pharmacokinetics-tracer/Fig3.png}
	\caption{The concentration of a drug (mebendazole) in a person after an oral dose. (I) indicates the drug itself, while the other symbols indicate metabolites (breakdown products) of this drug. From reference~\cite{Dawson1985}. \label{fig:pharmacokinetics-tracer}}
\end{figure}

Do these examples look like PFRs or CSTRs?
Perhaps something in-between?
We'll also discuss such intermediate models in coming chapters.

\subsection{Mathematics of tracers}

Mathematically, we often treat tracers as being in one of two limits: an instantaneous pulse injected at a discrete point in time, or a step change in concentration that occurs instantaneuously.
Here, we'll focus primarily on the former.

Mathematically, this is defined as a ``Dirac delta'' function.
A dirac delta is essentially a pulse that is on at one particular exact time, and off at all other times.
We can define it---in part---as:

\begin{equation}\label{eq:dirac-instant}
\delta (t - t')
= \left \{ \begin{array}{ll}
0, & t \neq t' \\
\infty, & t = t' \\
\end{array} \right.
\end{equation}


\noindent
where the pulse enters at time $t'$.
This looks like a challenging function to deal with---since it takes on an infinite volume, for an infinitismal amount of time.
How would we even quantify how much total tracer would be injected if the concentration is infinity for a zero time span?
However, when you multiply infinity times zero it is possible to get a finite number, and the Dirac delta is defined to have just such a well-defined integral:

\begin{equation}\label{eq:dirac-integral} \int_{-\infty}^{\infty} \delta (t-t') \, dt = 1 \end{equation}

These two equations, \eqref{eq:dirac-instant} and \eqref{eq:dirac-integral}, make up the definition of the Diracfunction.
A consequence of this is a very nice property:

\[ \int_{-\infty}^{\infty} f(t)\, \delta (t-t') \, dt = f(t') \]

\noindent
We will make use of this property in the upcoming treatment.

\subsection{Tracers in an ideal CSTR\label{sxn:cstr-tracers}}

Since the PFR case is trivial---it's just a time translation---we'll only formally examine the case of the behavior of a tracer in a CSTR.
Let's say we inject an instantaneous pulse of $n$ moles of tracer at time zero; to find the tracer behavior (and thus the residence time distribution) we need to perform a transient analysis.

The inlet concentration of the tracer is expressed as

\[ \cao(t) = n \, \delta(t - 0) = n \delta(t) \]

\noindent
where the time of the injection is $t'=0$, so we can just write $\delta(t)$.
We can perform a transient analysis by performing a mole balance on A:

\genericbalance

\[ \frac{d (\ca V)}{dt} =  n \, \delta(t) - v \, \ca \]

\noindent
If we design our tracer experiment correctly, the reactor volume and volumetric flow rates are constant, and we can substitute in $\taut = V / v$, leading to

\[ \taut \frac{d \ca}{dt} = \frac{n}{v} \delta(t) - \ca \]

We can solve this differential equation with an integrating factor:

\[ \frac{d \ca}{dt} + \frac{1}{\taut} \ca = \frac{n}{v \, \taut} \delta(t) \]

\[ u \equiv \exp \left\{\int \frac{1}{\taut} dt \right\} = \exp \left\{ \frac{t}{\taut} \right\} \]

\[ \frac{d}{dt} \left[u \, \ca \right] = \frac{n}{v \, \taut} \delta(t) u \]

\[ \frac{d}{dt} \left[ \exp\left(\frac{t}{\taut}\right) \cdot \ca \right] = \frac{n}{v \, \taut} \delta(t) \exp(t/\taut) \]

\[ d \left[ \exp(t/\taut) \ca \right] = \frac{n}{v \, \taut} \delta(t) \exp(t/\taut) dt \]

\[ \int_{t=0, \ca=0}^{t=t, \ca=\ca} d \left[ \exp(t/\taut) \ca \right] = \int_{t=0}^{t=t} \frac{n}{v \, \taut} \delta(t) \exp(t/\taut) dt \]

\[ \left[ \exp(t / \taut) \ca \right]_{t=0, \ca=0}^{t=t, \ca=\ca} =
\frac{n}{v \taut} \int_{t=0}^{t=t}  \delta(t) \exp(t/\taut) dt \]

We can use the property of the Dirac delta described earlier to integrate:

\[ \ca \exp(t / \taut)  =
\frac{n}{v \, \taut}  \exp(0/\taut) \]

\[ \boxed{ \ca (t) =
\frac{n}{v \, \taut}  \exp(-t/\taut) } \]

\noindent
(which is valid for $t \ge 0$.)
Now we have the concentration at the output as a function of time after a pulse was input.
It is just a simple exponential decay like we imagined!

The tracer is properly normalized, and called $E(t)$:

\[ E(t) \equiv \frac{C(t)}{\int_0^{\infty} C(t) \, dt} =
\frac{\exp (-t / \taut)}{\int_0^\infty \exp (-t/\taut) dt} \]

\[ \int_0^\infty \exp(-t/\taut) dt =
- \taut \left[ \exp(-t/\tau) \right]_0^\infty = \taut \]

\begin{equation}\label{eq:cstr-rtd} \boxed{ E(t) = \frac{1}{\taut} \exp (-t/\taut) } \end{equation}

This is the expression for the residence time distribution in a perfect CSTR.
Imagine what this means: experimentally, you could inject a tracer, and this is the response you should see.
You can use this to quantify deviations from ideality in real reactors.

\paragraph{Statistics.}
We can also ask the questions that we went over of our regular histogram.
For example, what is the mean residence time?

\[
\langle t \rangle \equiv
\frac{\int_0^{\infty} t \, E(t) dt}{\int_0^\infty E(t) dt}
=
\int_0^{\infty} t \, E(t) dt
=
\int_0^{\infty} t \exp(-t / \taut) dt
\]

\noindent
We will leave it to you to work out what this is, as well as the other moments of the distribution.

\endgroup
