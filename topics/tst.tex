\begingroup

\newcommand{\kb}{\ensuremath{k_\mathrm{B}}}

\chapter{Transition-state theory \label{sxn:tst}}

Reactions happen at different rates for a physical reason.
The physics of this are often described in terms of ``transiton-state theory'', which is an atomic-level picture that is relatively easy to grasp.
To understand it, we need a solid understanding of the potential energy surface, which we introduced in the very first lecture.

\section{The potential energy surface (PES)}

Imagine if you could grab two atoms from a stable molecule and pull them apart: a force would resist you as you pulled the bond.
Similarly to if you pulled on a spring, this means that you are increasing the potential energy of the atomic system with the work you did on it.

In class, we examined this for a \ce{H2} molecule vibrating, and saw that it vibrates in some (locally) parabolic well, as in Figure~\ref{fig:H2-H2O-PES}.
We also looked at \ce{H2O} vibrating; clearly, there are now two bonds, so we plot the potential energy against two independent axes, corresponding to each \ce{O-H} bond in the system, shown in the same figure.
However, this is clearly not complete: we should at least specify the bond angle (measured across \ce{H-O-H}, as this will also effect the potential energy).
Thus, we should ideally be plotting this in at least one more dimension (which is hard to do).

\begin{figure}
\centering
\includegraphics[width=0.8 \textwidth]{f/H2-H2O-PES/drawing.pdf}
\caption{Potential energy ``surfaces for \ce{H2} and \ce{H2O} vibrating. Note that the picture of \ce{H2O} is incomplete, as we certainly need a bond angle as well!
\label{fig:H2-H2O-PES}
}
\end{figure}

When we looked at a simulation of an \ce{H2} molecule vibrating, we saw that as its potential energy went up and down, its kinetic energy went down and up, in lock-step.
That is, energy is conserved at the atomic scale, just as it is everywhere else.

In the case of an (isolated) \ce{H2} molecule, the potential energy of the system is a function only of the bond length, and we were able to make a simple plot.
However if we want to be general, given the positions of both H atoms in space, there is a unique potential energy assigned to the system.
This requires six coordinates: the $(x, y, z)$ coordinates of each H, and we can say that we can describe the potential energy in ``six-dimensional space''.\footnote{Of course, it's really just two atoms, each in regular-old three-dimensional space.}

We can extend this concept to a general system of a molecule created of $N$ atoms.
We should supply $(x, y, z)$ coordinates for each atom, and in total supply $3N$ coordinates.
We therefore say that the potential energy is described in $3N$-dimensional space.\footnote{Likewise, this is also just regular-old three-dimensional space with $N$ positions specified.}

Mathematically, this means that potential energy is a function of $3N$ coordinates; however, we call this a potential energy \emph{surface} for reasons we will soon describe.

\subsection{Features of the PES}

\paragraph{Local minimum: a molecule.}
As we described in the first lecture, a molecule or phase corresponds to a local minimum in potential energy, with respect to the position of every atom in the system.
Or more properly, it responds to some reasonable region around a local minimum---perhaps we can think of it oscillating about that local minimum, and all points that it can oscillate through and still make it back towards that minimum.
This is easy to picture for something like an \ce{H2} molecule, where we just envision a parabolic well (or better, a Morse potential as we examined in our homework).

However, that's only a single dimension.
If we extend this to two dimensions, we have a surface, like in Figure~\ref{fig:PES-example}.
Here, a stable molecule is a valley, and we can perhaps envision a ball rolling around in a valley.
In class, we looked at an example of a ball in a bowl.


\begin{figure}
\centering
\includegraphics[width=3.5in]{f/PES-example/PES.png}
\caption{Example of an arbitrary 2-dimensional potential energy surface, showing key regions.  From http://www.chem.wayne.edu/~hbs/chm6440/PES.html\label{fig:PES-example}}
\end{figure}

How would we describe this mathematically?
From calculus, we have a clear way of defining local minima:

\[ \frac{\partial E_\mathrm{pot}}{\partial r_i} = 0 \text{ for all $i$ DOFs} \]

\[ \frac{\partial^2 E_\mathrm{pot}}{\partial r_i^2} > 0 \text{ for all $i$ DOFs} \]

\paragraph{Saddle point: a reaction.}
Two get between two local minima, we need to cross over some energy barrier.
A great analogy is hiking.
In Figure~\ref{fig:mt-washington}, we see the view going up one side of a mountain range.
Let's say you live in a town on one side and want to blaze a trail to a town in the next valley.
Perhaps it would make sense to go over the lowest point on the ridge?

\begin{figure}
\centering
\includegraphics[width=0.9 \textwidth]{f/mt-washington/pic.jpg}
\caption{If you need to get to the other side, perhaps take the easy route?
\label{fig:mt-washington}
}
\end{figure}

The easiest route over the mountain is known as the \emph{minimum energy path}.
The apex of this is called the \emph{saddle point}.
This is named for obvious reasons, as it looks like a horse saddle.
Reactions are the same way, as shown in Figure~\ref{fig:PES-example}, with saddle points connecting the easiest routes between two molecules.



How do we define the saddle point mathetmatically?
Locally it is still flat.
If we follow the minimum energy path; the saddle point is the top of it.
What about in all other directions?
It slopes uphill. Therefore, we have the criteria:

\[ \frac{\partial E_\mathrm{pot}}{\partial r_i} = 0 \text{ for all $i$ DOFs} \]

\[ \frac{\partial^2 E_\mathrm{pot}}{\partial r_j^2} < 0 \text{ for the minimum energy path dimension, $j$} \]

\[ \frac{\partial^2 E_\mathrm{pot}}{\partial r_i^2} > 0 \text{ for all $i \ne j$ DOFs} \]


\paragraph{A $3N$-dimensional surface?}
I can picture a potential energy in two dimensions: the surface of the earth \emph{is} potential energy in two dimensions.
If I have some functional form, I can figure out how to use a computer to make a surface in two dimensions like Figure~\ref{fig:PES-example}.
However, a relatively simple molecule like benzene has 12 atoms, and thus 36 ``dimensions''.
I have no idea how to mentally picture this for 36 dimensions.

Fortunately, the two-dimensional picture gives us the key information we need to mentally envision this process: the local minimum and the saddle point.
Thus, although we say that the potential energy surface is a $3N$-dimensional surface, we can just use the two-dimensional surface as our mental abstraction.

\section{Degrees of freedom}

If we want to be able to make plots like the above, how many dimensions do we need?
In general, each atom is free to move in the $x$, $y$, and $z$ directions; there are 3 ``degrees of freedom'' (DOF) for each atom.
So we have $3N$ degrees of freedom.
However, we saw we got away with only 1 dimension for \ce{H2} and 3 for \ce{H2O}.
Why?

There are certain symmetries, and they are easy to understand.
We can translate the whole molecule in any direction ($x$, $y$, or $z$) and it is unchanged.\footnote{Neglecting gravity and other fields that may be present.}
Similarly, we can rotate in 3 dimensions and its unchanged.
So, typically, we don't care about 6 of the dimensions and we have

\[ \text{DOF} = 3N - 6 \]

\noindent
This works for \ce{H2O}: $3 \times 3 - 6 = 3$, and we earlier deduced we should specify three things (two bond lengths and one angle).
But if we try it for \ce{H2}, we get $6-6 = 0$ degrees of freedom, but we konw we need to specify one, the bond length.

It turns out if we have a linear object, there are only two unique rotations\footnote{You can work this out by trying to rotate a book about three axes, then trying it for a pencil.}, so we should only subtract 5, not 6.
In the extreme limit if we only have a single atom, rotating it at all doesn't change it\footnote{If this confuses you, forget electronic orbitals and all that: just picture trying to rotate the nucleus of the atom. The nucleus is essentially a point charge, and the atom (including its electrons) doesn't care if this nucleus is rotated. When we talk of where atoms are in space, we really are saying where the nuclei go, and the electrons fill in the space around them where they are most happy. When we specify atomic positions, we are really only stating nuclear positions, as the electrons exist in a diffuse, probabilistic space around the nuclei. (This is known as the Born--Oppenheimer approximation.)}
Therefore, for small molecules we have a slight modification to the above rule:

\[ \text{DOF} = 3N - 5 \hspace{2em}  \text{(linear systems)} \]
\[ \text{DOF} = 3N - 3 \hspace{2em}  \text{(atomic systems)} \]

But these are subtletites.
For reasonably sized molecules, like benzene or glucose we have ($36-6=30$) or ($72-6=66$) degrees of freedom and the ``$-6$'' or ``$-5$'' or ``$-3$'' doesn't matter much: in other words, that's a whole lot of dimensions.
More or less,

\[ E_\mathrm{pot} = f(\mathcal{R}^{3N}) \]

\noindent
and we can't picture that, so just keep the two-dimensional picture in your mind, but be cognizant that it has many more dimensions.

\section{Understanding equilibria}\label{sxn:equilibria-wells}

The potential energy surface provides deep\footnote{Pun intended.} intuition on chemical equilibrium; that is, why mother nature favors one molecule more than another.
Let's draw some local minima on the potential energy surface (Figure~\ref{fig:pes-wells}): remember, these are ``wells'' correspond to molecules.
My most physical (but perhaps slightly strange) intuition is to think of a ball in sitting on such a surface, and then placing the whole surface on a vibratory shaker; that is, it bounces the ball around randomly, but with an average energy.
The higher the shaker speed, the higher the temperature.\footnote{Roughly, this is what is predicted by the Boltzmann distribution.}

Some comparison cases are shown in Figure~\ref{fig:pes-wells}.
Here, I've drawn barriers between wells to make sure we are aware that the molecules can interconvert (that is, react); however, we're not going to focus on the barriers right now, just the wells.
Additionally, I've tried to draw (in red) an orthogonal direction to each well, reminding us that the well does not exist in a single dimension, but multiple dimensions.
Let's try to understand which molecule will be favored in each case:

\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/pes-wells/drawing.pdf}
\caption{Some competing molecules. The red curves are intended to remind us the potential energy surface has more dimensions than we can draw in a page.
\label{fig:pes-wells}
}
\end{figure}

\begin{enumerate}

\item \textbf{Two equal wells.}
Here, the wells are roughly identical, so there's no reason that one well will be favored over the other.
If we think of a ball randomly bouncing around, it will spend roughly equal amounts of time in each well.
Thermal energy pops it over the top between the two wells, and leads to an equilibrium.
Because the states are equivalent, it will be 50:50.

\item \textbf{One deeper well.}
Thinking of a ball, once it falls into the right well, it will spend more time there as it's harder to get enough energy to bounce out.
So we should expect equilibrium to favor the right side, and that's exactly what we see; we know from equilibrium constants that the lower energy state is preferred.
That is, the right well is \emph{energetically preferred}.

\item \textbf{Same depth, but one is much wider.}
The wider well is preferred, because the system is more likely to find it.
There are more possibilities to be in it than the other; if the ball is bouncing randomly inside the right well, it's less likely to bounce out than to bounce around inside the well.
This is entropy: entropy just means there are more possibilities in one state than the other.
That is, the right well is \emph{entropically preferred}.

\item \textbf{One deep and narrow, one shallow and wide.}
Which is preferred?
It depends upon the temperature!
At low temperatures, the deep one is preferred, as we never have enough energy to shoot out of it.
At higher temperatures, the wider one is preferred as it has more possible locations.
Thermodynamically, we say this as:
\[ G = H - TS \]
As $T$ increases, the $TS$ term starts to dominate.
\end{enumerate}

\section{Transition-state theory}


When I think of transition-state theory, I like to think of Altamont Pass.
Just east of San Francisco, Interstate 580 connects Livermore with the Central Valley and passes over the Diablo mountain range through Altamont Pass.
California has some reasonably strict land preservation laws, so there are not many road through this range, and the bulk of the traffic takes I-580.
See Figure~\ref{fig:altamont}.

\begin{figure}
\centering
	\includegraphics[width=0.9 \textwidth]{f/altamont/map.png}
\caption{It's a reasonable guess that the bulk of the traffic is on I-580.
\label{fig:altamont}
}
\end{figure}

If we want to count the number of people traveling from Livermore to the Central Valley, it would be a pretty good approximation to just count those driving on the 580.
We'd miss a few people on back roads, and perhaps a few hikers.
But our estimate would be pretty good regardless.

Transition-state theory is the same way: we find the easiest way to get between reactant and product, and assume the vast majority of ``traffic'' between them follows this route.\footnote{Since low energy states are exponentially favored (by the Boltzmann distribution), the probability of taking higher-energy routes is typically low, except at very high temperatures. We can assess how significant this might be by comparing the how much the potential energy differs to $k_\mathrm{B} T$; that is, the potential energy to the kinetic energy.}

\paragraph{Deriving transition-state theory.}
The undergrad derivation of transition-state theory is admittedly a bit hand-wavy, because the more-rigorous derivation is steeped in the language of partition functions.\footnote{If partition functions are intuitive, you should definitely read the grad-level derivation. My notes from ENGN 2770 cover this, and are currently available at \url{https://bitbucket.org/andrewpeterson/atomistic-notes}.}
However, both supply the same intuition of why reactions proceed at the rates they do.

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/TST-PES/drawing.pdf}
\hspace{2em}
\includegraphics[width=3.0in]{f/pes-wells/transitionstate.pdf}
	\caption{(Left) The transition-state region can be thought of as a narrow region of space dividing the reactant region from the product region. We initially give it some width $\delta x$ such that we can conceive of a finite probability in this region, but it is differentially thick such that it can be thought of as a dividing surface. (Right) Side view. \label{fig:TST-PES}}
\end{figure}

Let's draw a contour map of a potential energy surface, Figure~\ref{fig:TST-PES}.
If you've ever used a contour map while hiking, this is the same thing; the curves mark points of constant elevation (or energy, in our case).
The reactant (R) is on the left, while the product (P) is on the right.
The shaded region, marked TS, is the transition-state region, which we'll say has some narrow width, kind of like the Diablo range in Altamont Pass.
Recall that the TS region starts includes the saddle point, and this is the simplest way to get from R to P.

Let's say our reaction is:

\[ \ce{A + B -> TS -> C + D} \]

\noindent
That is, let's assume every time we have a reaction, it goes through the region we're labeling as TS.
We're going to give it a bit of width.
Let's switch our analogy from a mountain pass to a bridge over a body of water; for example, we'd like to know the rate of cars passing from Bristol to Portsmouth.

I'll posit that we can write the rate as

\[ \frac{\text{(events)}}{\text{(time)}} \equiv \mathrm{(rate)} = \left(\parbox{12em}{\centering number of events in the transition-state region}\right) \times \left(\parbox{17em}{\centering rate of crossing the transition-state region, in the forward direction}\right) \]

\noindent
By our analogy, this is the (number of cars on the bridge) multiplied by the (speed of traffic).
The end result should be the number of cars crossing the bridge in the forward direction.
We're not examining the number coming back across the bridge in the opposite direction, for now; but we could do an identical analysis when we want to find that.

Further, the rate we're defining is as simple as possible: events per time.
We can multiply by a mole to get mol/s when we're done.

\paragraph{First term.}
Let's try to write the first term of our desired equation: the number of events in the transition-state region.
Although the transition state is ephemeral, we can still assess the likelihood of being in this region with the same equilibria tools we discussed in \secref{sxn:equilibria} and \secref{sxn:equilibria-wells}.\footnote{To a certain extent, we can still think of the transition state as a well in the potential energy surface. Although the transition state sits on a local maximum in one dimension, we're at a local minimum in $3N$ dimensions.}
We use the symbol $^\ddag$ (double dagger) to indicate the transition state and we can try to find the transition state's concentration ($C_\mathrm{TS}$):

\[ \frac{C_\mathrm{TS}}{C_\mathrm{A} C_\mathrm{B}} \approx \frac{a_\mathrm{TS}}{a_\mathrm{A} a_\mathrm{B}} = K^\ddag = \exp \left \{ \frac{- \Delta G^\ddag}{k_\mathrm{B} T} \right\}
\]

\noindent
Note that we are equating the number of events in the transition-state region to a concentration, which is just a number density.
However, we should be thermodynamically ``perfect'' and instead use activities, thus we instead refer to $a_\mathrm{TS}$.
(Recall for an ideal gas, the concentration is the activity, times a unit conversion; the case is similar for a dilute solution.)
So you can just think of the activity as being equivalent to concentration---but expressed thermodynamically rigorously; that is, we'll work in terms of activity, not concentration, for the rest of the transition-state discussion.
We add the double dagger to the $K$ and $G$ just to note that they are those of the TS; also, note that $\Delta G^\ddag$ is the free energy at the transition state minus that at the initial state when each has an activity of one (\secref{sxn:equilibrium-constant}).
Therefore, we can express the first term as

\[ a_\mathrm{TS}= \exp \left \{ \frac{- \Delta G^\ddag}{kT} \right\} a_\mathrm{A} a_\mathrm{B} \]

\paragraph{Second term.}
(Here is where the hand-waving comes in to play.)
The second term is harder in practice, but conceptually it's not too difficult.
It is rooted in the Boltzmann distribution, which states that probability of observing something is an exponential function of its energy.
Here, the energy at play is the kinetic energy (the velocity of crossing), and there's a way to calculate the probability of observing that kinetic energy, given that our potential energy is already at the top.
Refer to the grad notes if you want more detail\footnote{I'm mangling things slightly, for simplicity. Properly, we also shrink out the narrow width of the crossing region; that is, we let it go to zero. Again, see the grad notes for more rigor.}, which we won't go into here; but from this analysis we find the rate of crossing to be:

\[ \frac{k_\mathrm{B} T}{h} \]

\paragraph{Final form.}
Bringing together the two forms, we get:

\[ (\text{rate}) = \underbrace{\frac{\kb T}{h} \exp \left \{ \frac{- \Delta G^\ddag}{\kb T} \right\}}_{k(T)} \underbrace{a_\mathrm{A} a_\mathrm{B}}_{\sim C_\mathrm{A} C_\mathrm{B}} \]

\noindent
where the analogy to the common rate expression is marked.
It is important to note that $\Delta G^\ddag$ is calculated with the all activities equal to one; this is because we derived it from the equilibrium constant (\secref{sxn:equilibrium-constant}). The true concentrations at the conditions of interest enter through the $a_\mathrm{A}$ and $a_\mathrm{B}$ terms.
The result is typically expressed as the transition-state rate constant

\[ k^\ddag = \frac{\kb T}{h} \exp \left \{ \frac{- \Delta G^\ddag}{\kb T} \right\} \]


\noindent
It's often useful to split out the entropic and enthalpic contributions to the free energy; that is, $G = H = TS$:

\begin{equation}\label{eqn:tst-h-s}
k^\ddag =
 \frac{\kb T}{h} \exp \left \{ \frac{ \Delta S^\ddag}{\kb } \right\}
                 \exp \left \{ \frac{- \Delta H^\ddag}{\kb T} \right\}
\end{equation}

If we compare this to the Arrhenius form ($k = A e^{-E_\mathrm{A}/\kb T}$, letting $E_\mathrm{A}$ and $\Delta H^\ddag$ be equivalent), we can see that Svante almost got it right, but that $A$ does technically have a direct temperature dependence.
Interestingly, a direct dependence is quite small compared to an exponential dependence, so Arrhenius fits data just fine with all the T dependence put in the exponential.



\section{Coffee cans}

Hopefully, Section~\secref{sxn:equilibria-wells} helped make the reasons for chemical equilibria intuitive.
I'd like to do the same for reaction rates.

We can consider a ball bouncing in a bowl, as shown in class, as something analogous to reacting chemicals.
The wall of the bowl gives the potential energy, while the velocity of the ball leads to the kinetic energy (or temperature\footnote{Technically, temperature is how excited the system is, which includes how much kinetic energy it has \emph{and} how high up it is in potential energy surface. But, the equipartition of energy theorem says these will be equal, on average. So we can still think of temperature as our indicator of how much kinetic energy we have.}).
The ball will occasionally escape from the bowl if it has enough temperature and bounces out: this is a thermally activated event.
(This is why it is called an activated process---it needs energy.)
If we provide enough energy, it will occasionally pop out of the bowl. 

To make our analogy a little better, let's instead perform a thought experiment with a grain of sand inside of a coffee can, where the sand replaces the ball and the can replaces the bowl.
We'll again put our can on a vibratory shaker; the rate of shaking is analogous to temperature.
However, we'll cut a slit down our coffee can to give us a region where the sand is most likely to exit, analogous to the transition state.
See Figure~\ref{coffee-can}.


\begin{figure}
\centering
\includegraphics[width=4.0in]{f/coffee-can/drawing2.pdf}
\caption{Coffee can + grain of sand thought experiment. This can give us intuitive ideas of temperature, enthalpy, and entropy in transition-state theory.\label{coffee-can}}
\end{figure}

Let's consider some cases:

\begin{itemize}
	\item \textbf{Coffee can with a slit.}
First consider Figure~\ref{coffee-can} on left.
If we put the coffee can on a vibratory table the grain of sand will occasionally bounce out through the slot.
The frequency at which this occurs can be considered the reaction rate.
Obviously, if we shake it harder it will occur at a higher frequency; we can consider the shaking as  our \underline{temperature} (or kinetic energy).
Note the kinetic energy is in random directions, so this is a stochastic process, but we'll have a certain average frequency of ``reaction'' given enough observations.

\item \textbf{Coffee can with a deeper slit.}
		  If we consider cutting the slot deeper into the coffee can, like in the center figure, we'll expect it to pop out more often, as it needs less energy to get over the barrier. This is equivalent to changing the \underline{enthalpy} barrier, $\Delta H^\ddag$, in equation~(\ref{eqn:tst-h-s}).

\item \textbf{Coffee can with a wider slit.}
We could also consider widening the slot, at a constant height, as shown in the right of the figure.
We'd also expect the rate to increase due to more trajectories by which the grain of sand can escape.
However, the barrier height is the same: so if we think of the conventional ``activation energy'' explanation we miss this.
Does TST account for this?
Yes, in the $S$ part of the equation.
This is equivalent to changing \underline{entropy}, $\Delta S^\ddag$, in equation~(\ref{eqn:tst-h-s}).
Recall that entropy relates to the number of possibilities.

Examining at $\Delta S^\ddagger$, we recall it's the \emph{difference} in entropy between the transition state and initial state:

\[ \Delta S^\ddagger = S_\mathrm{TS} - S_\mathrm{R} \]

With the wider opening, $S_\mathrm{TS}$ is large; therefore $\Delta S^\ddag$ is large and the reaction is faster.

\item \textbf{Oil tank with the same slit.}
(Not pictured.)
What about the other extreme? Consider putting this same size (\textit{e.g.}, six inch) slot not on a coffee can but on an oil tank, which might be 20 m in diameter.
If we bounce the grain of sand around, do we expect it to make it out as often as the 6-inch slot on the coffee can? No.
Relative to the coffee can, the $S_\mathrm{TS}$ is the same, but $S_\mathrm{R}$ is different.
This highlights why it is the \emph{difference} in entropy that matters, and TST captures this.

This translates to real reactions.
We can think of transition state regions that are very narrow, which will have slower reaction rates compared to transition state regions that are wide.
Thus it is important to take into account the shape of the PES near the transition state region, not just the barrier height.
Transition-state theory captures this qualitative understanding.

\end{itemize}


\endgroup
