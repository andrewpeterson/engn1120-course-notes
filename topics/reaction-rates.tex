\begingroup
\chapter{Reaction rates}

\newcommand{\pd}[3]{\left(\frac{\partial #1}{\partial #2}\right)_{#3}}
\newcommand{\cc}[1]{C_\mathrm{#1}}
\newcommand{\act}[1]{a_\mathrm{#1}}
\newcommand{\n}[1]{n_\mathrm{#1}}
\newcommand{\ea}{E_\mathrm{A}}

Here, we'll describe how to mathematically formalize reaction rates.
From a physics standpoint this section is simple: no alchemy allowed.
That is, we conserve the number of atoms of each element; this is done by writing mole balances on each species (molecule type) and only allowing molecules to be created or destroyed through reactions.
Then, we introduce Boudart's rules for reaction rates, which is an empirical list of observations about how reactions typically proceed.
Justification and refinement of these rules will come in further sections.

\section{Reaction rates}

Take a generic reaction:

\[ \ce{A ->[1] B} \]

\noindent which we describe by its extent of reaction, $\xi$.
(Recall: $\xi$ just exists so that we don't have to worry about if we're specifying moles of A lost or moles of B produced---it is both.)
Logically, its ``rate'' is

\[ \mathrm{(rate)} = \frac{d \xi_1}{d t} \]

\noindent
Typically, we give the reaction rate the letter $r$ when it is normalized by the volume:

\begin{equation} \label{eqn:rate-def}
r_1 = \frac{1}{V} \frac{d \xi_1}{d t}
\end{equation}

\noindent
But be careful here---it is up to the person writing the rate equation to decide which form it is in.
In catalysis or electrochemistry, it often makes sense to have the rate be defined per catalyst area since this limits the amount of area available for reaction.
Similarly, in enzyme kinetics it may be per mole of enzyme.
Pay attention to the units.

Of course, the rate expression in equation~\eqref{eqn:rate-def} is just a definition and doesn't tell us anything---it is really just an expression of what we \emph{want} to know.
But if we can come up with expressions for $r$, then we can describe the progress of the reaction.
If we have this expression, we can use relations between $\xi_1$ and $n_A$, $n_B$ to track the progress.
For a single reaction,

\[ n_i = n_i^0 + \nu_i \xi_1 \]

\[ d n_i = \nu_i d \xi_1 \]

\noindent
(Today we'll employ:)

\begin{align}
i:& \text{ species} \\
j:& \text{ reaction}
\end{align}

\noindent
Plugging in, we get

\[ r_1 = \frac{1}{\nu_i V} \frac{d n_i}{d t} \]

\noindent
or

\[ \frac{d n_i}{d t} = \nu_i r_1 V \]

\noindent
In our generic reaction \ce{A -> B}, this would be:

\begin{align*}
\frac{d n_A}{d t} = - r_1 V \\
\frac{d n_B}{d t} = + r_1 V
\end{align*}

So far, these are pretty intuitive relations.
The reactant becomes in less abundance over time, and the product becomes in higher abundance.
For a constant-volume system, we can move volume into the derivative:

\begin{align*}
\frac{d C_A}{d t} = - r_1 \\
\frac{d C_B}{d t} = + r_1
\end{align*}

\noindent where $C_i = n_i / V$.
Note that this can be written in a convenient vector form (that will become our workhorse form):

\[
\frac{d}{d t}\left[
\begin{array}{c}
		  C_A \\
		  C_B \\
\end{array} \right] = 
\left[ \begin{array}{c}
	-r_1 \\
	+r_1 \\
\end{array} \right]
\]

\subsection{Multiple reactions}

What if we have multiple reactions?
For example consider if there are two reactions that compete:

\[ \ce{A  ->[1] B} \]
\[ \ce{A ->[2] C} \]

\noindent
There are two reactions, so this implies two extents of reactions, $\xi_1$ and $\xi_2$.
By intuition, we can write mole balances on A, B, and C as:

\[ \n{A} = \n{A}^0 - \xi_1 - \xi_2 \]
\[ \n{B} = \n{A}^0 + \xi_1 \]
\[ \n{C} = \n{C}^0 + \xi_2 \]

\noindent
where $n_i^0$ is the initial amount (in moles) of species $i$.
We can generalize this for any species $i$:

\[ n_i = n_i^0 + \sum_{j=1}^{N_\mathrm{rxns}} \nu_{i,j} \xi_j \]

\noindent
To express as rates, we take a time derivative.
Note that the initial concentration and the stoichiometric coefficient are not functions of time:

\[ \frac{d n_i}{d t} = \sum_{j=1}^{N_\mathrm{rxns}} \nu_{i,j} \frac{d \xi_j}{d t} \]

\noindent
Using our earlier definition of a reaction rate,

\[ r_j = \frac{1}{V} \frac{d \xi_j}{d t} \]

\[ \frac{d n_i}{d t} = V \sum_{j=1}^{N_\mathrm{rxns}} \nu_{i,j} r_j \]

\noindent
where $r_j$ are volumetric reaction rates.
For the case of the system of reactions we had above, this would lead to

\[ \frac{d}{d t} 
\left[ \begin{array}{c}
n_A \\
n_B \\
n_C \\
\end{array} \right] =
V \left[ \begin{array}{c}
-r_1 - r_2 \\
+ r_1 \\
+ r_2 \\
\end{array} \right]
\]

This is a nicely formatted system of first-order differential equations, set up for solving computationally.
(In general, it can be tough to solve these analytically; this will be addressed in a later section, \secref{sxn:numeric-integration}.)
Note that if we have constant volume, the $n$'s can be replaced by $C$'s.

But this leaves the question...what do we use for $r$?



\section{Boudart's rules}

\paragraph{Boudart's rules, aka observations, of $r$ for macroscopic reactions.}
Now that we know what we want ($r_i$, the reaction rate), the question is, what is it?
We all probably already have some intuition for $r$ from chemistry courses we have taken, but fundamentally, these intuitions are based on observation.
Chemists measure the rates of reaction under various conditions, and develop a ``rate form'' or ``rate equation'' for the system.
(Later, we will talk about how we can derive $r$ from first principles and see that it is properly tied to activity; for now, we will look at what has been observed.
Although deriving form first principles becomes more powerful every year---and my research program is built upon this---it's still not nearly as powerful as just observing.)

Boudart's rules---that is, observations\footnote{I believe every one of these ``rules'' has at least one exception to prove it. Except \#5.}---are:


\begin{enumerate}
\item The rate decreases monotonically with increasing reaction extent.
\item A one-way reaction can generally be written as:

\[ r = k(T) f(C_i, T) \text{ for all species } i \]


Note that $k(T)$ is called the rate ``constant''---even though it is not constant---because it does not depend on concentration.
As the most prominent exception to being constant, it varies exponentially with temperature!
(See rule \#3.)
However, the second function is frequently not a function of temperature, in which case the temperature dependence is separable:

\[ r = k(T) f(C_i) \]

\item The rate constant generally takes an Arrhenius form:\footnote{It appears that ideal gases must be involved due to the inclusion of $R$, the ideal gas constant.
This interpretation is wrong.
I'll note that this is the ``macroscopic'' version of this equation; in the microscopic version we have $k_\mathrm{B} T$ in the denominator, where $k_\mathrm{B}$ is the Boltzmann constant.
$R = k_\mathrm{B} N_\mathrm{A}$.}

	\[ k(T) = A \exp \left\{- \frac{ \ea }{ R T} \right\} \]

where $\ea$ is the ``activation energy''.
(These sorts of processes are called ``activated processes''.)


\item
The function of concentrations can often be approximated as
\[ f(C_i) = \prod_i C_i^{\alpha_i} \]
This is called ``power-law'' kinetics.

\item A net reaction can be taken to be the sum of its forward and reverse reactions

		  $$ r = r_{+} - r_{-}$$
\end{enumerate}

\subsection{Guldberg--Waage form (Law of mass action)}

The form of reaction rates we're probably familiar with is called the ``law of mass action'', and was formulated in Norway in 1864 by Guldberg and Waage.~\cite{Guldberg1864}
It simply states that the rate of a (forward) chemical reaction is proportional to the concentration of the reactants:


\[ r = k \prod_i^{N_\mathrm{reactants}} C_i^{-\nu_i} \]

\noindent
($\prod$, which is the capital of the Greek letter $\pi$, is the mathematical symbol for ``product'', analogous to the $\sum$ symbol for summations.)
For example,

\[ \begin{array}{c@{\hspace{2em}}c}
	\ce{A ->[1] B} & r_1 = k_1 \cc{A} \\
	\ce{2 A ->[2] B} & r_2 = k_2 \cc{A}^2 \\
	\ce{A + B ->[3] C} & r_3 = k_3 \cc{A} \cc{B}  \\
\end{array} \]


This form is often true, but is \textit{not} true in general.
(It will turn out to be true for elementary reactions, though.)
For example, the reaction

\[ \ce{H2 + Br2 -> 2 HBr} \]

\noindent has a rate expression

\begin{equation}\label{eqn:HBr-rate}
r = \frac{k_1 C_{\ce{H2}}C_{\ce{Br2}}^{1/2}}{k_2 + \frac{C_{\ce{HBr}}}{C_{\ce{Br2}}}}
\end{equation}

\subsection{Reaction order}

When a reaction can be expressed as in Rule \#4, we define a reaction order. For instance

\[ r = k \cc{A} \]
\[ r = k \cc{A}^2 \]
\[ r = k \cc{A} \cc{B} \]

\noindent
The first is a ``first-order reaction in A'' and has a ``total order'' of 1.
The second is second-order both in A and in total.
The last is first order in A, first order in B, and has a total order of 2, and thus could also be called a second order reaction.
(These descriptions are mainly colloquial---for convenience when kineticists talk to each other.)


\subsection{Concentrations versus activities}

The reactions above are written in terms of concentrations, which is common for many reactions, particularly when the rate constants are measured from experimental data, where concentrations are directly controlled.
However, as we will see in \secref{sxn:tst} when we derive rates from theory, that rates are often more rigorously expressed in terms of activities; \textit{e.g.},

\[ r = k \, \act{A} \]
\[ r = k \, \act{A}^2 \]
\[ r = k \, \act{A} \, \act{B} \]

\noindent
Note that in this case $k$ is interpreted as the reaction rate when all activities have an activity of one, and that $k$ will have different units depending on which approach is used.
For now, we'll primarly use concentrations until we arrive at the theory chapter.

\section{Example}\label{sxn:simple-reversible}
Here, we'll work through a simple example reaction, so that we can see it how this all works in practice.
In this case, we'll study the isomerization of glucose to make fructose, which is a reversible reaction.
(This is the reaction in the manufacture of high-fructose corn syrup.)
We'll assume both sugars are reasonably dilute in water, and thus the total system volume is constant.
We'll also assume the temperature and pressure are constant.
We first need to define the geometry of the reactor we'll employ.

\subsection{The batch reactor}

\begin{figure}
\centering
\includegraphics[width=0.2 \textwidth]{f/batch-reactor/drawing.pdf}
\caption{A batch reactor.
\label{batch-reactor}
}
\end{figure}

Our first simple type of reactor: the ``batch reactor''.
Imagine that we have a tank, which we can sketch like in Figure~\ref{batch-reactor}.
At time zero, we add in our initial reactants (which in this case will be glucose and fructose), which are mixed perfectly from the moment they enter the reactor.
Throughout the course of the reaction, we assume it stays perfectly mixed.
The key assumptions for an idealized batch reactor are:

\begin{itemize}
\item No reaction before time zero.
\item Perfectly mixed at all times after time zero.
\end{itemize}

\subsection{Reactions and rates}
Let's write our isomerization reaction as:

\[ \ce{A <-> B} \]

\noindent
which we'll write as two separate reactions to make life simpler:

\[ \ce{A ->[1] B} \]
\[ \ce{B ->[2] A} \]

\noindent
Let's assume the reaction rate forms are known and are of the simplest, first-order form:

\[ \begin{array}{l@{\hspace{1em}}l}
r_1 = k_1 C_\mathrm{A},& k_1 = 1\, \mathrm{s}^{-1} \\
r_2 = k_2 C_\mathrm{B},& k_2 = 3\, \mathrm{s}^{-1} \\
\end{array} \]

\noindent
and that the initial concentrations are given as 5 M for A and 1 M for B.

\subsection{Procedure}

I typically use a mole balance to ``derive'' the governing equations on the fly:

\[
\mathrm{(acc)} = 
\mathrm{(in)} - 
\mathrm{(out)} + 
\mathrm{(gen)} - 
\mathrm{(cons)} \]

\noindent
For species A and B:

\[ \frac{\partial}{\partial t} \left( \cc{A} V \right) = 0 - 0 + r_2 V - r_1 V \]

\[ \frac{\partial}{\partial t} \left( \cc{B} V \right) = 0 - 0 + r_1 V - r_2 V \]

\noindent
Plugging in ($r_1$, $r_2$), pulling out the constant $V$, and converting the partial derivatives to ordinary\footnote{At this point in the course, you may be wondering why we are writing partial derivatives at all. In general, concentration will be a function of both time and position. For now, we're assuming it's perfectly mixed, so it's only a function of time. Thus, it's an ordinary differential equation.} gives the system:

\begin{equation}\label{eqn:batch-ODEs}
	\frac{d}{d t} \left[ \begin{array}{c}
	\cc{A} \\
	\cc{B} \\
\end{array} \right] = \left[ \begin{array}{c}
  -r_1 + r_2 \\
  +r_1 - r_2 \\
\end{array} \right] = \left[ \begin{array}{c}
	-k_1 \cc{A} + k_2 \cc{B} \\
	+k_1 \cc{A} - k_2 \cc{B} \\
\end{array} \right]
\end{equation}

First, note that this is already slightly complicated, although we only have one reversible reaction.
As you can imagine, as we start adding in more reactions or more complicated reactor geometries these will start to get very messy.  
However, this system can be solved quite easily by hand, so we will do that.

We have a system of two coupled ODEs, but we can easily remove one variable by realizing that at any time $\cc{A} + \cc{B}$ is a constant\footnote{You could also work in terms of extent of reaction ($\xi$), which would resolve this issue. However, to keep things intuitive we'll keep this in terms of $\cc{A}$ and $\cc{B}$.}; therefore we can express $\cc{B}$ as

\[ \cc{B} = \cc{B,0} + (\cc{A,0} - \cc{A}) \]

\noindent
and we can then re-write the top ODE as:

\[ \frac{d \cc{A}}{dt} = - k_1 \cc{A} + k_2 \left( \cc{B,0} + \cc{A,0} - \cc{A}\right) \]

\[ \frac{d \cc{A}}{dt} = - (k_1 + k_2) \cc{A} + k_2 \left( \cc{A,0} + \cc{B,0} \right) \]


\noindent
I personally prefer to solve this by substitution; that is, by defining $u$:

\[ \begin{array}{lll}
u &\equiv& - (k_1 + k_2) \cc{A} + k_2 \left( \cc{A,0} + \cc{B,0} \right) \\
du &=& - (k_1 + k_2) d \cc{A} \\
\end{array} \]

\[ \therefore \frac{du}{dt} = - (k_1 + k_2) u \]

\[\int_{u(t=0)}^{u(t=t)} \frac{du}{u} = - (k_1 + k_2) \int_0^t dt \]

\[ \ln \frac{ -(k_1 + k_2) \cc{A} + k_2 (\cc{A,0} + \cc{B,0})}
            { -(k_1 + k_2) \cc{A,0} + k_2 (\cc{A,0} + \cc{B,0})}
	    = - (k_1 + k_2) t
\]


\[ \cc{A}(t) = \underbrace{\left[ \cc{A,0} - \frac{k_2}{k_1 + k_2} (\cc{A,0} + \cc{B,0})\right]}_{\text{constant}}
e^{-(k_1 + k_2) t}
+ \underbrace{\frac{k_2}{k_1 + k_2} (\cc{A,0} + \cc{B,0})}_{\text{constant}}
	\]

This is messy, but the form is simple when we note the terms that are constants: that is, large lumped parameters.
Functionally, all we have in the equation is the exponential decay with time constant of $(k_1 + k_2)^{-1}$.


\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/batch-reactor/plot-form.pdf}
\caption{Rough form of the solution for the batch reactor isomerization example.
\label{batch-plot-form}}
\end{figure}

We can reason out what this should look like and sketch it, the result will be in Figure~\ref{batch-plot-form}.
Steps to create this:

\begin{itemize}
\item Plugging in $t=0$ to the equation, we get $\cc{A} = \cc{A}^0$. This is intuitive.
\item Plug in $t=\infty$. We get

\[ \cc{A}(t=\infty) = \frac{k_2}{k_1 + k_2} (\cc{A,0} + \cc{B,0}) = \frac{3}{1+3}(5+1) = 4.5 \text{M} \]
\item In between, we get exponential decay from the initial concentration to the asymptote, as drawn.
\item By difference, B must just do the opposite: starting at 1 and rising to 4.5.
\end{itemize}

We could likely simplify the derivation somewhat by working in terms of $\xi$ instead of $\cc{A}$.
However, a major point of this example is that it is already \emph{messy}---and this is the simplest realistic system (one reaction, reversible, a trivial reactor geometry) that we can conjure.
As we add more reactions, variable temperature, interesting reactor geometries, coupling to transport, \textit{etc.}, you can imagine that this will get more complicated.
At some point, solving these equations analytically (that is, by hand) becomes cumbersome if not impossible, and at this stage stops providing insight.
Therefore, we often switch to numeric solutions to these problems, in which we directly integrate ordinary differential equations via sophisticated computational algorithms.
We'll provide intuition on these algorithms in Section~\secref{sxn:numeric-integration}.



\section{The kinetic link to thermodynamics}

When examining the results of the example above, notice that it levels off: this is the equilibrium.
But this is strange: thermodynamics dictates the equilibrium, and we have only considered the kinetics.
So there must be some link between thermodynamics and kinetics.
We only supplied rate constants ($k_1$ and $k_2$); we didn't (knowingly) supply any thermodynamics, like an equilibrium constant $K$ or a $\Delta G_\mathrm{rxn}$.
Therefore, there must be some thermodynamic information embedded in the kinetic information we provided.

Let's figure out what $K$ would be in this case based on the behavior.
For now, we'll assume ideal solution behavior\footnote{If you like to be rigorous: $K \equiv \act{B}/\act{A}$. For both components, we'll define the $a_i=1$ state to be at a concentration of $C_i =$ 1M. Thus, $a_i = \gamma_i C_i \rightarrow \gamma_i = C_i / a_i$, and $\gamma_i$ is the same for A and B at 1 M. By assuming ideality, we also assume $\gamma$ is not a function of concentration, which is reasonable for dilute solutions. Thus, $K = (\gamma \cc{B}) / (\gamma \cc{A}) = \cc{B}/\cc{A}$.} which means we can write

\[ K = \frac{C_B^\mathrm{(eq)}}{C_A^\mathrm{(eq)}} \]

\noindent
We get $K = 1/3$ in this example.
Interestingly, you can try plugging in different initial concentrations for A and B, and you will always find that the two tend towards this same ratio.
So there must be some link between the thermodynamics, which we know is included in $K$, and the kinetics, which we express as little $k_i$.
Let's find it.

At equilibrium, what must be true?
The concentrations should go to some steady values; mathematically this means:

\[ \left. \frac{dC_A}{dt} \right|_\mathrm{(eq)} = 0, \left. \frac{dC_A}{dt} \right|_\mathrm{(eq)} = 0 \]

\noindent
Using either of these with equation~\eqref{eqn:batch-ODEs} we find that

\[ r_1 = r_2 \]

\noindent at equilibrium. Or

\[ k_1 C_A = k_2 C_B \]

\[ \frac{C_B}{C_A} = \frac{k_1}{k_2} \]

\[ K = \frac{k_1}{k_2} \]

That is, the thermodynamic equilibrium constant $K$ is immediately related to the reaction rate constants $k_i$.
Another way of thinking about this is we only have the freedom to specify either one of these two rate constants---the forward or reverse---and thermodynamics dictates the other rate.
Experimentally, this also means that if you could measure the forward rate constant and knew the thermodynamics of your system, you would automatically know the backwards rate constant.

Also, note that the above relation was for the particular reaction we analyzed.
If you were looking at a reaction that had a complicated rate form like equation~\eqref{eqn:HBr-rate}, you would not be able to find such a simple constraint.
In general, the above analysis works for elementary reactions and others ones with power-law kinetics, but at this stage you should re-derive it for any system you are interested in.

\endgroup
