\begingroup
\newcommand{\genericbalance}{ \[ \left( \begin{array}{c} \mathrm{Acc.}\\ \end{array}\right) = \left( \begin{array}{c} \mathrm{In}\\ \end{array}\right) - \left( \begin{array}{c} \mathrm{Out}\\ \end{array}\right) + \left( \begin{array}{c} \mathrm{Gen.}\\ \end{array}\right) - \left( \begin{array}{c} \mathrm{Con.}\\ \end{array}\right) \] }
\newcommand{\vr}{\ensuremath{v^\mathrm{r}}}
\newcommand{\vo}{\ensuremath{v^\mathrm{0}}}
\newcommand{\vi}{\ensuremath{v^\mathrm{i}}}
\newcommand{\ve}{\ensuremath{v^\mathrm{e}}}
\newcommand{\cao}{\ensuremath{C_\mathrm{A}^\mathrm{0}}}
\newcommand{\cae}{\ensuremath{C_\mathrm{A}^\mathrm{e}}}
\newcommand{\cai}{\ensuremath{C_\mathrm{A}^\mathrm{i}}}
\newcommand{\taut}{\ensuremath{\tau}}
\newcommand{\tc}{\ensuremath{\tilde{C}_i}}
\newcommand{\axc}{\ensuremath{A_\mathrm{xc}}}
\newcommand{\Da}{\ensuremath{D_\mathrm{a}}}
\newcommand{\cifeed}{\ensuremath{C_i^0}}
\newcommand{\peclet}{\ensuremath{\mathrm{Pe}_\mathrm{a}}}

\chapter{Beyond the ideal reactors}

So far we have looked at two extremes---or ideal limits---of flowing reactors: the perfectly mixed reactor and the perfectly unmixed reactor.
Of course, most real reactors operate somewhere in-between these limits.
In this chapter, we'll first look at some interesting reactor models that can ``tune'' the reactor behavior between these limits, while still using these ideal reactors as building blocks.
Then we'll examine a more sophisticated modeling of coupled reaction and transport that take place within real reactors.

\section{Food and animals}

Before discussing this reactor configuration, let's talk about food.

We humans need high-quality diets; that is, we are limited in what we can eat.
Of course, we can choose to be omnivores or vegetarians, but we are still very limited.
Let's stick to the world of the vegetable kingdom: we generally only eat things that are \emph{intended} to be food.
Let's look at some examples:

\vspace{1em}

\begin{tabular}{ll}
\hline \hline
Food & Biological role \\
\hline
wheat & seed (food for baby plants) \\
nut & seed (also food for baby plants) \\
potato & tuber (food to make sprouts next spring) \\
corn & seed \\
canola oil & seed oil \\
berries & seed/bait (food for baby plants and/or for animals to disperse) \\
\hline \hline
\end{tabular}

\vspace{1em}

\noindent
It's only the stuff that the plant is intending\footnote{You can probably think of some exceptions to this, like broccoli. But these exceptions were typically bred by humans to be food. And from a percentage of our caloric intake, the vast majority of (plant-based) foods are from seeds, fruits, and tubers.} to produce as food---either for itself or as some sort of bait for animals---that we take from it and eat.

However, the most prominent biological molecule (at least on land) is cellulose.\cite{Klemm2005}
Cellulose is used to give plants structure---for example, grass, trees, and leaves contain huge amounts of cellulose.
Although cellulose, like starch, is a polymer of glucose, plants build cellulose in a way that it is challenging to break down into food, and our bodies cannot accomplish this.
However, some animals are capable of eating this ``low-quality'' diet of  cellulose, and these herbivores tend to be the megafauna.
Most herbivores that eat low-quality food are massive animals, because they need to carry around huge reactors (with long residence times) in order to digest this low-quality food.
For example, a cow is a massive digestive system on stilts that walks around eating grass and chewing cud.
Cellulose takes a long time to digest, as bacteria in the cow's gut work on breaking down the cellulose into sugars.
Other examples of large herbivores include bison, elephants, giraffes, hippos.
The economies of scale can make this worthwhile!

Rabbits are an exception to this.
The rabbit eats a very similar diet to a buffalo, but is a much smaller animal.
The rabbit itself is also food to many predators, so it has to be quick on its feet and can't afford to carry around large amounts of food with it.
It needs to have a very small, fast GI tract.

What is the rabbit's solution?
It digests a little, then excretes.
Later, it comes back and eats its own poop---this is nature's recycle reactor.
Thus, it can get a long residence time for its food but keep a small, lightweight digestive tract.
A second benefit in the rabbit's case is that the poop is full of the microbes that act to break down the cellulose while the food is outside of the body, so some of the digestion has continued while the rabbit was hopping around avoiding wolves.

(As a side note, termites do roughly the same thing, but defecate in a termite mound.)

\section{The recycle reactor}

\begin{figure}
\centering
\includegraphics[width=3.0in]{f/recycle/drawing.pdf}
\caption{A recycle loop around a reactor. The labels match the variable names in the text:  0 (feed), i (reactor inlet), e (system exit), and r (recycle stream).
\label{fig:recycle}}
\end{figure}

\paragraph{Recycle reactors.}
``Recycle'' is a technique to take part of the effluent of a reactor, and feed it back into the entrance; see Figure~\ref{fig:recycle}.
In principle, any reactor can be inside of the recycle loop, but for now let's just consider our two ideal reactor types: the CSTR and the PFR.

The CSTR is perfectly mixed---so if we take some of the output and feed it back into the inlet feed, it should have no effect.
Therefore, we won't consider a CSTR in a recycle loop, but will confine our analysis to the PFR.


Let's analyze a recycle system; for now, we'll just call it a generic reactor in the middle of the loop and will analyze the surroundings.
We will also assume that the density of the fluid is constant throughout the system.
First, we define a recyle ratio $R$ as the ratio of the recycle stream to the original feed stream:

\[ R \equiv \frac{\vr}{\vo} \]

We'll do a mole balance on a species A around the first mixing point:

\genericbalance

\[ 0 = v^0 \cao + v^r \cae - v^i \cai \]

\noindent
In this case, note that we did not assume it was in steady state in order to set the accumulation term to zero!
This is the case because there's no volume at the mixing point.
We can also do a balance on the fluid at the same junction:

\[ 0 = \vo + \vr - \vi \]

\noindent
or we can express this in terms of our recycle ratio $R$

\[ \vi = \vo + \vr = (1 + R) \vo \]

\noindent
Plugging this into the mole balance equation, and using the recycle ratio definition again,

\[ 0 = \vo \cao + R \vo \cae - (1 + R) \vo \cai \]

\[ (1 + R) \cai = \cao + R \cae \]

\[ \cai = \frac{\cao + R \cae}{1 + R} \]

The analysis above assumes nothing about the behavior \emph{inside} the reactor, but we nonetheless can see how this system behaves by examining the limiting cases.

\begin{itemize}
\item
If $R=0$, then $\cai = \cao$; that is, the reactor feed is equal to the system feed.
That is, we just have a normal reactor with no recycle.
%(In this case, a PFR.)

\item
If $R \to \infty$, we find $\cai = \cae$; that is, the concentration in the reactor is effectively \emph{uniform}.

\end{itemize}

\noindent
Let's imagine the reactor is a PFR.
Of course, when $R=0$, we just have normal PFR behavior.
However, when  $R \rightarrow \infty$, then the overall system behavior is that of a CSTR.
So by varying the recycle ratio, we can vary the behavior between that of a PFR and that of a CSTR.
That is, $R$ is effectively a knob that allows us to tune the reactor behavior between the two ideal limits.

Also note that if the reactor volume and the inlet volumetric flow rate are held constant, then as $R$ grows $\tau$ goes to zero.
That is, the volume stays constant but the volumetric flow rate through the reactor increases.
Sometimes it can be more useful to think of it in terms of holding \vi, rather than \vo, constant---that is, a constant flow rate through the reactor.
Then as the recycle ratio $R$ increases, the overall exit flow (\ve) decreases, ultimately to a trickle.

Recycle reactors are useful for autocatalytic reactions (\secref{sxn:autocatalytic}).
Interestingly, both rabbits and termites (apparently) rely on microbes to digest their cellulose, so this does re-introduce the microbes in.

\paragraph{Coupling with separations.}
In practice, recycle streams are most commonly used when coupled with separation steps, as shown in Figure~\ref{fig:with-separator}.
That is, even though the single-pass conversion within a reactor may be low, if an efficient separator is located downstream of it, much of the unreacted chemical can be fed back into the reactor.
Thus, the overall conversion of the system can be made quite high.
This can also be used in reactions where the thermodynamics are unfavorable: \textit{e.g.}, if the equilibrium conversion is low, by combining this step with a separation an overall conversion well above equilibrium can be achieved.


\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{f/recycle/with-separator.pdf}
\caption{Recycle coupled with separation increases the overall conversion. \label{fig:with-separator}}
\end{figure}

\section{Reactors in series}

We just saw that if we made a modification to a PFR, that we effectively added a parameter (the recycle ratio) that could ``tune'' the behavior between the PFR and CSTR limits.
Is there an analogous way we could start from the CSTR and make the behavior more PFR-like?

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{f/cstr/n-cstrs.pdf}
\caption{$N$ CSTRs of identical volume in series.\label{fig:ncstr-sketch}}
\end{figure}

As the title implies, the answer is to divide into smaller reactors and arrange them in series.
Clearly, this should have no effect on the kinetics of the PFR, so we can constrain our analysis to CSTRs.
Let's consider $N$ CSTRs in series, as shown in Figure~\ref{fig:ncstr-sketch}, each with identical residence time $\taut$.
We'll analyze this in terms of the residence time distribution.
From equation~\eqref{eq:cstr-rtd}, a tracer concentration at the end of a single CSTR will be:

\[ E_1(t) = \frac{1}{\taut} e^{-t/\taut} \]

If we feed this to a second CSTR, you can work out\footnote{Try integration by parts, like in~\secref{sxn:cstr-tracers}.}

\[ E_2(t) = \frac{t}{\taut^2} e^{-t/\taut} \]


\begin{figure}
\centering
\includegraphics[width=3.0in]{f/ncstrs/out.pdf}
\caption{Result of RTD for N CSTRs in series, all with equal residence time.
\label{fig:ncstr-result}
}
\end{figure}

\noindent
where $\tau$ is the total residence time of the two reactors.
Before adding more reactors to the series, let's compare the 1- and 2-CSTR models in Figure~\ref{fig:ncstr-result}.
The plot shows the residence time distribution, which we can recall would be the expected result of putting a detector at the output of a tank reactor and recording the concentration of a pulse tracer injected at the inlet at time zero.
With the 1-CSTR model, we see the concentration immediately shoot up in a step function at time zero; that is, the tracer takes precisely zero seconds to become perfectly mixed in and make it to the exit.
Even if we subtract off the small amounts of piping at the inlet and exit, this would be difficult behavior to achieve in practice.
The 2-CSTR model might give something a bit more like what we'd expect in practice: the tracer concentration rises very sharply, before peaking and starting to decay.

For three CSTRs, we get

\[ E_3(t) = \frac{t^2}{2 \taut^3} e^{-t/\taut} \]

\noindent
where again $\taut$ is the total residence time of the three-reactor system.
The result of having the squared on the time means it will dominate a little more at the beginning, keeping it closer to zero; we see this on the plot.
We can see a pattern emerging in both the equations and the plot; if we keep working through the math we can convince ourselves that for $N$ CSTRs we have:

\begin{equation}\label{eq:n-cstrs} E_N(t) = \frac{t^{N-1}}{(N-1)! \, \taut^N} e^{-t/\taut} \end{equation}

As we examine the plot, we see the distribution start to get sharper and center on the value of $\taut$.
If we take this to very large values, we get a pulse!
Thus, $N$ CSTRs in series will give us the residence-time distribution behavior of a PFR if $N$ is very large.

\subsection{Aside: The gamma distribution.}
The probability distribution that emerged for $N$-CSTRs is known as the gamma distribution\footnote{Technically it's an ``Erlang'' distribution, which is just a gamma distribution where the power ($N$) is restricted to integer values, but broadly the category is known as the gamma distribution.}, which is an important distribution in statistics and emerges naturally in many areas of physical and social science.
It is known as a ``maximum entropy'' distribution, and is thus considered the distribution one should choose when you don't have better information to choose another.
You'll note that both the Boltzmann distribution and the Maxwell--Boltzmann distributions can be written as gamma distributions (with different shape parameters).
The gamma distribution also appears in many places, from epidemiology to economics.

Formally, it can be written as

\[ f(x; \, \bar{x}, k) = \frac{x^{k-1} e^{-x k / \bar{x}}}{\Gamma(k) \left(\frac{\mu}{k}\right)^k} \]

\noindent
where $\bar{x}$ and $k$ are the parameters and $x$ is the ``random variable''.
Note the gamma distribution replaces the factorial in equation~\eqref{eq:n-cstrs} with the ``gamma function'', $(N-1)! \to \Gamma(N)$. The gamma function is just the generalization of the factorial to non-integers, and when $N$ is an integer, it simplifies to the factorial.

\section{Accounting for transport (PFR with dispersion)\label{sxn:dispersion}}

Next we will examine real behavior within a flow reactor, by incorporating dispersion.
Dispersion can be thought of as some combination of diffusion and backmixing.~\cite{Levenspiel1957}
In fluid mechanics, dispersion is sometimes taken as just the effect of mixing and can be accounted for separately from diffusion; but since both follow a Fick's-law--type form, it is safe to combine diffusion and dispersion into a single term for our purposes.
I realize that not everyone who takes this course has already taken fluid mechanics and/or mass transfer courses, but the concepts that we use here are fairly simple---or at least our use of them will be straightforward.

A perfect PFR is thought of as ``perfectly unmixed'' along the axial dimension (and perfectly mixed along the radial direction).
However, in a real PFR, we would expect there to be some mixing, at minimum just from diffusion.\footnote{A neat, classic fluid mechanics video on turbulence illustrates this nicely! See \url{https://youtu.be/1_oyqLOqwnI}.}
Thus, in a perfect PFR, we would expect that if we injected a pulse of tracer we would see behavior like shown in Figure~\ref{fig:dispersion-concept}.
That is, when a tracer is injected, the tracer's shape is unchanged in an ideal PFR, but always broadens when dispersion effects are taken into account.

\begin{figure}
\centering
\includegraphics[width=4.0in]{f/dispersion-concept/drawing.pdf}
\caption{Dispersion in a PFR. The plots show the behavior of a hypothetical tracer infected at the entrance. When dispersion is ignored, the tracer's shape is unchanged. When dispersion is included, the shape always broadens.
\label{fig:dispersion-concept}
}
\end{figure}

From fluids and tranport, we know that the flow of species $i$ in the $z$ direction, when there is coupled convection (\textit{i.e.}, flow) and dispersion (\textit{i.e.}, diffusion/mixing) is

\[ F_i = u \axc C_i - \axc \Da \frac{\partial C_i}{\partial z} \]

\noindent
Here, $F_i$ and $C_i$ are the molar flow rate and concentration of species $i$, \axc\ is the cross-sectional area of the PFR, $u$ is the velocity of the flow down the reactor, and \Da\ is the dispersion coefficient in the axial direction.
(The subscript $\mathrm{a}$ refers to axial; dispersion in the radial direction may have a different coefficient.)
The first term should be obvious; it just accounts for the movement of the species along with the flow.
The second term is essentially Fick's law (written for dispersion); note it has a minus sign since the flow always moves in opposition to the gradient.
(That is, the action of diffusion/dispersion is to flatten a gradient.)

How is \Da\ determined?

\begin{itemize}
\item If we have laminar flow, \Da\ is the molecular diffusion coefficient of species $i$ in the medium of interest.
\item If we have turbulent flow, \Da\ is a combination of diffusion and backmixing. You can typically estimate this from correlations that you will find in your fluids and transport textbooks, or this might be measured from the behavior of tracer experiments.
\end{itemize}

\noindent

Let's first examine what will happen if we inject a tracer; that is, we'll ignore reaction. 
We will need to perform a transient analysis in order to study the movement of a tracer---we skipped this analysis back in \secref{sxn:pfr-transient}.
Let's examine a PFR in the region between $z$ and $z + \Delta z$, as shown in Figure~\ref{fig:pfr-transient}; we'll use the balance equation to account for the number of moles of $i$ in this region.

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{f/pfr/pfr.pdf}
\caption{Transient analysis of a PFR. \label{fig:pfr-transient}}
\end{figure}

\genericbalance

\[ \frac{\partial}{\partial t} \left( \axc \, \Delta z \, C_i \right) =
\left. \left(u \axc  C_i - \axc \Da \frac{\partial C_i}{\partial z} \right) \right|_z
- \left. \left(u \axc C_i - \axc \Da \frac{\partial C_i}{\partial z} \right) \right|_{z + \Delta z}
\]

We do the standard trick of breaking out the definition of a derivative to get

\[ \frac{\partial}{\partial t} \left( \axc C_i \right) =
-\frac{\partial}{\partial z} \left(u \axc C_i - \axc \Da \frac{\partial C_i}{\partial z} \right)
\]

If $u$, \axc, and \Da\ are constants (with respect to both $z$ and $t$), then

\[ \frac{\partial C_i }{\partial t} =
- u \frac{\partial C_i }{\partial z}  + D_a \frac{\partial^2 C_i}{\partial z^2} 
\]

\paragraph{Non-dimensionalizing.}
In all areas of engineering, it's often good (and common) practice to convert the governing equations into a ``non-dimensional'' form.
This is a very good idea, especially when we have competing effects at work.
(In the right-hand side of the equation above, there is a competition between a flow term and a dispersion term; soon, we will also have a term introducing reaction.)
By putting the equations into non-dimensional form, we are able to more readily understand the effects of these two terms (through dimensionless numbers that will naturally arise), as well as to more simply understand the physics of our situation, and which parameters are truly independent of one another versus lumped.

When we non-dimensionalize our equations, we are specifically looking to non-dimensionalize the \emph{variables}, not the parameters.
We can recognize the variables in the above partial differential equation: they are the quantities that follow $\partial$; that is, $C_i$, $t$, and $z$.

The general goal when non-dimensionalizing is to make new variables that are normalized by some natural quantity in the system.
Ideally, you will create new non-dimensional quantities that are all of order 1; this will be vary useful in assessing the relative importance of different effects.

Let's explore the natural choices to non-dimensionalize.
Let's first look at the $z$ variable, which is the easiest one in this case.
The only length scale in our problem is the length of the reactor, $L$, so we can propose a non-dimensional variable:

\[ Z \equiv \frac{z}{L} \]

\noindent
We note that $Z$ will vary between 0 at the reactor inlet and $1$ at the reactor exit, so this is nicely scaled.

Next we look at $t$.
There is no natural time variable in the system, but we note that both $u$ and \Da\ have time within their dimensions, as they describe rates of processes.
It would be legitimate to choose either of these as the basis for non-dimensionalizing, but in our case it's probably a bit more natural to choose $u$, since this is present in a PFR even when dispersion is ignored.

Now, $u$ does not have units of time, so it cannot be used directly; it has dimensions of length per time.
But note that $L/u \equiv \taut$, the residence time of the reactor, does have dimensions of time (that come from $u$).
Thus, this might be a natural choice for non-dimensionalizing time and we choose:

\[ \theta \equiv \frac{t}{\taut} = \frac{t u}{L} \]

\noindent
This also seems to be appropriately scaled; \textit{i.e.}, after $\theta=1$, one ``residence time'' has passed, and, for example if we have  inject a tracer we can expect our necessary observation time to be roughly over when $\theta=1$; that is, it will give us a good idea of the range of time to watch our system. 

Non-dimensionalizing the concentration is usually pretty straightforward; in a situation like this we would typically use the feed concentration \cifeed\ to normalize $C_i$ (assuming $i$ is a reactant, if it were a product, you might normalize by one of the reactants).
In this case, we are describing a tracer experiment, but we didn't ever say just what that tracer is.
If it were a step change, then we might put in the concentration of the feed after the step change.
If it were something more like a pulse of $N_i$ moles, we might try to define a realistic time period it takes to input the pulse so we can get a rough concentration, or perhaps we might divide by the total reactor volume.
But in this particular situation, we are just going to write \cifeed\ and leave it ambiguous, and we can decide how to define it later when we think about precise tracer types.
As it turns out in the differential equation above, $C_i$ appears to the first power in each term, so \cifeed\ will not appear in our final equations and it's choice won't matter to our immediate analysis.
This will hopefully become apparent when we substitute in these non-dimensional terms.
Thus, we can define a non-dimensional concentration as:

\[ \tc \equiv \frac{C_i}{\cifeed} \]

Next, we substitute these non-dimensional variables into our partial differential equation:

\[ \frac{\partial ( \cifeed \tc ) }{\partial (L \theta / u)} =
- u \frac{\partial (\cifeed \tc) }{\partial (LZ)}  + \Da \frac{\partial^2 (\cifeed  \tc)}{\partial (LZ)^2} 
\]

We can pull all of the constants (\cifeed, $L$, and $u$) out of our derivatives, just leaving them in terms of our variables (\tc, $\theta$, $Z$).
When we do this we can gather the constants together as below:

\[ \frac{\partial \tc }{\partial \theta} =
- \frac{\partial \tc }{\partial Z}  + \frac{\Da}{Lu} \cdot \frac{\partial^2 \tc}{\partial Z^2} \]

\noindent
Note that because our variables are dimensionless, this means our derivatives (such as $\partial \tc / \partial \theta$) are dimensionless.
Further, this means that the group of constants we have remaining ($\Da / (Lu)$) is also dimensionless.
In fact, this is how most of the dimensionless numbers we have in engineering (such as the Reynolds number, etc.) are typically discovered.

This suggests a non-dimensional number dictates the behavior of our differential equations; this is the ``P{\'e}clet'' number which is typically defined as

\[ \peclet \equiv \frac{Lu}{\Da} \hspace{2em} = \frac{\text{convective effects}}{\text{dispersive effects}} \]

\noindent
where the subscript again refers to the axial direction.
With this convention, we can re-write our partial differential equation as

\[
\boxed{
\frac{\partial  \tc }{\partial \theta} =
- \frac{\partial \tc }{\partial Z}  + \frac{1}{\peclet} \frac{\partial^2 \tc}{\partial Z^2} 
}\]

The above is the equation that dictates the behavior of a transient PFR with no reaction.
It tells us that---if we properly non-dimensionalize our system---that the behavior of this PFR is dictated by a single parameter, the P{\'e}clet number.
As noted above, this can be thought of as the balance between convective and dispersive effects.
This is a great simplification from our original understanding, where we had lots of parameters to worry about.
We can think about limiting cases: as $\peclet \to \infty$, the term on the right disappears and we are left with the standard equations of a ideal (dispersion-free) PFR.
As $\peclet \to 0$, the first convective term disappears, and we are left with only the dispersive behavior.

\paragraph{Residence time distribution.}
Levenspiel and Smith~\cite{Levenspiel1957} solved for the concentration at the outlet, when the initial conditions are a pulse,\footnote{The boundary conditions, however, are a bit tricky on this one. If you think about high-dispersion scenarios, there would be a prediction that the tracer gets pushed back out the entrance (and out the exit). The most ideal situation is one in which we envision and infinite-length tube, and at some point that we designate as $Z=0$ we ``inject'' a pulse of tracer at time $\theta=0$, then monitor the concentration at $Z=1$ as time evolves. This is the approach Levenspiel solved, and he notes that numeric approaches are required if closed boundary conditions are desired.} and find the residence time distribution when a tracer is injected to be


\[ E(\theta) = \left( \frac{\peclet}{4 \pi \theta} \right)^{1/2} \exp \left\{ \frac{- (1-\theta)^2 \peclet}{4 \theta} \right\} \]


\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{f/pfr-dispersion/out.pdf}
\caption{Dispersion effects in a PFR. When \peclet\ is very large, we have a regular PFR, as \peclet\ gets very small, the CSTR behavior is approached. 
\label{fig:pfr-dispersion-levenspiel}}
\end{figure}

\noindent
We can see the results of this in Figure~\ref{fig:pfr-dispersion-levenspiel}, which shows the outlet concentration with time when a pulse tracer is injected at time $\theta{=}0$.
We can see that this is remarkably\footnote{Although I must admit that the solution above looks very similar to the gamma distribution, and the results are very similar to the gamma distribution. So it's somewhat disappointing to start from opposite ends and not see the identical behavior in-between. But Levenspiel could circle laps around us in terms of his analytical skills! After all, the open-source alternative to MATLAB (``Octave'') was named after him.} similar to the case of the CSTRs in series, but with \peclet\ as the shape parameter, rather than $k$.
When the Peclet number is very large (flow dominates over mixing) we recover the ideal PFR limit; when the Peclet number is very small (mixing dominates over flow) we recover the ideal CSTR limit.

Thus, a PFR with high dispersion approaches CSTR behavior.
Many CSTRs in series approach PFR behavior.
We start to see reality lies somewhere in between.

\section{Coupling reaction and transport}

We now have the tools to couple chemical reactions with transport effects.
We'll stick with the simple example of a PFR with dispersion, but the general derivation procedure is the same in other geometries.
Practically, let's assume that we have performed residence time distribution studies to measure the dispersion coefficient in our plug-flow reactor.
We can then predict the kinetics for a reactive system.

We can write a mole balance on species $i$ as

\genericbalance

\[ \frac{\partial}{\partial t} \left( \axc \Delta z \, C_i \right) =
\left. \left(u \axc C_i - \axc \Da \frac{\partial C_i}{\partial z} \right) \right|_z
- \left. \left(u \axc C_i - \axc \Da \frac{\partial C_i}{\partial z} \right) \right|_{z + \Delta z}
+ \axc \Delta z \sum_j \nu_{i,j} r_j
\]


\noindent
We will break out the definition of a derivative to get

\[ \frac{\partial}{\partial t} \left( \axc C_i \right) =
-\frac{\partial}{\partial z} \left(u \axc C_i - \axc \Da \frac{\partial C_i}{\partial z} \right)
+ \axc \sum_j \nu_{i,j} r_j
\]

If $u$, \axc, and \Da\ are constants, then

\[ \frac{\partial C_i }{\partial t} =
- u \frac{\partial C_i }{\partial z}  + \Da \frac{\partial^2 C_i}{\partial z^2} 
+ \sum_j \nu_{i,j} r_j
\]

Thus, we have a IBVP to solve, where the reaction term at the end can take fairly arbitrary forms.
You can take whole classes devoted to solving these analytically (and people used to devote whole PhD theses to such tasks!).
However, today we can solve these numerically in a pretty straightforward manner, but we'll need some new tools to deal with the partial derivatives and the higher-order derivatives, which we'll cover in coming sections.

Note that the above procedure can be replicated in arbitrary geometries, with multiple dimensions, to make more elaborate reactor models.
As the geometry becomes more elaborate and the reaction becomes coupled with fluid mechanics and transport, models will almost certainly be numerical in nature, and are typically solved in either the FDA (finite-difference approximation) or FEM (finite-element modeling) formalism.



\endgroup
