{

\chapter{Course introduction, types of reactions}



\section{Intro to the field}

The fundamental \emph{process} in chemistry is \emph{the reaction}.
This class is about the engineering of reactions, so in many ways we can consider this field to be the heart of the chemical engineering curriculum.

When we consider the physical world around us, we know that literally everything is made of chemicals.
With the exception of natural products in their natural form---such as a wooden desk or a stone walkway---everything in our manufactured world is in a different chemical form than its starting material.
Almost everything you encounter (random examples below) 

\begin{itemize}[noitemsep]
\item deodorant
\item AIDS drug
\item gasoline, petrochemicals
\item steel
\item paper
\item gorilla glass (screens for smartphones)
\item fermented foods (beers, breads, cheeses)
\item transistors
\end{itemize}

\noindent is the result of an engineered chemical reaction.
We can consider this course to be the principles behind engineering nearly everything around us, or at least a key step in it.

Although the field of reaction kinetics has its origins in petrochemicals (and examples in textbooks draw from these areas perhaps too strongly), the applications are very broad.
The concepts of chemical kinetics that we'll cover in this course apply equally to the natural world, such as in geochemical or biological transformations, and chemical engineers' skills can have wide applications in these areas.
We will try to highlight examples from diverse applications.

This class will be broken down into three general areas over the course of the semester:

\begin{enumerate}

\item \emph{The reaction.}
First, we will focus on reactions themselves.
We'll discuss elementary steps versus observable/stoichiometric reactions, quantum-mechanical driving forces (interpreted through potential energy surfaces and transition-state theory), rate theory, phenomenological effects (such as chain reactions or enzyme kinetics), thermal and pressure effects, and simplifications to kinetic understandings. 

\item \emph{The reactor.}
In the second part of the class, we'll examine reactors.
That is, what can we as engineers do to design systems that will influence the dynamics of reactions?
To do this, we'll focus on three extreme designs of reactors---that is, the limiting cases, and understand how to describe real reactors in terms of deviations from these ideal cases.
This portion of the class will focus on how to analyze and design these complicated systems, and as such, will have a strong numerical/analytical component, as you will be solving differential equations that are much too complicated to solve in closed form.

\item \emph{Special topics.}
In the final section of the course, we'll look at selected applications---how parts 1 and 2 apply to some real systems.
I am open to suggestions from you as to what types of systems you would like to see covered.
Last time, we focused on biological reactors and electrochemical reactions, to see how we can understand both of these from the context of the course.
If you have suggestions, please let me know.
The earlier the better!

\end{enumerate}

\section{The reaction}

Let's look at an example reaction.
A simple, well-known reaction is:

\[ \ce{H2 + 1/2 O2 -> H2O} \]

\noindent
This is a simple reaction; is it an \emph{elementary} reaction?
The definition of an elementary reaction from Davis \&\ Davis~\cite{Davis2003} is ``an irreducible molecular event''.
Clearly this is not, since there's no such thing as half of a molecule.
So at minimum we should double it:

\[ \ce{2 H2 + O2 -> 2 H2O} \]

\noindent
Now we have something like an empirical formula for the reaction.
Is this now a good description of a reaction?
How much does it tell us about what is actually happening?

Fishtik and Datta~\cite{Fishtik2007} have examined this reaction, and suggest the elementary steps in Table~\ref{table:water-rxns}.
There are something like 20 \emph{elementary} steps!
Of course, researchers disagree about exactly what steps to include in these complex networks, but this is provides one example of the complexity of even the simplest of reactions.
Note that only certain steps can initiate the reaction (from a mixture of only \ce{H2} and \ce{O2}), such as 3, 10, and 11 in reverse.

\begin{table}
\centering
	\caption{Possible elementary steps in \ce{H2 + 1/2 O2 -> H2O} as proposed by Fishtik and Datta~\cite{Fishtik2007}. \label{table:water-rxns}}
\begin{tabular}{cl}
\hline \hline
Number & Reaction \\
\hline
1 & \ce{H2 + OH <-> H2O + H} \\
2 & \ce{O + H2 <-> OH + H} \\
3 & \ce{H + HO2 <-> H2 + O2} \\
4 & \ce{H + O2 <-> OH + O} \\
5 & \ce{OH + OH <-> O + H2O} \\
6 & \ce{H + O2 + M <-> HO2 + M} \\
7 & \ce{H + HO2 <-> OH + OH} \\
8 & \ce{OH + HO2 <-> H2O + O2} \\
9 & \ce{HO2 + O <-> OH + O2} \\
10 & \ce{H + H + M <-> H2 + M} \\
11 & \ce{O + O + M <-> O2 + M} \\
12 & \ce{H + O + M <-> OH + M} \\
13 & \ce{OH + H + M <-> H2O + M} \\
14 & \ce{H2 + O2 <-> OH + OH} \\
15 & \ce{O + OH + M <-> HO2 + M} \\
16 & \ce{HO2 + HO2 <-> H2O + O2} \\
17 & \ce{H2O2 + M <-> OH + OH + M} \\
18 & \ce{H2O2 + H <-> HO2 + H2} \\
19 & \ce{H2O2 + OH <-> H2O + HO2} \\
20 & \ce{H2O2 + O <-> OH + HO2} \\
Overall & \ce{2 H2 + O2 <-> 2 H2O} \\
\hline
\end{tabular}
\end{table}

Let's see if we can map out a complete route from the reactants to the products.
(This will differ from what is done in class, as students choose a unique set of reactions.)
We'll assume we \emph{only} have \ce{H2} and \ce{O2} present initially, so the first elementary reaction we choose will have to have only those to species.
Let's start with reaction 14, then find reactions to consume each of the products.

\begin{align*}
\ce{H2 + O2 ->[14] OH + OH} \\
\ce{OH + OH ->[5] H2O + O} \\
\ce{O + H2 ->[2] OH + H} \\
\ce{OH + H + M ->[13] H2O + M}
\end{align*}

We have found one possible pathway, using four elementary steps.
But there are many other possibilities, obviously, and probably they are all all hapenning simultaneously!
Recall that there are typically moles ($> 10^{23}$) of each reactant present, so at any point in a reaction that has reasonable kinetics we shouldn't think of just a pair of OH's being produced by reaction 14, but a concentration of OH being produced.
Taht is, some of the OH could be consumed in reaction 5 or 1, and they are both happening simultaneously.
Thus, it's best to think of this as a reaction network, not a pathway.

Note there's an extra species, called M; this indicates a ``molecule''; which in this case is required to carry energy to or from a collision, in order that energy is conserved.
Why is this needed?
And why is it on both sides of the chemical equation?
Let's examine the simplest reaction in which it is listed:

\[ \ce{H2 + M ->[-10] H + H + M} \]

\noindent
On the left-hand side, we have the molecule \ce{H2}, in which there is a bond holding the two H's together.
On the right-hand side, this bond is broken.
The need for M is just conservation of energy: it takes energy to break that bond, and it needs to come from somewhere.
Here, it is assumed to come when \ce{H2} collides with another molecule---where the collision has enough energy to break the bond and the molecules are oriented in the right way for this to happen.
So we can think of the M on the reactant side of the above equation as being a high-energy species (that is, having a high velocity) and the M on the product side as being a low-energy species.
By the reversibility of Newton's laws, when two H's come together in the reverse reaction they must still collide with an M in order to \emph{carry away} the excess energy that is left over when the bond forms.

\subsection{Stoichiometric versus elementary reactions}

This raises some definitions that we should be clear about.
In this class, we'll refer to reactions as either ``stoichiometric'' or ``elementary''.

\begin{itemize}

\item \emph{Stoichiometric reaction.}
A stoichiometric reaction is a description of the overall change in stoichiometry that may occur (over reasonably long times) in a reacting system.
It is a slightly vague definition, but this is what we intuitively consider when we think of a chemical reaction.
Examples:

\[ \ce{CH4 + 2 O2 -> CO2 + 2 H2O} \]

\[ \ce{H2 + Br2 -> 2 HBr} \]

\[ \ce{glucose -> fructose} \]

The last is the infamous reaction used in producing high-fructose corn syrup, catalyzed by the enzyme known as glucose isomerase.

\item \emph{Elementary reaction} (or elementary step).
According to Davis \&\ Davis~\cite{Davis2003}, an elementary reaction is an ``irreducible molecular event'' (Davis and Davis).
What exactly does this mean?
We know there are sub-steps in any of the above stoichiometric reactions, as we already observed with \ce{H2} combustion in Table~\ref{table:water-rxns}.
But how do we know where to stop dividing?
We'll discuss this qualitatively in the next paragraphs, and more precisely when we discuss transition-state theory in a future lecture.

\end{itemize}

\paragraph{Elementary reactions and potential energy.}
We'll take a deeper look at an elementary reaction, starting with elementary reaction 2 from the \ce{H2} combustion example:

\[ \ce{O + H2 -> OH + H} \]

\noindent
How do we know that this is an elementary step?
Let's start by schematically sketching out the endstates.

\begin{verbatim}
O        H - H    (initial state)
O - H        H    (final state)
\end{verbatim}

\noindent
But why couldn't we have said that there was another initial or final state in between, like

\begin{verbatim}
O      H     H   (another state?)
\end{verbatim}

\noindent
so that we could write the reaction as 

\[ \ce{O + H2 -> OHH -> OH + H} \text{ ?} \]

\noindent
Even if we restrict the geometry to fixed positions for the end O and H, and a linear path between for the mobile H, this middle hydrogen atom occupies a continuum of states---why not count each of these as an elementary state?

We can understand this better from a thermodynamic perspective, by plotting the reaction in  energy coordinates in Figure~\ref{first-PES}.
Here, we'll describe how we can intuitively justify such a plot.

\begin{figure}
\centering
\includegraphics[width=0.5 \textwidth]{f/first-PES/drawing.pdf}
	\caption{A qualitative potential energy plot for a hypothetical elementary reaction with two atoms (O and H) fixed in space while a third atom (H) transits between them. We can deduce the shape by knowing the restorative force near the stable bonds acts similarly to Hooke's Law.
\label{first-PES}
}
\end{figure}

When the ``mobile'' H is near the fixed H we have a hydrogen molecule---in other words, an H--H bond.
We'll assume the O is far enough away that the mobile H doesn't ``feel'' the O's presence.
We know that if we stretch the bond, it is like stretching a spring: it pulls back to its equilibrium bond length; similarly if we compress the bond, it pushes back to the equilibrium length.
This is completely equivalent to a rise in potential energy as we might encounter in a dynamics course; thus, we label the $y$ axis as ``potential energy''.
If we employ quantum mechanics to calculate the potential energy as a function of this bond length, we get almost exactly a Hookean response over short displacements.
That is, it's locally quadratic.

Of course, if we move the H over by the O, we get a similar response.
We would expect a different spring constant to the Hookean response, but a similar shape.
That is, the H--H and the H--O configurations are local minima with respect to potential energy.
If you again use quantum mechanics to calculate the points in between these two local minima, you find a (smooth) maximum in between, as shown in the drawing.

We can call this a \emph{potential energy plot}, in which we can see something a little special is occuring about this elementary step.
Namely, the reaction is proceeding from one local minimum to another local minimum.
And more specifically, it does so by proceeding over only a single maximum.
(Logically, if there was a second local maximum a third local minimum would appear.)

In our example, we restricted the system by saying that the O and one H were fixed, and the other H was restricted to moving in a single dimension.
In truth, all three atoms are free to move in three dimensions: that is, at any point in time we could give\footnote{Heisenberg's uncertainty principle puts limits on how precisely we can say this, but you can ignore that for now.} the state of the systems as ($x_\mathrm{H1}$, $y_\mathrm{H1}$, $z_\mathrm{H1}$, $x_\mathrm{H2}$, $y_\mathrm{H2}$, $z_\mathrm{H2}$, $x_\mathrm{O}$, $y_\mathrm{O}$, $z_\mathrm{O}$); that is, by specifying the Cartesian coordinates of each atom.
At any configuration, we could use quantum mechanics to calculate the potential energy, which is a function of $3n$ coordinates.
This hypersurface in $3n$ dimensions is typically called a \emph{potential energy surface}, which we'll discuss more in a later lecture.

In general, this is a good working description of an elementary step: proceeding from one local minimum on the potential energy surface.
We could be a little better by using free energy---which would allow us to consider ``barrierless'' reactions and bumpy potential energy surfaces---but you typically don't encounter such situations until the grad level, so the mental picture here is good.

This now gives us more rigor about our definition of an elementary step.
\emph{Working definition}: an elementary step is one that connects two local minima on the potential energy surface while crossing a single local maximum.





}
