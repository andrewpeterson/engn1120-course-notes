\begingroup

\chapter{Nonlinear regression (fitting parameters of arbitrary functions)}


As we saw in Section~\secref{sxn:simple-reversible}, solving for analytical solutions can quickly become arduous, if not impossible, for realistic kinetic systems.
(However, as a rule of thumb, when you can solve it by hand you should if you are looking for insights from your model---and you can check your hand solution versus a numeric solution to make sure you don't have errors.)

Additionally, we often need to infer things like rate constants from experimental data.
There are many techniques to ``linearize'' functions such that one can use linear regression.
However, this is also cumbersome and can distort the fit.
(For a prominent example of where this distortion becomes extreme, see the Lineweaver--Burk analysis of enzyme kinetics, \secref{sxn:enzyme}.)
However, with modern software, performing nonlinear regression is not much more effort than performing linear regression (and is less effort when you remove the need to first linearize your data).

In practice, we typically employ these techniques as ``black boxes'', and separate instructions have been provided for how to run them in python.
In this section, we'll peer into the black box a bit to describe how these two common numeric techniques work.

\section{Fitting functions}

You are most likely used to performing linear regression---you've probably at least taken two columns of numbers in Excel and selected the option to create a best-fit line.
Here, we'll extend that to fitting any function.
(This will be useful for your homework assignment about fitting the Morse potential.
And you may not realize it, but it will make your life a lot easier on problem \#3 as well where you are tasked with finding a rate constant.)

\section{Linear regression}

Let's remind ourselves who linear regression works.
See the left side of Figure~\ref{fig:regression}.
We start off with some data, shown as the black points.
We propose a line (characterized by its slope and intercept, $m$ and $b$) shown as the black line.
Then we assess how well our proposed line fits by looking at the ``residuals''; that is, for each point, how far is the model's prediction from the actual data?
Whatever line can minimize the residuals is the ``best fit'' line; this is linear regression.
Mathematically, this is stated as

\begin{figure}
\centering
\includegraphics[width=0.99 \textwidth]{f/regression/linear-nonlinear.pdf}
\caption{Linear and nonlinear regression.
\label{fig:regression}
}
\end{figure}

\[ y^\mathrm{pred} = m x + b \]

\[ \min_{m,b} \underbrace{ \sum_i^{\mathrm{observations}} \left[ y_\mathrm{pred}(x_i; m, b) - y_i \right]^2}_{\text{``cost function''}} \]

\noindent
Historically, we sum the square of the error, as a way to make all the residuals positive.
(This also allowed for a closed-form algebraic solution to the best-fit line.)
When solving numerically, we can choose other cost functions: another common choice is the mean absolute error.
The values of $m$ and $b$ that minimize the cost function are the ``best fit'' line.

\section{Non-linear regression}

For non-linear regression, we can do the exact same thing.
It's annoyingly easy and makes you wonder why everyone always uses linear regression.
See the example on the right side of Figure~\ref{fig:regression}(right).
Assume that we have reason to believe that we can fit the data to

\[ y^\mathrm{pred} = A \left( 1 - e^{-B t} \right) \]

\noindent
Interestingly, we do the \emph{exact same thing} as we did above in linear regression:

\[ \min_{A,B} \sum_i^{\mathrm{observations}} \left[ y_\mathrm{pred}(x_i; A, B) - y_i \right]^2 \]

\noindent
that is, we write a loss function in terms of the residuals, and we minimize the loss function.

\section{How do I minimize a function?}
Of course, to do this we need a way to minimize a function.
We can envision doing this numerically by guessing A and B, then perturbing both A and B to see in which direction the function goes down; that is, following the gradient down.
By repeating this, we can walk downhill to the lowest point, a local minimum.
(Note that finding the \emph{global} minimum is much more challenging.)
In practice, more sophisticated methods exist that build better estimates of the gradient and second derivative (called the Hessian) to more quickly reach the bottom.
The ``BFGS'' method is probably the most common, but we won't go further into the internals of optimizers.

\section{Example in python}

Here, we'll show how to fit the parameters of a general, non-linear model to provided data.
The technique is simple: we guess parameters for our model and use it to predict our data.
We compare the predictions to our measured data through a ``cost function''---typically a sum of square residuals---and we use an optimizer to make this cost function as small as possible by varying our parameters.

Let's assume that we have the below data, and expect it to fit a parabola.
Our goal is to find the parameters that best describe the parameter, given the data available.

\begin{center}
\input{f/regression/data}
\end{center}

\noindent
Our model is that a parabola; that is, to find $a$, $b$, and $c$ in

\begin{equation}\label{eq:regression-model}
y^\mathrm{pred}(x) = a x^2 + b x + c
\end{equation}

\noindent
To do so we'll minimize a loss function that gives the `sum of square residuals' (SSR) of the model's prediction as compared to the actual data:

\begin{equation}\label{eq:regression-loss}
f = \sum_{i=1}^{N} (y^\mathrm{pred}(x_i) - y^\mathrm{meas}_i)^2
\end{equation}

\noindent
The heart of any nonlinear regression approach is to just code up a ``loss function''; that is, a function that gets closer to zero the better the parameters fit.
This is done in python below, where the loss function is called \verb'get_loss'.

\noindent
File attached here.  \attachfile{f/regression/run.py}
\pythoncode{f/regression/run.py}

\noindent
Notes:

\begin{itemize}
\item \emph{Lines 6--12:} The loss function (SSR) of equation~\eqref{eq:regression-loss}.
Note that it needs to take exactly one input (here called \verb'p'), but this input can be an array.
Here, our array contains the values of a, b, and c, which we can see are fed to the model in line 9.
Note the \verb'+=' statement of line 15 is a shortcut, equivalent to writing \verb'loss = loss + ...'.
The \verb'print' statement can be useful to monitor how the procedure is behaving.

\item \emph{Lines 15--16:} The ``model'' of equation~\eqref{eq:regression-model}.

\item \emph{Line 21:} The regression itself occurs with a single command, \verb'fmin'.
This takes as arguments the function to be minimized and an initial guess of the best parameters.
\end{itemize}

The result of this script is Figure~\ref{fig:regression}.

\begin{figure}
\centering
\includegraphics[width=0.6 \textwidth]{f/regression/out.pdf}
\caption{Regression exxample.
\label{fig:regression}
}
\end{figure}

\endgroup
