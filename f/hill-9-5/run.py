import numpy as np
from scipy.integrate import odeint
from matplotlib import pyplot


def dy_dt(y, t):
    """Returns the derivative dy_dt."""
    cA, cB, cC, cAB = y  # Unpack the vector.
    r1 = k1 * cA**2
    r2 = k2 * cA * cB
    r3 = k3 * cAB**2
    dcA_dt = - 2. * r1 - r2
    dcB_dt = + r1 - r2 + 3. * r3
    dcC_dt = + r1 + r3
    dcAB_dt = + r2 - 2. * r3
    dydt = [dcA_dt, dcB_dt, dcC_dt, dcAB_dt]
    return dydt


fig, axes = pyplot.subplots(ncols=2, figsize=(6.5, 3.), sharex=True, sharey=True)
fig.subplots_adjust(left=0.08, right=0.99, bottom=0.14)
times = np.linspace(start=0., stop=0.3, num=1000)
k1 = 1.02
k2 = 150.
k3 = 172.

CA0 = 1.0
CB0 = 0.0
answer = odeint(func=dy_dt, y0=[CA0, CB0, 0., 0.], t=times)
cAs = answer[:, 0]
cBs = answer[:, 1]
cCs = answer[:, 2]
cABs = answer[:, 3]
ax = axes[0]
ax.plot(times, cAs, label='A')
ax.plot(times, cBs, label='B')
ax.plot(times, cCs, label='C')
ax.plot(times, cABs, label='AB')
ax.legend(loc='best')
ax.set_xlabel('time')
ax.set_ylabel('$C$');
ax.set_title('no $C_{B,0}$')

CA0 = 1.0
CB0 = 0.3
answer = odeint(func=dy_dt, y0=[CA0, CB0, 0., 0.], t=times)
cAs = answer[:, 0]
cBs = answer[:, 1]
cCs = answer[:, 2]
cABs = answer[:, 3]
ax = axes[1]
ax.plot(times, cAs, label='A')
ax.plot(times, cBs, label='B')
ax.plot(times, cCs, label='C')
ax.plot(times, cABs, label='AB')
#ax.legend(loc='best')
ax.set_xlabel('time')
#ax.set_ylabel('$C$');
ax.set_title('$C_{{B,0}}$ = {:.1f}'.format(CB0))

fig.savefig('out.pdf')
