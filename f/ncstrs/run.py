from matplotlib import pyplot
import numpy as np
from scipy.special import gamma as gamma_fxn

class NCSTRs:
    """RTD model for N-CSTRs."""
    def __init__(self, N):
        self.N = N
    def __call__(self, t):
        """Returns the value of the RTD at time t."""
        if t <= 0.:
            return 0.
        N = self.N
        tau = 1. / N
        return t**(N - 1.) / gamma_fxn(N) / tau**N * np.exp(-t / tau)
        

fig, ax = pyplot.subplots()
fig.subplots_adjust(top=0.99, right=0.99)
times = np.linspace(-0.2, 3., num=200)

for N in [1, 2, 3, 5, 10, 20]:
    ncstrs = NCSTRs(N)
    ys = [ncstrs(_) for _ in times]
    ax.plot(times, ys, label='$N={:d}$'.format(N))

ax.legend()
ax.set_xlabel(r'$t/\tau$')
ax.set_ylabel('tracer concentration')
ax.set_xlim(times[[0, -1]])

fig.savefig('out.pdf')
