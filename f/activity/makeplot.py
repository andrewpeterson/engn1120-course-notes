#!/usr/bin/env python
"""
Starter script for matplotlib figures.
"""

import numpy as np
from matplotlib import pyplot


def makefig(figsize=(5., 5.), nrows=1, ncols=1,
            lm=0.1, rm=0.1, bm=0.1, tm=0.1, hg=0.1, vg=0.1,
            hr=None, vr=None):
    """
    figsize: canvas size
    nrows, ncols: number of horizontal and vertical images
    lm, rm, bm, tm, hg, vg: margins and "gaps"
    hr is horizontal ratio, and can be fed in as for example
    (1., 2.) to make the second axis twice as wide as the first.
    [same for vr]
    """
    nv, nh = nrows, ncols
    hr = np.array(hr, dtype=float) / np.sum(hr) if hr else np.ones(nh) / nh
    vr = np.array(vr, dtype=float) / np.sum(vr) if vr else np.ones(nv) / nv

    axwidths = (1. - lm - rm - (nh - 1.) * hg) * hr
    axheights = (1. - bm - tm - (nv - 1.) * vg) * np.array(vr)

    fig = pyplot.figure(figsize=figsize)
    axes = []
    bottompoint = 1. - tm
    for iv, v in enumerate(range(nv)):
        leftpoint = lm
        bottompoint -= axheights[iv]
        for ih, h in enumerate(range(nh)):
            ax = fig.add_axes((leftpoint, bottompoint,
                               axwidths[ih], axheights[iv]))
            axes.append(ax)
            leftpoint += hg + axwidths[ih]
        bottompoint -= vg
    return fig, axes


def get_chemical_potential(activity):
    mu0 = 0.
    R = 8.3  # J/mol/K
    T = 300.  # K
    return mu0 + R * T * np.log(activity)

fig, axes = makefig(figsize=(5., 4.), bm=0.12, lm=0.12, tm=0.01, rm=0.01)
ax = axes[0]
activities = np.linspace(0.05, 1.0)
chemical_potentials = get_chemical_potential(activities)
ax.plot(activities, chemical_potentials, '-')
xlim = ax.get_xlim()
ax.plot(xlim, [1., 1.], ':k', zorder=1)
ax.set_xlim(xlim)
ax.set_yticks([0])
ax.set_yticklabels(['$\mu_i^\circ$'])
ax.set_xticks(np.arange(0., 1.2, 0.2))
ax.set_xlabel('activity')
ax.set_ylabel('chemical potential')
fig.savefig('out.pdf')
