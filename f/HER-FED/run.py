import numpy as np
from matplotlib import pyplot

def get_Gs(G_BH, E_RHE):
    """Returns the free energy of the two HER intermediates
    for a specified binding free energy (G_BH, in eV) and
    potential (E_RHE, in volts RHE)."""
    G1 = G_BH + 1. * E_RHE  # in eV
    G2 = 2. * E_RHE  # in eV; this is the overall change.
    return [G1, G2]

def plot_Gs(Gs, color):
    """Adds free energy lines to the plot for specified G values."""
    # Horizontal lines.
    ax.plot([0, 2./3.], [0.]*2, '-', linewidth=2., color=color)
    ax.plot([1, 1. + 2./3.], [Gs[0]]*2, '-', linewidth=2., color=color)
    ax.plot([2, 2. + 2./3.], [Gs[1]]*2, '-', linewidth=2., color=color)
    # Connections.
    ax.plot([2./3., 1.], [0., Gs[0]], linewidth=1., color=color)
    ax.plot([1 + 2./3., 2.], [Gs[0], Gs[1]], linewidth=1., color=color)

fig, axes = pyplot.subplots(ncols=2, figsize=(6.5, 3.),
                            sharex=True, sharey=True)
fig.subplots_adjust(bottom=0.15, right=0.98)
potentials = np.linspace(-1., 0., num=10)
colors0 = pyplot.cm.Blues(np.linspace(0., 1., len(potentials)))
colors1 = pyplot.cm.Reds(np.linspace(0., 1., len(potentials)))
for potential, color0, color1 in zip(potentials, colors0, colors1):
    ax = axes[0]
    Gs = get_Gs(0.25, potential)
    plot_Gs(Gs, color0)
    ax = axes[1]
    Gs = get_Gs(-0.40, potential)
    plot_Gs(Gs, color1)
axes[0].set_title('Material A')
axes[1].set_title('Material B')
axes[0].set_ylabel('$\Delta G(\mathcal{E}^\mathrm{RHE})$')
for ax in axes:
    ax.set_xlabel('(H$^+$ + e$^-$) transferred')
    ax.set_xticks([0, 1, 2])
fig.savefig('out.pdf')
