
from matplotlib import pyplot
import numpy as np



times = np.linspace(0., 50., num=1000)
k = 0.1
k1 = k
k2 = k
normals = [np.exp(-k1 * _) for _ in times]
autocats = [np.exp(+k2 * _) for _ in times]



#pyplot.xkcd()
fig, axes = pyplot.subplots(ncols=2, sharex=True, sharey=False, figsize=(5.5,2.5))
fig.subplots_adjust(left=0.05, right=0.99, wspace=0.05)
ax = axes[0]
ax.plot(times, normals)
ax = axes[1]
ax.plot(times, autocats)
for ax in axes:
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlabel('time')
axes[0].set_ylabel('reactant concentration')
axes[0].set_title('normal reaction')
axes[1].set_title('autocatalytic reaction')


fig.savefig('out.pdf')
