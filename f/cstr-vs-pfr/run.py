from matplotlib import pyplot
import numpy as np


fig, axes = pyplot.subplots(ncols=2, figsize=(6., 3.))
fig.subplots_adjust(top=0.99, right=0.99, bottom=0.15)

kts = np.linspace(0., 3.)
cstrs = [1./(1. + _) for _ in kts]
pfrs = [np.exp(-_) for _ in kts]

ax = axes[0]
ax.plot(kts, cstrs, label='CSTR')
ax.plot(kts, pfrs, label='PFR')
ax.legend()
ax.set_ylabel(r'$\frac{C_{A}}{C_{A,0}}$')
ax.set_xlabel(r'$k_1 \tau$')

ax = axes[1]
ax.plot([kts[0], kts[-1]], [cstrs[-1]]*2, label='CSTR')
ax.plot(kts, pfrs, label='PFR')
ax.xaxis.set_ticks([0, 3])
ax.xaxis.set_ticklabels(['inlet', 'exit'])
ax.set_ylabel('rate $\propto$ concentration')
ax.set_yticks([])

fig.savefig('out.pdf')
