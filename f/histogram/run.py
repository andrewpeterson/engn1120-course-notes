import numpy as np
from matplotlib import pyplot

with open('MT2.csv') as f:
    lines = f.read().splitlines()

scores = np.array([float(_) for _ in lines])
scores *= 2.  # Was out of 50 last time.

fig, axes = pyplot.subplots(figsize=(6.5, 3.), ncols=2)
fig.subplots_adjust(right=0.99, top=0.99, left=0.08, bottom=0.15,
                    wspace=0.32)

ax = axes[0]
bins = None
bins = np.arange(10., 101., step=10.)
hist = ax.hist(scores, bins=bins)
bins = hist[1]
ax.set_xticks(bins)

ax.set_xlabel('score')
ax.set_ylabel('number of scores in this range')

# Show the scores explicitly. (?)
if False:
    scores.sort()
    lastbin = 0
    for score in scores:
        print('--')
        print(score)
        #print(score < bins)
        # Magic command to figure out which bin we are in.
        bin = np.where(score < bins)[0][0]
        print(bin)
        print(bins[bin])
        if bin != lastbin:
            lastbin = bin
            bincount = 0
        else:
            bincount += 1
        binmean = (bins[bin - 1] + bins[bin]) / 2.
        ax.text(binmean, bincount + 0.5, '{:.0f}'.format(score), ha='center',
                fontsize=7.)

ax.text(10., 6.5, '$N$={:d}'.format(len(scores)))

ax = axes[1]
ax.set_xticks(bins)
ax.set_xlabel('score')
ax.set_ylabel('probability density in this range')
ax.hist(scores, bins=bins, density=True)
    

fig.savefig('out.pdf')

# Compare the true mean from the bin-calculated mean.
print('True mean: {:.2f}'.format(np.mean(scores)))
counts = hist[0]
bins = hist[1]
means = [(bins[_] + bins[_ + 1])/2. for _ in range(len(bins) - 1)]
bin_calculated_mean = np.sum(counts * means) / len(scores)
print('Bin-calculated mean: {:.2f}'.format(bin_calculated_mean))

# Calculate the variance, again from the bins.
v = sum(counts * ((means - bin_calculated_mean)**2)) / len(scores)
print('Bin-calculated variance: {:.2f}'.format(v))
print('Bin-calculated standard deviation: {:.2f}'.format(np.sqrt(v)))


