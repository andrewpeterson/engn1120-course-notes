import numpy as np
from matplotlib import pyplot

with open('MT2.csv') as f:
    lines = f.read().splitlines()

scores = np.array([float(_) for _ in lines])
scores *= 2.  # Was out of 50 last time.

fig, ax = pyplot.subplots()
fig.subplots_adjust(right=0.99, top=0.99, left=0.08, bottom=0.09)
bins = None
bins = np.arange(10., 101., step=10.)
hist = ax.hist(scores, bins=bins)
bins = hist[1]
ax.set_xticks(bins)

ax.set_xlabel('score')
ax.set_ylabel('number in this range')

scores.sort()
lastbin = 0
for score in scores:
    print('--')
    print(score)
    #print(score < bins)
    # Magic command to figure out which bin we are in.
    bin = np.where(score < bins)[0][0]
    print(bin)
    print(bins[bin])
    if bin != lastbin:
        lastbin = bin
        bincount = 0
    else:
        bincount += 1
    binmean = (bins[bin - 1] + bins[bin]) / 2.
    ax.text(binmean, bincount + 0.5, '{:.0f}'.format(score), ha='center')
    

fig.savefig('out.pdf')
