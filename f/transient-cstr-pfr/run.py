import numpy as np
from matplotlib import pyplot

fig, ax = pyplot.subplots(figsize=(5.5, 3.))
fig.subplots_adjust(bottom=0.15, right=0.99, top=0.99)

ktau = 1.
lw = 2.
maxt = 5.

# PFR.
ss = np.exp(-ktau)
ax.plot([0., 1.], [0., 0.], '-', lw=lw, color='C0')
ax.plot([1., 1.], [0., ss], '-', lw=lw, color='C0')
ax.plot([1., maxt], [ss, ss], '-', lw=lw, color='C0')

# CSTR.
def get_c(t):
    return (1. - np.exp(-t)) / (1. + ktau)

ts = np.linspace(0., maxt)
cs = get_c(ts)
ax.plot(ts, cs, '-', lw=lw, color='C1')

ax.set_xlabel(r't/$\tau$')
ax.set_ylabel(r'${C_\mathrm{A}}/{C_\mathrm{A}^0}$')
ax.text(4., 0.3, 'PFR', color='C0')
ax.text(4., 0.53, 'CSTR', color='C1')
ax.set_ylim(-0.05, 1.05)

fig.savefig('out.pdf')
