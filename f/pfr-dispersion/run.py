import numpy as np
from matplotlib import pyplot


class RTD:
    """Create RTD plots for PFR with dispersion."""

    def __init__(self, Pe):
        self.Pe = Pe

    def __call__(self, theta):
        if theta <= 0.:
            return 0.
        Pe = self.Pe
        e = np.sqrt(Pe / 4. / np.pi / theta)
        e *= np.exp((-(1. - theta)**2 * Pe) / 4. / theta)
        return e


def get_max_theta(thetas, Es):
    """Finds the maximum value of E, and returns the theta that
    corresponds to it."""
    argmax = np.argmax(Es)
    return thetas[argmax]


fig, ax = pyplot.subplots()
fig.subplots_adjust(left=0.09, top=0.99, right=0.99, bottom=0.09)
thetas = np.linspace(-0.1, 2., num=1000)

Pes = [0.01, 0.1, 1., 10., 100., 1000.]
text_offsets = [-0.02, 0., 0., -0.00, -0.02, +0.03]
for index, Pe in enumerate(Pes):
    rtd = RTD(Pe)
    Es = [rtd(_) for _ in thetas]
    ax.plot(thetas, Es / np.max(Es), label='Pe={:g}'.format(Pe))
    ax.text(get_max_theta(thetas, Es) + text_offsets[index], 1.02,
            'Pe={:g}'.format(Pe), color='C{:d}'.format(index),
            rotation=90, ha='center', va='bottom')

ax.set_ylim(-0.1, 1.3)
ax.set_xlim(thetas[0], thetas[-1])
ax.set_xlabel(r'$\theta$ ($t/\tau$)')
ax.set_ylabel(r'$E(\theta) \, / \, \mathrm{max}(E)$')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
fig.savefig('out.pdf')
