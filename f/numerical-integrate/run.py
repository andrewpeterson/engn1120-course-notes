import numpy as np
from scipy.integrate import odeint
from matplotlib import pyplot


def get_yprime(y, t):
    y1, y2 = y
    yprime1 = y2 - 10. * np.exp(-1.0 * t)
    yprime2 = y1 + 10. * np.sin(10.0 * t)
    return [yprime1, yprime2]


times = np.linspace(0., 2.)
ans = odeint(func=get_yprime, y0=[5., 2.], t=times)
y1s = ans[:, 0]
y2s = ans[:, 1]

fig, ax = pyplot.subplots()
ax.plot(times, y1s, label='$y_1$')
ax.plot(times, y2s, label='$y_2$')
ax.legend()
ax.set_xlabel('time, s')
ax.set_ylabel('$y$')
fig.savefig('out.pdf')
