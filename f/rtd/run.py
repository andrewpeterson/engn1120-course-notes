from matplotlib import pyplot
import numpy as np


fig, axes = pyplot.subplots(nrows=2, ncols=2)
fig.subplots_adjust(left=0.20, right=0.99)
print(axes)
w = 0.05  # pulse width
b = -0.2  # begin time
e = 1.8   # end time

# PFR in.
ax = axes[0, 0]


ax.plot([b, 0., 0., w, w, e ],
        [0.,   0., 1., 1., 0., 0. ],
        'b-')

# PFR out.
ax = axes[1, 0]

ax.plot([b, 1., 1., 1. + w, 1 + w, e  ],
        [0., 0., 1., 1., 0., 0. ],
        'b-')

# CSTR in.
ax = axes[0, 1]

ax.plot([b, 0., 0., w, w, e ],
        [0.,   0., 1., 1., 0., 0. ],
        'r-')

# CSTR out.
ax = axes[1, 1]

def get_cstr_out(t):
    if t < 0.:
        return 0.
    else:
        return np.exp(-t / 1.)

ts = np.linspace(b, e, num=1000)
ys = [get_cstr_out(_) for _ in ts]
ax.plot(ts, ys, 'r-')
ax.set_ylim(-.05, 5.)

# Make it look pretty.

for ax in axes.flatten():
    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    # Only show ticks on the left and bottom spines
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    
    # Actually shut off all ticks.
    ax.set_xticks([])
    ax.set_yticks([])

    ax.set_xlabel('time')
    ax.set_ylabel('tracer concentration')

    ax.set_xlim(b, e)

axes[0, 0].set_title('PFR', color='b')
axes[0, 1].set_title('CSTR', color='r')

fig.text(0., 0.25, r'Outlet$\rightarrow$')
fig.text(0., 0.75, r'Inlet$\rightarrow$')

fig.savefig('out.pdf')
